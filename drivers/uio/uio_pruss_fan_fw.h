#ifndef __UIO_PRUSS_FAN_FW_H__
#define __UIO_PRUSS_FAN_FW_H__

#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/clk.h>

int uio_pruss_fan_fw_load(struct platform_device *pdev, tprussdrv *data);
int uio_pruss_fan_fw_start(struct device *dev, int pru_id);
int uio_pruss_fan_fw_stop(struct device *dev, int pru_id);

#endif /* __UIO_PRUSS_FAN_FW_H */

