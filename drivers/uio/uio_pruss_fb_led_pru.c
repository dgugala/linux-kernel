#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/firmware.h>

#include "uio_pruss_fb_led_pru.h"
#include "uio_pruss_fb_led_conv.h"

#define FW0_NAME		"pru_led_fw0.bin"
#define FW1_NAME		"pru_led_fw1.bin"
#define FW0_LED_DIAG_NAME	"pru_led_diagnostics_fw0.bin"
#define FW1_LED_DIAG_NAME	"pru_led_diagnostics_fw1.bin"
/** Internal Pruss driver data pointer */
static tprussdrv* uio_pruss_fb_pru_data = NULL;

/* 1 framebuffer, 4 LED chains and two bitplanes per chain */
#define NUM_BITPLANES		8
#if 0
#define BITPLANE0_CONF_OFFSET	0x0010
#define BITPLANE1_CONF_OFFSET	0x0030
#define BITPLANE2_CONF_OFFSET	0x0050
#define BITPLANE3_CONF_OFFSET	0x0070
#define BITPLANE4_CONF_OFFSET	0x2010
#define BITPLANE5_CONF_OFFSET	0x2030
#define BITPLANE6_CONF_OFFSET	0x2050
#define BITPLANE7_CONF_OFFSET	0x2070
#endif
#define CONF_REGS_BUFFER_SIZE	50
static DECLARE_WAIT_QUEUE_HEAD(pru0_wait_queue);

static int pru0_fr_done = 0;
static int pruss_error_cnt = -1;
static int pruss_not_connected_errors_cnt = 0;
static int pruss_max_error_cnt = 0;

static int pruss_chain_feedback_0 = 0; // 0=OK, 1=ERROR.
static int pruss_chain_feedback_1 = 0;
static int pruss_chain_feedback_2 = 0;
static int pruss_chain_feedback_3 = 0;
static int pru0_conf_regs_0[CONF_REGS_BUFFER_SIZE] = {0xBEEF,};
static int pru0_conf_regs_1[CONF_REGS_BUFFER_SIZE] = {0xBEEF,};
static int pru1_conf_regs_0[CONF_REGS_BUFFER_SIZE] = {0xBEEF,};
static int pru1_conf_regs_1[CONF_REGS_BUFFER_SIZE] = {0xBEEF,};

static tled_bitplane_config uio_pruss_fb_bitplane_configs[NUM_BITPLANES];

tprussdrv* uio_pruss_fb_led_pru_create_data(void)
{
	tprussdrv* data;
	data = kzalloc(sizeof(tprussdrv), GFP_KERNEL);
	if (data)
		uio_pruss_fb_pru_data = data;
	return data;
}
EXPORT_SYMBOL(uio_pruss_fb_led_pru_create_data);

void uio_pruss_fb_led_pru_remove_data(tprussdrv* data)
{
	if (data) {
		kfree(data);
	}
	uio_pruss_fb_pru_data = NULL;
}
EXPORT_SYMBOL(uio_pruss_fb_led_pru_remove_data);
#if 0
void prussdrv_pru_write_memory_u32(unsigned int pru_ram_id, 
                u32 offset, u32 *datatowrite,
                u16 wordstowrite, tprussdrv *data)
{
        u32 *addresstowrite;
        u16 loop;
        //offset += prussdrv_pru_get_memory_offset(pru_ram_id, data);

        addresstowrite = (u32 *)(offset);
        printk(KERN_INFO "Write u32 (%d): %x\n", pru_ram_id, addresstowrite);
        for (loop = 0; loop < wordstowrite; loop++)
                *addresstowrite++ = *datatowrite++;
}
#endif
void uio_pruss_fb_led_pru0_ram_write_data_u32(u32 offset, u32 *datatowrite,
		u16 wordstowrite, tprussdrv *data)
{
	offset +=(u32)prussdrv_pru_get_memory_offset(PRUSS0_PRU0_DATARAM, data); 

	prussdrv_pru_write_memory_u32(offset,
		datatowrite, wordstowrite, data);
/*
	u32 *addresstowrite;
	u16 loop;
	offset +=(u32)prussdrv_pru_get_memory_offset(PRUSS0_PRU0_DATARAM, data); 

	addresstowrite = (u32 *)(offset);

	for (loop = 0; loop < wordstowrite; loop++)
		*addresstowrite++ = *datatowrite++;*/
}

void uio_pruss_fb_led_pru1_ram_write_data_u32(u32 offset, u32 *datatowrite,
		u16 wordstowrite, tprussdrv *data)
{
	offset +=(u32)prussdrv_pru_get_memory_offset(PRUSS0_PRU1_DATARAM, data); 

	prussdrv_pru_write_memory_u32(offset,
		datatowrite, wordstowrite, data);
/*
	u32 *addresstowrite;
	u16 loop;
	offset +=(u32)prussdrv_pru_get_memory_offset(PRUSS0_PRU1_DATARAM, data); 

	addresstowrite = (u32 *)(offset);

	for (loop = 0; loop < wordstowrite; loop++)
		*addresstowrite++ = *datatowrite++;
*/
}
#if 0
void uio_pruss_fb_led_pru_ram_write_data_u32(u32 offset, u32 *datatowrite,
	    u16 wordstowrite, tprussdrv *data)
{
//	u32 *addresstowrite;
//	u16 loop;
	s32 ret = 0;
	ret = prussdrv_pru_write_memory(PRUSS0_PRU0_DATARAM, offset, datatowrite, wordstowrite*4, data);
	if (ret == -1)
		printk(KERN_INFO "Writing PRU memory failed\n");
//	offset = (u32)pru_arm_iomap->pru_io_addr + offset;
//	addresstowrite = (u32 *)(offset);

//	for (loop = 0; loop < wordstowrite; loop++)
//		*addresstowrite++ = *datatowrite++;
}
#endif
void uio_pruss_fb_led_pruss_ram_read_data_u32(u32 offset, u32 *datatoread,
	    u16 wordstoread, tprussdrv *data)
{
	u32 *addresstoread;
	u16 loop;

	addresstoread = (u32 *)(offset);

	//printk(KERN_INFO "R - trg: %x src: %x len: %d\n", addresstoread, datatoread, wordstoread);
	for (loop = 0; loop < wordstoread; loop++)
		*datatoread++ = *addresstoread++;
}

void uio_pruss_fb_led_pru0_ram_read_data_u32(u32 offset, u32 *datatoread,
	    u16 wordstoread, tprussdrv *data)
{
	offset +=(u32)prussdrv_pru_get_memory_offset(PRUSS0_PRU0_DATARAM, data); 

	prussdrv_pru_read_memory_u32(offset,
		datatoread, wordstoread, data);
}

void uio_pruss_fb_led_pru1_ram_read_data_u32(u32 offset, u32 *datatoread,
	    u16 wordstoread, tprussdrv *data)
{
	offset +=(u32)prussdrv_pru_get_memory_offset(PRUSS0_PRU1_DATARAM, data); 

	prussdrv_pru_read_memory_u32(offset,
		datatoread, wordstoread, data);
}

/*
 * This function sets both bitplanes data pointers for each chain,
 * so they reference to the same address. Remember that PRU switches from
 * bitplane 0 to 1 and back all the time.
 */
void uio_pruss_fb_led_set_visible_chain_data_pointer(u8 chain_index, u32 data_addr)
{
	u8 bp_index = chain_index*2;
	tled_bitplane_config *conf0;
	tled_bitplane_config *conf1;

	if (bp_index >= NUM_BITPLANES) {
		printk("%s: Bitplane index out of range!\n", __FUNCTION__);
		return;
	}

	if (!uio_pruss_fb_pru_data) {
		printk("%s: Internal pru data pointer is NULL!\n", __FUNCTION__);
		return;
	}

	/* Bitplane 0 */
	conf0 = &uio_pruss_fb_bitplane_configs[bp_index];
	conf0->data_pointer = (void *)data_addr;

	/* Bitplane 1 */
	conf1 = &uio_pruss_fb_bitplane_configs[bp_index + 1];
	conf1->data_pointer = (void *)data_addr;

	switch (chain_index) {
		case 0:
		        uio_pruss_fb_led_pru0_ram_write_data_u32(BITPLANE0_CONFIG_OFFSET,
                                                (u32 *)&conf0->data_pointer,
                                                1, uio_pruss_fb_pru_data);
		        uio_pruss_fb_led_pru0_ram_write_data_u32(BITPLANE1_CONFIG_OFFSET,
                                                (u32 *)&conf1->data_pointer,
                                                 1, uio_pruss_fb_pru_data);
			break;
		case 1:
		        uio_pruss_fb_led_pru0_ram_write_data_u32(BITPLANE2_CONFIG_OFFSET,
                                                (u32 *)&conf0->data_pointer,
                                                1, uio_pruss_fb_pru_data);
		        uio_pruss_fb_led_pru0_ram_write_data_u32(BITPLANE3_CONFIG_OFFSET,
                                                (u32 *)&conf1->data_pointer,
                                                 1, uio_pruss_fb_pru_data);
			break;
		case 2:
		        uio_pruss_fb_led_pru1_ram_write_data_u32(BITPLANE0_CONFIG_OFFSET,
                                                (u32 *)&conf0->data_pointer,
                                                1, uio_pruss_fb_pru_data);
		        uio_pruss_fb_led_pru1_ram_write_data_u32(BITPLANE1_CONFIG_OFFSET,
                                                (u32 *)&conf1->data_pointer,
                                                 1, uio_pruss_fb_pru_data);
			break;
		case 3:
		        uio_pruss_fb_led_pru1_ram_write_data_u32(BITPLANE2_CONFIG_OFFSET,
                                                (u32 *)&conf0->data_pointer,
                                                1, uio_pruss_fb_pru_data);
		        uio_pruss_fb_led_pru1_ram_write_data_u32(BITPLANE3_CONFIG_OFFSET,
                                                (u32 *)&conf1->data_pointer,
                                                 1, uio_pruss_fb_pru_data);
			break;
	}
}
EXPORT_SYMBOL(uio_pruss_fb_led_set_visible_chain_data_pointer);

int uio_pruss_fb_led_init_chain_configs(tprussdrv *data)
{
	u8 i = 0;
	tled_bitplane_config *conf;
	u8 config_size = sizeof(tled_bitplane_config)/sizeof(u32);
	u32 addr = 0;

	/* Initial values of the LED bitplanes */
	for (i = 0; i < NUM_BITPLANES; i++) {
		conf = &uio_pruss_fb_bitplane_configs[i];
		conf->latch_on = LATCH_ON_DEF;
		conf->next_sync = NEXT_SYNC_DEF;
		conf->shift_reg_out = SHIFT_REG_OUT_DEF;
		conf->shift_reg_in = SHIFT_REG_IN_DEF;
		conf->number_of_frames = NUM_OF_FRAMES_DEF;
		conf->number_of_chain_leds = NUM_OF_CHAIN_LEDS_DEF;
		conf->led_bit_counter = LED_BIT_COUNTER_DEF;
		conf->latch_length = LATCH_LEN_DEF;
		conf->last_address = LAST_ADDRESS_DEF;
		conf->bit_configs = BIT_CONFIGS_DEF;
		conf->word_width_bits = WORD_WIDTH_BITS_DEF;
		conf->ii = II_BIT_COUNTER_DEF;
		conf->address_counter = ADDRESS_COUNTER_DEF;
	}

	/* PRU0 init values */
	uio_pruss_fb_led_pru0_ram_write_data_u32(BITPLANE0_CONFIG_OFFSET,
					 (u32 *)(&uio_pruss_fb_bitplane_configs[0]),
					 config_size, data);
	uio_pruss_fb_led_pru0_ram_write_data_u32(BITPLANE1_CONFIG_OFFSET,
					 (u32 *)(&uio_pruss_fb_bitplane_configs[1]),
					 config_size, data);
	uio_pruss_fb_led_pru0_ram_write_data_u32(BITPLANE2_CONFIG_OFFSET,
					 (u32 *)(&uio_pruss_fb_bitplane_configs[2]),
					 config_size, data);
	uio_pruss_fb_led_pru0_ram_write_data_u32(BITPLANE3_CONFIG_OFFSET,
					 (u32 *)(&uio_pruss_fb_bitplane_configs[3]),
					 config_size, data);
	/* PRU1 init values */
	uio_pruss_fb_led_pru1_ram_write_data_u32(BITPLANE0_CONFIG_OFFSET,
					 (u32 *)(&uio_pruss_fb_bitplane_configs[4]),
					 config_size, data);
	uio_pruss_fb_led_pru1_ram_write_data_u32(BITPLANE1_CONFIG_OFFSET,
					 (u32 *)(&uio_pruss_fb_bitplane_configs[5]),
					 config_size, data);
	uio_pruss_fb_led_pru1_ram_write_data_u32(BITPLANE2_CONFIG_OFFSET,
					 (u32 *)(&uio_pruss_fb_bitplane_configs[6]),
					 config_size, data);
	uio_pruss_fb_led_pru1_ram_write_data_u32(BITPLANE3_CONFIG_OFFSET,
					 (u32 *)(&uio_pruss_fb_bitplane_configs[7]),
					 config_size, data);
	
	// Without the following initializations, PRU software won't be able
	// find the initialization configurations of the bitplanes.
	// PRU0 (Chain 1 and 2)
	// PRU1 (Chain 3 and 4)
	addr = 0x00000010;
	uio_pruss_fb_led_pru0_ram_write_data_u32(BITPLANE0_DATA_BLOCK_OFFSET, &addr, 1, data);
	uio_pruss_fb_led_pru1_ram_write_data_u32(BITPLANE0_DATA_BLOCK_OFFSET, &addr, 1, data);

	addr = 0x00000030;
	uio_pruss_fb_led_pru0_ram_write_data_u32(BITPLANE1_DATA_BLOCK_OFFSET, &addr, 1, data);
	uio_pruss_fb_led_pru1_ram_write_data_u32(BITPLANE1_DATA_BLOCK_OFFSET, &addr, 1, data);

	addr = 0x00000050;
	uio_pruss_fb_led_pru0_ram_write_data_u32(BITPLANE2_DATA_BLOCK_OFFSET, &addr, 1, data);
	uio_pruss_fb_led_pru1_ram_write_data_u32(BITPLANE2_DATA_BLOCK_OFFSET, &addr, 1, data);

	addr = 0x00000070;
	uio_pruss_fb_led_pru0_ram_write_data_u32(BITPLANE3_DATA_BLOCK_OFFSET, &addr, 1, data);
	uio_pruss_fb_led_pru1_ram_write_data_u32(BITPLANE3_DATA_BLOCK_OFFSET, &addr, 1, data);
	return 0;
}
EXPORT_SYMBOL(uio_pruss_fb_led_init_chain_configs);

/**********************************************************************
 *
 * SYSFS attribute definitions
 *
 **********************************************************************/
#define DEV_GET_PRUSS_DATA(x) ((((struct uio_pruss_dev*)dev_get_drvdata(x))->pru_data))

static ssize_t uio_pruss_fb_led_pru_run_show(struct device *dev,
                              struct device_attribute *attr,
                              char *buf)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);

	return sprintf(buf, "%d\n", data->pruss_running);
}

static int uio_pruss_fb_start_pru(struct device *dev, int pru_id)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
	int ret = 0;

	printk(KERN_INFO "Starting PRU %d\n", pru_id);
	ret = pru_run(pru_id, data);
	if (ret) {
		dev_err(dev, "can't start PRU%d (%d)\n", pru_id, ret);
		return -1;
	}

        data->pruss_running = 1;
	if (pru_id == PRU_NUM0)
                data->pruss_0_running = 1;
	if (pru_id == PRU_NUM1)
                data->pruss_1_running = 1;

	return 0;
}

static ssize_t uio_pruss_fb_led_pru_run_store(struct device *dev,
                               struct device_attribute *attr,
                               const char *buf, size_t len)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
	int ret = 0;
	printk(KERN_INFO "LOAD PRU\n");
	if (sysfs_streq(buf, "1")) {
		ret = uio_pruss_fb_start_pru(dev, PRU_NUM0);
		if (ret) {
			return len;
		}

		ret = uio_pruss_fb_start_pru(dev, PRU_NUM1);
		if (ret) {
			return len;
		}

	} else if (sysfs_streq(buf, "0")) {
		pru_disable(data);
		data->pruss_running = 0;
		data->pruss_0_running = 0;
		data->pruss_1_running = 0;
	}

	return len;
}
static DEVICE_ATTR(run, S_IRUGO | S_IWUSR, uio_pruss_fb_led_pru_run_show,
               uio_pruss_fb_led_pru_run_store);

static ssize_t uio_pruss_fb_led_pru_run_0_show(struct device *dev,
                              struct device_attribute *attr,
                              char *buf)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);

	return sprintf(buf, "%d\n", data->pruss_0_running);
}

static ssize_t uio_pruss_fb_led_pru_run_0_store(struct device *dev,
                               struct device_attribute *attr,
                               const char *buf, size_t len)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
	int ret = 0;
	
	if (sysfs_streq(buf, "1")) {
		ret = uio_pruss_fb_start_pru(dev, PRU_NUM0);
		if (ret) {
			return len;
		}
	} else if (sysfs_streq(buf, "0")) {
		ret = pru_stop(PRU_NUM0, data);
		if (ret) {
			dev_err(dev, "can't stop PRU0 (%d)\n", ret);
			return len;
		}
		data->pruss_0_running = 0;
		if (data->pruss_1_running == 0)
		{
		    data->pruss_running = 0;
		}
	}
	
	return len;
}
static DEVICE_ATTR(run_0, S_IRUGO | S_IWUSR, uio_pruss_fb_led_pru_run_0_show,
               uio_pruss_fb_led_pru_run_0_store);

static ssize_t uio_pruss_fb_led_pru_run_1_show(struct device *dev,
                              struct device_attribute *attr,
                              char *buf)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);

	return sprintf(buf, "%d\n", data->pruss_1_running);
}

static ssize_t uio_pruss_fb_led_pru_run_1_store(struct device *dev,
                               struct device_attribute *attr,
                               const char *buf, size_t len)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
	int ret = 0;
	
	if (sysfs_streq(buf, "1")) {
		ret = uio_pruss_fb_start_pru(dev, PRU_NUM1);
		if (ret) {
			return len;
		}
	} else if (sysfs_streq(buf, "0")) {
		ret = pru_stop(PRU_NUM1, data);
		if (ret) {
			dev_err(dev, "can't stop PRU1 (%d)\n", ret);
			return len;
		}
		data->pruss_1_running = 0;
		if (data->pruss_0_running == 0)
		{
		    data->pruss_running = 0;
		}
	}

	return len;
}
static DEVICE_ATTR(run_1, S_IRUGO | S_IWUSR, uio_pruss_fb_led_pru_run_1_show,
               uio_pruss_fb_led_pru_run_1_store);

static ssize_t uio_pruss_fb_led_pru_run_led_diagnostics_show(struct device *dev,
						      struct device_attribute *attr,
						      char *buf)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);

	return sprintf(buf, "%d\n", data->led_diagnostics_running);
}

static ssize_t uio_pruss_fb_led_pru_run_led_diagnostics_store(struct device *dev,
						       struct device_attribute *attr,
						       const char *buf, size_t len)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
	struct platform_device *pdev = to_platform_device(dev);
	int ret = 0, pru0_was_running = 0, pru1_was_running = 0;
	int error_count_pru0 = 0, error_count_pru1 = 0;
	int max_error_count_pru0 = 0, max_error_count_pru1 = 0;
	int chain_fb_pru0 = 0, chain_fb_pru1 = 0;
	int i = 0;
	void *data_pointer = 0;
	int j = 0;
	int* regs = NULL;

	
	if (sysfs_streq(buf, "1")) {
		data->led_diagnostics_running = 1;
		uio_pruss_fb_led_fb_clr_bitplanes();
		msleep(100);
		if (data->pruss_0_running == 1)
		{
			ret = pru_stop(PRU_NUM0, data);
			if (ret) {
				dev_err(dev, "can't stop PRU0 (%d)\n", ret);
				return len;
			}
			data->pruss_0_running = 0;
			pru0_was_running = 1;
		}
		if (data->pruss_1_running == 1)
		{
			ret = pru_stop(PRU_NUM1, data);
			if (ret) {
				dev_err(dev, "can't stop PRU1 (%d)\n", ret);
				return len;
			}
			data->pruss_1_running = 0;
			pru1_was_running = 1;
		}
		data->pruss_running = 0;
		/* Do the diagnostics. */

		ret = uio_pruss_fb_led_load_diagnostics_fw(pdev, data);
		if (ret < 0) {
			dev_err(dev, "can't load PRUSS firmware (%d)\n", ret);
			return len;
		}

		ret = uio_pruss_fb_start_pru(dev, PRU_NUM0);
		if (ret) {
			return len;
		}
		ret = uio_pruss_fb_start_pru(dev, PRU_NUM1);
		if (ret) {
			return len;
		}
		msleep(500);
		ret = pru_stop(PRU_NUM0, data);
		if (ret) {
			dev_err(dev, "can't stop PRU0 (%d)\n", ret);
			return len;
		}
		ret = pru_stop(PRU_NUM1, data);
		if (ret) {
			dev_err(dev, "can't stop PRU1 (%d)\n", ret);
			return len;
		}

		uio_pruss_fb_led_pru0_ram_read_data_u32(PRUSS_ERR_CNT_OFFSET, 
			(u32*)&error_count_pru0, 1, data);
		uio_pruss_fb_led_pru1_ram_read_data_u32(PRUSS_ERR_CNT_OFFSET,
			(u32*)&error_count_pru1, 1, data);
		pruss_error_cnt = (0x0000FFFF & error_count_pru0) + (0x0000FFFF & error_count_pru1);
		uio_pruss_fb_led_pru0_ram_read_data_u32(PRUSS_MAX_ERR_CNT_OFFSET,
			(u32*)&max_error_count_pru0, 1, data);
		uio_pruss_fb_led_pru1_ram_read_data_u32(PRUSS_MAX_ERR_CNT_OFFSET, 
			(u32*)&max_error_count_pru1, 1, data);
		pruss_max_error_cnt = (0x0000FFFF & max_error_count_pru0) + (0x0000FFFF & max_error_count_pru1);
		// Chain feedback return values.
		uio_pruss_fb_led_pru0_ram_read_data_u32(PRUSS_CHAIN_FEEDBACK_OFFSET, 
			(u32*)&chain_fb_pru0, 1, data);
		uio_pruss_fb_led_pru1_ram_read_data_u32(PRUSS_CHAIN_FEEDBACK_OFFSET, 
			(u32*)&chain_fb_pru1, 1, data);
		pruss_chain_feedback_0 = (0x00000001 & chain_fb_pru0);
		pruss_chain_feedback_1 = (0x00000010 & chain_fb_pru0) >> 4;
		pruss_chain_feedback_2 = (0x00000001 & chain_fb_pru1);
		pruss_chain_feedback_3 = (0x00000010 & chain_fb_pru1) >> 4;

		for (j = 0; j < 4; j++) {
			uio_pruss_fb_led_conv_get_bitplane_pointer(&data_pointer, j);
			if ( j == 0 )
				regs = &pru0_conf_regs_0[0];
			if ( j == 1 )
				regs = &pru0_conf_regs_1[0];
			if ( j == 2 )
				regs = &pru1_conf_regs_0[0];
			if ( j == 3 )
				regs = &pru1_conf_regs_1[0];
			i = 0;
			do {

				if (i >= CONF_REGS_BUFFER_SIZE)
				{
					break;
				}
				regs[i] = *(int *)data_pointer;
				data_pointer += sizeof(int);
				//++i;
			} while (regs[++i] != 0xBEEF);

		}
#if 0
		/* PRU0 - chain index 0 and 1*/
		uio_pruss_fb_led_conv_get_bitplane_pointer(&data_pointer, 0);
		i = 0;
		do {
			if (i >= CONF_REGS_BUFFER_SIZE)
			{
				break;
			}
			pru0_conf_regs_0[i] = *(int *)data_pointer;
			data_pointer += sizeof(int);
			++i;
		} while (pru0_conf_regs_0[i] != 0xBEEF);

		lpp01_led_conv_get_bitplane_pointer(&data_pointer, 1);
		i = 0;
		do {
			if (i >= CONF_REGS_BUFFER_SIZE)
			{
				break;
			}
			pru0_conf_regs_1[i] = *(int *)data_pointer;
			data_pointer += sizeof(int);
			++i;
		} while (pru0_conf_regs_1[i] != 0xBEEF);
		/* PRU1 - chain index 2 and 3 */
		lpp01_led_conv_get_bitplane_pointer(&data_pointer, 2);
		i = 0;
		do {
			if (i >= CONF_REGS_BUFFER_SIZE)
			{
				break;
			}
			pru1_conf_regs_0[i] = *(int *)data_pointer;
			data_pointer += sizeof(int);
			++i;
		} while (pru1_conf_regs_0[i] != 0xBEEF);
		lpp01_led_conv_get_bitplane_pointer(&data_pointer, 3);
		i = 0;
		do {
			if (i >= CONF_REGS_BUFFER_SIZE)
			{
				break;
			}
			pru1_conf_regs_1[i] = *(int *)data_pointer;
			data_pointer += sizeof(int);
			++i;
		} while (pru1_conf_regs_1[i] != 0xBEEF);
		#endif
		uio_pruss_fb_led_fb_clr_bitplanes();
		
		ret = uio_pruss_fb_led_load_fw(pdev, data);
		if (ret < 0) {
			dev_err(dev, "can't load PRUSS firmware (%d)\n", ret);
			return len;
		}


		if (pru0_was_running)
		{
			ret = uio_pruss_fb_start_pru(dev, PRU_NUM0);
			if (ret) {
				return len;
			}
		}

		if (pru1_was_running)
		{
			ret = uio_pruss_fb_start_pru(dev, PRU_NUM1);
			if (ret) {
				return len;
			}
		}
		data->led_diagnostics_running = 0;
	}

	return len;
}
static DEVICE_ATTR(run_led_diagnostics, S_IRUGO | S_IWUSR, uio_pruss_fb_led_pru_run_led_diagnostics_show,
               uio_pruss_fb_led_pru_run_led_diagnostics_store);


static ssize_t uio_pruss_fb_led_pru_led_errors_count_show(struct device *dev,
						   struct device_attribute *attr,
						   char *buf)
{
	int tmp;
	
	if (pruss_error_cnt >= pruss_not_connected_errors_cnt)
	{
		tmp = pruss_error_cnt - pruss_not_connected_errors_cnt;
	} else {
		tmp = pruss_error_cnt;
	}
	
	if (tmp > pruss_max_error_cnt)
		tmp = pruss_max_error_cnt;
	return sprintf(buf, "%d\n", tmp);
}
static DEVICE_ATTR(led_errors_count, S_IRUGO | S_IWUSR, uio_pruss_fb_led_pru_led_errors_count_show,
               NULL);

static ssize_t uio_pruss_fb_led_pru_not_connected_errors_count_show(struct device *dev,
							   struct device_attribute *attr,
							   char *buf)
{
	return sprintf(buf, "%d\n", pruss_not_connected_errors_cnt);
}
static ssize_t uio_pruss_fb_led_pru_not_connected_errors_count_store(struct device *dev,
							    struct device_attribute *attr,
							    const char *buf, size_t len)
{
	int tmp = 0;

	sscanf(buf, "%d", &tmp);
	if (tmp < 0)
	{
		pruss_not_connected_errors_cnt = 0;
	} else {
		pruss_not_connected_errors_cnt = tmp;
	}
	return len;
}
static DEVICE_ATTR(not_connected_errors_count, S_IRUGO | S_IWUSR,
		   uio_pruss_fb_led_pru_not_connected_errors_count_show,
		   uio_pruss_fb_led_pru_not_connected_errors_count_store);

static ssize_t uio_pruss_fb_led_pru_chain_feedback_0_show(struct device *dev,
						   struct device_attribute *attr,
						   char *buf)
{
	return sprintf(buf, "%d\n", pruss_chain_feedback_0);
}

static DEVICE_ATTR(chain_feedback_0, S_IRUGO | S_IWUSR, uio_pruss_fb_led_pru_chain_feedback_0_show,
               NULL);

static ssize_t uio_pruss_fb_led_pru_chain_feedback_1_show(struct device *dev,
						   struct device_attribute *attr,
						   char *buf)
{
	return sprintf(buf, "%d\n", pruss_chain_feedback_1);
}

static DEVICE_ATTR(chain_feedback_1, S_IRUGO | S_IWUSR, uio_pruss_fb_led_pru_chain_feedback_1_show,
               NULL);

static ssize_t uio_pruss_fb_led_pru_chain_feedback_2_show(struct device *dev,
						   struct device_attribute *attr,
						   char *buf)
{
	return sprintf(buf, "%d\n", pruss_chain_feedback_2);
}

static DEVICE_ATTR(chain_feedback_2, S_IRUGO | S_IWUSR, uio_pruss_fb_led_pru_chain_feedback_2_show,
               NULL);

static ssize_t uio_pruss_fb_led_pru_chain_feedback_3_show(struct device *dev,
						   struct device_attribute *attr,
						   char *buf)
{
	return sprintf(buf, "%d\n", pruss_chain_feedback_3);
}

static DEVICE_ATTR(chain_feedback_3, S_IRUGO | S_IWUSR, uio_pruss_fb_led_pru_chain_feedback_3_show,
               NULL);

static ssize_t uio_pruss_fb_led_pru_conf_regs_0_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	u16 i = 0;
        ssize_t len = 0;

	len += sprintf(&buf[len], "Chain 0:\n");
	while ( (i < CONF_REGS_BUFFER_SIZE) &&
		(pru0_conf_regs_0[i] != 0xBEEF) && 
		(pru0_conf_regs_0[i] != 0) )
	{
		len += sprintf(&buf[len], "\t[chip%d] 0x%04x\n", i, pru0_conf_regs_0[i]);
		i++;
	}

	i = 0;
	len += sprintf(&buf[len], "Chain 1:\n");
	while ( (i < CONF_REGS_BUFFER_SIZE) &&
		(pru0_conf_regs_1[i] != 0xBEEF) && 
		(pru0_conf_regs_1[i] != 0) )
	{
		len += sprintf(&buf[len], "0x%04x\n", pru0_conf_regs_1[i]);
		i++;
	}
	
	return len;
}

static DEVICE_ATTR(led_conf_regs_pru0, S_IRUGO | S_IWUSR, uio_pruss_fb_led_pru_conf_regs_0_show,
               NULL);

static ssize_t uio_pruss_fb_led_pru_conf_regs_1_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	u16 i = 0;
        ssize_t len = 0;
	
	len += sprintf(&buf[len], "Chain 2:\n");
	while ( (i < CONF_REGS_BUFFER_SIZE) &&
		(pru1_conf_regs_0[i] != 0xBEEF) && 
		(pru1_conf_regs_0[i] != 0) )
	{
		len += sprintf(&buf[len], "\t[chip%d] 0x%04x\n", i, pru1_conf_regs_0[i]);
		i++;
	}

	i = 0;
	len += sprintf(&buf[len], "Chain 3:\n");
	while ( (i < CONF_REGS_BUFFER_SIZE) &&
		(pru1_conf_regs_1[i] != 0xBEEF) &&
		(pru1_conf_regs_1[i] != 0) )

	{
		len += sprintf(&buf[len], "0x%04x\n", pru1_conf_regs_1[i]);
		i++;
	}

	return len;
}

static DEVICE_ATTR(led_conf_regs_pru1, S_IRUGO | S_IWUSR, uio_pruss_fb_led_pru_conf_regs_1_show,
               NULL);

static ssize_t uio_pruss_fb_led_pru_cfg_regs_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
        ssize_t len = 0;
	unsigned int cfg_register = prussdrv_pru_get_cfg_syscfg_reg(data);

	len += sprintf(&buf[len], "PRUSS Config:\n");
	len += sprintf(&buf[len], "\t0x%08x\n", cfg_register);

	if (cfg_register & 0x0003)
		len += sprintf(&buf[len], "\tIDLE_MODE: Reserved\n");
	else if (cfg_register & 0x0002)
		len += sprintf(&buf[len], "\tIDLE_MODE: Smart-idle mode\n");
	else if (cfg_register & 0x0001)
		len += sprintf(&buf[len], "\tIDLE_MODE: No-idle mode\n");
	else
		len += sprintf(&buf[len], "\tIDLE_MODE: Force-idle mode\n");
	if (cfg_register & 0x000C)
		len += sprintf(&buf[len], "\tSTANDBY_MODE: Reserved\n");
	else if (cfg_register & 0x0008)
		len += sprintf(&buf[len], "\tSTANDBY_MODE: Smart standby mode\n");
	else if (cfg_register & 0x0004)
		len += sprintf(&buf[len], "\tSTANDBY_MODE: No standby mode\n");
	else
		len += sprintf(&buf[len], "\tSTANDY_MODE: Force standby mode\n");

	if (cfg_register & 0x0010)
		len += sprintf(&buf[len], "\tSTANDBY_INIT: Initial standby sequence\n");
	else
		len += sprintf(&buf[len], "\tSTANDBY_INIT: Enable OCP master ports\n");

	if (cfg_register & 0x0020)
		len += sprintf(&buf[len], "\tSUB_MWAIT: Wait until 0\n");
	else
		len += sprintf(&buf[len], "\tSUB_MWAIT: Ready for Transaction\n");

	return len;
}
static DEVICE_ATTR(led_cfg_regs_pru, S_IRUGO | S_IWUSR, uio_pruss_fb_led_pru_cfg_regs_show,
               NULL);
static ssize_t uio_pruss_fb_led_pru_control_regs_0_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
        ssize_t len = 0;
	unsigned int control_register = prussdrv_pru_get_controlreg(PRU_NUM0, data);

	len += sprintf(&buf[len], "PRU 0:\n");
	len += sprintf(&buf[len], "\t0x%08x\n", control_register);
	if (control_register & 0x0001)
		len += sprintf(&buf[len], "\tSOFT_RST_N: RUN\n");
	else
		len += sprintf(&buf[len], "\tSOFT_RST_N: RESET\n");
	if (control_register & 0x0002)
		len += sprintf(&buf[len], "\tEN: Enabled\n");
	else
		len += sprintf(&buf[len], "\tEN: Disabled\n");
	if (control_register & 0x0004)
		len += sprintf(&buf[len], "\tSLEEPING: ASLEEP\n");
	else
		len += sprintf(&buf[len], "\tSLEEPING: NOT ASLEEP\n");
	if (control_register & 0x0008)
		len += sprintf(&buf[len], "\tCTR_EN: Counters enabled\n");
	else
		len += sprintf(&buf[len], "\tCTR_EN: Counters disabled\n");
	if (control_register & 0x0100)
		len += sprintf(&buf[len], "\tSINGLE_STEP: SINGLE\n");
	else
		len += sprintf(&buf[len], "\tSINGLE_STEP: FREE RUN\n");
	if (control_register & 0x8000)
		len += sprintf(&buf[len], "\tRUNSTATE: RUNNING\n");
	else
		len += sprintf(&buf[len], "\tRUNSTATE: HALTED\n");
	len += sprintf(&buf[len], "\tPC: 0x%04x\n", (control_register >> 16));

	return len;
}
static DEVICE_ATTR(led_control_regs_pru0, S_IRUGO | S_IWUSR, uio_pruss_fb_led_pru_control_regs_0_show,
               NULL);

static ssize_t uio_pruss_fb_led_pru_debug_0_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
        ssize_t len = 0;
	unsigned int control_register = prussdrv_pru_get_controlreg(PRU_NUM0, data);

	len += sprintf(&buf[len], "PRU 0:\n");
	if (control_register & 0x0100)
		len += sprintf(&buf[len], "\tSINGLE_STEP: SINGLE\n");
	else
		len += sprintf(&buf[len], "\tSINGLE_STEP: FREE RUN\n");
	return len;
}

static ssize_t uio_pruss_fb_led_pru_debug_0_store(struct device *dev,
					      struct device_attribute *attr,
					      const char *buf, size_t len)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
	unsigned int *control_register = prussdrv_pru_get_controlreg_pointer(PRU_NUM0, data);

	int tmp = 0;

	sscanf(buf, "%d", &tmp);

	if (tmp == 0)
	{
		*control_register = (*control_register & 0xFFFFFEFF) | 0x2;
	} else {
		*control_register = (*control_register | 0x100);
	}
	return len;

}
static DEVICE_ATTR(led_debug_pru0, S_IRUGO | S_IWUSR,
			uio_pruss_fb_led_pru_debug_0_show,
			uio_pruss_fb_led_pru_debug_0_store);

static ssize_t uio_pruss_fb_led_pru_registers_0_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
        ssize_t len = 0;
	unsigned int *debug_register_base = prussdrv_pru_get_debug_register_base(PRU_NUM0, data);
	int i = 0;

	len += sprintf(&buf[len], "PRU 0 Registers:\n");

	for (i = 0; i < 31; i++) {
		len += sprintf(&buf[len], "\tR%d: %08x\n", i, *debug_register_base );
		debug_register_base++;
	}

	return len;
}
static DEVICE_ATTR(led_pru0_show_registers, S_IRUGO | S_IWUSR,
			uio_pruss_fb_led_pru_registers_0_show,
			NULL);

static ssize_t uio_pruss_fb_led_pru_status_regs_0_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
        ssize_t len = 0;
	unsigned int control_register = prussdrv_pru_get_statusreg(PRU_NUM0, data);

	len += sprintf(&buf[len], "PRU 0:\n");
	len += sprintf(&buf[len], "\t0x%08x\n", control_register);
	len += sprintf(&buf[len], "\tPC: 0x%04x\n", (control_register & 0xFFFF));

	return len;
}
static DEVICE_ATTR(led_status_regs_pru0, S_IRUGO | S_IWUSR, uio_pruss_fb_led_pru_status_regs_0_show,
               NULL);

static ssize_t uio_pruss_fb_led_pru_cycle_regs_0_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
        ssize_t len = 0;
	unsigned int control_register = prussdrv_pru_get_cyclereg(PRU_NUM0, data);

	len += sprintf(&buf[len], "PRU 0:\n");
	len += sprintf(&buf[len], "\t0x%08x\n", control_register);

	return len;
}
static DEVICE_ATTR(led_cycle_regs_pru0, S_IRUGO | S_IWUSR, uio_pruss_fb_led_pru_cycle_regs_0_show,
               NULL);

static ssize_t uio_pruss_fb_led_pru_stall_regs_0_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
        ssize_t len = 0;
	unsigned int control_register = prussdrv_pru_get_stallreg(PRU_NUM0, data);

	len += sprintf(&buf[len], "PRU 0:\n");
	len += sprintf(&buf[len], "\t0x%08x\n", control_register);

	return len;
}
static DEVICE_ATTR(led_stall_regs_pru0, S_IRUGO | S_IWUSR, uio_pruss_fb_led_pru_stall_regs_0_show,
               NULL);



static unsigned int get_bitplane_offset(int bitplane_number)
{
	//printk(KERN_INFO "get offset: %d\n", bitplane_number);
	/** Adjust bitplane number from 4 - 7 to 0 - 3 
         *  This is needed because bitplane numbers in filesystem
         *  level are from 0 to 7. */
	if (bitplane_number > 3)
		bitplane_number -= 4;
	//printk(KERN_INFO "get offset 2: %d\n", bitplane_number);

	if (bitplane_number == 0) return BITPLANE0_CONFIG_OFFSET;
	if (bitplane_number == 1) return BITPLANE1_CONFIG_OFFSET;
	if (bitplane_number == 2) return BITPLANE2_CONFIG_OFFSET;
	if (bitplane_number == 3) return BITPLANE3_CONFIG_OFFSET;
	printk(KERN_INFO "FATAL: bitplane number is out of range.\n");
	return 0;
}

static void read_bitplane_data(int bitplane_number, u8 config_size, tprussdrv* data)
{
	if (bitplane_number < 4) {
		uio_pruss_fb_led_pru0_ram_read_data_u32( get_bitplane_offset(bitplane_number),
				    (u32 *)(&uio_pruss_fb_bitplane_configs[bitplane_number]),
				    config_size, data);
	} else {
		uio_pruss_fb_led_pru1_ram_read_data_u32(
					get_bitplane_offset(bitplane_number),
				    (u32 *)(&uio_pruss_fb_bitplane_configs[bitplane_number]),
				    config_size, data);
	}
}

static void write_bitplane_data(int bitplane_number, u8 config_size, tprussdrv* data)
{
	if (bitplane_number < 4) {
		uio_pruss_fb_led_pru0_ram_write_data_u32( get_bitplane_offset(bitplane_number),
				    (u32 *)(&uio_pruss_fb_bitplane_configs[bitplane_number]),
				    config_size, data);
	} else {
		uio_pruss_fb_led_pru1_ram_write_data_u32( get_bitplane_offset(bitplane_number),
				    (u32 *)(&uio_pruss_fb_bitplane_configs[bitplane_number]),
				    config_size, data);
	}
}

#define CREATE_CHAIN_ATTRIBUTE_RO(attrname, bitplane) \
static ssize_t uio_pruss_fb_led_pru_##attrname##_##bitplane##_show(struct device *dev, \
                              struct device_attribute *attr, \
                              char *buf) \
{ \
	u8 config_size = sizeof(struct uio_pruss_fb_led_bitplane_config)/sizeof(u32); \
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);\
\
	read_bitplane_data(bitplane, config_size, data); \
	return sprintf(buf, "%#x\n", uio_pruss_fb_bitplane_configs[bitplane].attrname); \
} \
static DEVICE_ATTR(attrname##_##bitplane, S_IRUGO | S_IWUSR, \
		uio_pruss_fb_led_pru_##attrname##_##bitplane##_show, NULL);
		
#define CREATE_CHAIN_ATTRIBUTE_PTR_RO(attrname, bitplane) \
static ssize_t uio_pruss_fb_led_pru_##attrname##_##bitplane##_show(struct device *dev, \
                              struct device_attribute *attr, \
                              char *buf) \
{ \
	u8 config_size = sizeof(struct uio_pruss_fb_led_bitplane_config)/sizeof(u32); \
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);\
\
	read_bitplane_data(bitplane, config_size, data); \
	return sprintf(buf, "%p\n", uio_pruss_fb_bitplane_configs[bitplane].attrname); \
} \
static DEVICE_ATTR(attrname##_##bitplane, S_IRUGO | S_IWUSR, \
		uio_pruss_fb_led_pru_##attrname##_##bitplane##_show, NULL);


#define CREATE_CHAIN_ATTRIBUTE_RW(attrname, bitplane) \
static ssize_t uio_pruss_fb_led_pru_##attrname##_##bitplane##_show(struct device *dev, \
                              struct device_attribute *attr, \
                              char *buf) \
{ \
	u8 config_size = sizeof(struct uio_pruss_fb_led_bitplane_config)/sizeof(u32); \
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);\
\
	read_bitplane_data(bitplane, config_size, data); \
	return sprintf(buf, "%#x\n", uio_pruss_fb_bitplane_configs[bitplane].attrname); \
} \
static ssize_t uio_pruss_fb_led_pru_##attrname##_##bitplane##_store(struct device *dev, \
                               struct device_attribute *attr, \
                               const char *buf, size_t len) \
{ \
	u8 config_size = sizeof(struct uio_pruss_fb_led_bitplane_config)/sizeof(u32); \
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);\
\
	sscanf(buf, "0x%x", (u32 *)&uio_pruss_fb_bitplane_configs[bitplane].attrname); \
\
	write_bitplane_data(bitplane, config_size, data); \
	return len; \
} \
static DEVICE_ATTR(attrname##_##bitplane, S_IRUGO | S_IWUSR, \
		uio_pruss_fb_led_pru_##attrname##_##bitplane##_show, \
		uio_pruss_fb_led_pru_##attrname##_##bitplane##_store);
		
		
#define CREATE_CHAIN_ATTRIBUTE_U16_RW(attrname, bitplane) \
static ssize_t uio_pruss_fb_led_pru_##attrname##_##bitplane##_show(struct device *dev, \
                              struct device_attribute *attr, \
                              char *buf) \
{ \
	u8 config_size = sizeof(struct uio_pruss_fb_led_bitplane_config)/sizeof(u32); \
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);\
\
	read_bitplane_data(bitplane, config_size, data); \
	return sprintf(buf, "%#x\n", uio_pruss_fb_bitplane_configs[bitplane].attrname); \
} \
static ssize_t uio_pruss_fb_led_pru_##attrname##_##bitplane##_store(struct device *dev, \
                               struct device_attribute *attr, \
                               const char *buf, size_t len) \
{ \
	u8 config_size = sizeof(struct uio_pruss_fb_led_bitplane_config)/sizeof(u32); \
	u32 value = 0;\
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev); \
\
	sscanf(buf, "0x%x", &value); \
	uio_pruss_fb_bitplane_configs[bitplane].attrname = value;\
	write_bitplane_data(bitplane, config_size, data); \
	return len; \
} \
static DEVICE_ATTR(attrname##_##bitplane, S_IRUGO | S_IWUSR, \
		uio_pruss_fb_led_pru_##attrname##_##bitplane##_show, \
		uio_pruss_fb_led_pru_##attrname##_##bitplane##_store);
		
		
#define CREATE_CHAIN_ATTRIBUTE_U8_RW(attrname, bitplane) \
static ssize_t uio_pruss_fb_led_pru_##attrname##_##bitplane##_show(struct device *dev, \
                              struct device_attribute *attr, \
                              char *buf) \
{ \
	u8 config_size = sizeof(struct uio_pruss_fb_led_bitplane_config)/sizeof(u32); \
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev); \
\
	read_bitplane_data(bitplane, config_size, data); \
	return sprintf(buf, "%#x\n", uio_pruss_fb_bitplane_configs[bitplane].attrname); \
} \
static ssize_t uio_pruss_fb_led_pru_##attrname##_##bitplane##_store(struct device *dev, \
                               struct device_attribute *attr, \
                               const char *buf, size_t len) \
{ \
	u8 config_size = sizeof(struct uio_pruss_fb_led_bitplane_config)/sizeof(u32); \
	u32 value = 0;\
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev); \
\
	sscanf(buf, "0x%x", &value); \
	uio_pruss_fb_bitplane_configs[bitplane].attrname = value;\
	write_bitplane_data(bitplane, config_size, data); \
	return len; \
} \
static DEVICE_ATTR(attrname##_##bitplane, S_IRUGO | S_IWUSR, \
		uio_pruss_fb_led_pru_##attrname##_##bitplane##_show, \
		uio_pruss_fb_led_pru_##attrname##_##bitplane##_store);
		

// lpp01_bitplane_configs[chain].attrname = simple_strtol(buf, NULL, 10);

// Bitplane 0 (Chain 0, frame 0)
CREATE_CHAIN_ATTRIBUTE_PTR_RO(data_pointer, 0);
CREATE_CHAIN_ATTRIBUTE_RW(latch_on, 0);
CREATE_CHAIN_ATTRIBUTE_RW(next_sync, 0);
CREATE_CHAIN_ATTRIBUTE_RO(shift_reg_out, 0);
CREATE_CHAIN_ATTRIBUTE_RO(shift_reg_in, 0);
CREATE_CHAIN_ATTRIBUTE_U16_RW(number_of_frames, 0);
CREATE_CHAIN_ATTRIBUTE_U16_RW(number_of_chain_leds, 0);
CREATE_CHAIN_ATTRIBUTE_U16_RW(led_bit_counter, 0);
CREATE_CHAIN_ATTRIBUTE_U8_RW(latch_length, 0);
CREATE_CHAIN_ATTRIBUTE_U8_RW(last_address, 0);
CREATE_CHAIN_ATTRIBUTE_U8_RW(bit_configs, 0);
CREATE_CHAIN_ATTRIBUTE_U8_RW(word_width_bits, 0);
CREATE_CHAIN_ATTRIBUTE_RO(ii, 0);
CREATE_CHAIN_ATTRIBUTE_U8_RW(address_counter, 0);

// Bitplane 1 (Chain 0, frame 1)
CREATE_CHAIN_ATTRIBUTE_PTR_RO(data_pointer, 1);
CREATE_CHAIN_ATTRIBUTE_RW(latch_on, 1);
CREATE_CHAIN_ATTRIBUTE_RW(next_sync, 1);
CREATE_CHAIN_ATTRIBUTE_RO(shift_reg_out, 1);
CREATE_CHAIN_ATTRIBUTE_RO(shift_reg_in, 1);
CREATE_CHAIN_ATTRIBUTE_U16_RW(number_of_frames, 1);
CREATE_CHAIN_ATTRIBUTE_U16_RW(number_of_chain_leds, 1);
CREATE_CHAIN_ATTRIBUTE_U16_RW(led_bit_counter, 1);
CREATE_CHAIN_ATTRIBUTE_U8_RW(latch_length, 1);
CREATE_CHAIN_ATTRIBUTE_U8_RW(last_address, 1);
CREATE_CHAIN_ATTRIBUTE_U8_RW(bit_configs, 1);
CREATE_CHAIN_ATTRIBUTE_U8_RW(word_width_bits, 1);
CREATE_CHAIN_ATTRIBUTE_RO(ii, 1);
CREATE_CHAIN_ATTRIBUTE_U8_RW(address_counter, 1);

// Bitplane 2 (Chain 1, frame 0)
CREATE_CHAIN_ATTRIBUTE_PTR_RO(data_pointer, 2);
CREATE_CHAIN_ATTRIBUTE_RW(latch_on, 2);
CREATE_CHAIN_ATTRIBUTE_RW(next_sync, 2);
CREATE_CHAIN_ATTRIBUTE_RO(shift_reg_out, 2);
CREATE_CHAIN_ATTRIBUTE_RO(shift_reg_in, 2);
CREATE_CHAIN_ATTRIBUTE_U16_RW(number_of_frames, 2);
CREATE_CHAIN_ATTRIBUTE_U16_RW(number_of_chain_leds, 2);
CREATE_CHAIN_ATTRIBUTE_U16_RW(led_bit_counter, 2);
CREATE_CHAIN_ATTRIBUTE_U8_RW(latch_length, 2);
CREATE_CHAIN_ATTRIBUTE_U8_RW(last_address, 2);
CREATE_CHAIN_ATTRIBUTE_U8_RW(bit_configs, 2);
CREATE_CHAIN_ATTRIBUTE_U8_RW(word_width_bits, 2);
CREATE_CHAIN_ATTRIBUTE_RO(ii, 2);
CREATE_CHAIN_ATTRIBUTE_U8_RW(address_counter, 2);

// Bitplane 3 (Chain 1, frame 1)
CREATE_CHAIN_ATTRIBUTE_PTR_RO(data_pointer, 3);
CREATE_CHAIN_ATTRIBUTE_RW(latch_on, 3);
CREATE_CHAIN_ATTRIBUTE_RW(next_sync, 3);
CREATE_CHAIN_ATTRIBUTE_RO(shift_reg_out, 3);
CREATE_CHAIN_ATTRIBUTE_RO(shift_reg_in, 3);
CREATE_CHAIN_ATTRIBUTE_U16_RW(number_of_frames, 3);
CREATE_CHAIN_ATTRIBUTE_U16_RW(number_of_chain_leds, 3);
CREATE_CHAIN_ATTRIBUTE_U16_RW(led_bit_counter, 3);
CREATE_CHAIN_ATTRIBUTE_U8_RW(latch_length, 3);
CREATE_CHAIN_ATTRIBUTE_U8_RW(last_address, 3);
CREATE_CHAIN_ATTRIBUTE_U8_RW(bit_configs, 3);
CREATE_CHAIN_ATTRIBUTE_U8_RW(word_width_bits, 3);
CREATE_CHAIN_ATTRIBUTE_RO(ii, 3);
CREATE_CHAIN_ATTRIBUTE_U8_RW(address_counter, 3);

// Bitplane 4 (Chain 2, frame 0)
CREATE_CHAIN_ATTRIBUTE_PTR_RO(data_pointer, 4);
CREATE_CHAIN_ATTRIBUTE_RW(latch_on, 4);
CREATE_CHAIN_ATTRIBUTE_RW(next_sync, 4);
CREATE_CHAIN_ATTRIBUTE_RO(shift_reg_out, 4);
CREATE_CHAIN_ATTRIBUTE_RO(shift_reg_in, 4);
CREATE_CHAIN_ATTRIBUTE_U16_RW(number_of_frames, 4);
CREATE_CHAIN_ATTRIBUTE_U16_RW(number_of_chain_leds, 4);
CREATE_CHAIN_ATTRIBUTE_U16_RW(led_bit_counter, 4);
CREATE_CHAIN_ATTRIBUTE_U8_RW(latch_length, 4);
CREATE_CHAIN_ATTRIBUTE_U8_RW(last_address, 4);
CREATE_CHAIN_ATTRIBUTE_U8_RW(bit_configs, 4);
CREATE_CHAIN_ATTRIBUTE_U8_RW(word_width_bits, 4);
CREATE_CHAIN_ATTRIBUTE_RO(ii, 4);
CREATE_CHAIN_ATTRIBUTE_U8_RW(address_counter, 4);

// Bitplane 5 (Chain 2, frame 1)
CREATE_CHAIN_ATTRIBUTE_PTR_RO(data_pointer, 5);
CREATE_CHAIN_ATTRIBUTE_RW(latch_on, 5);
CREATE_CHAIN_ATTRIBUTE_RW(next_sync, 5);
CREATE_CHAIN_ATTRIBUTE_RO(shift_reg_out, 5);
CREATE_CHAIN_ATTRIBUTE_RO(shift_reg_in, 5);
CREATE_CHAIN_ATTRIBUTE_U16_RW(number_of_frames, 5);
CREATE_CHAIN_ATTRIBUTE_U16_RW(number_of_chain_leds, 5);
CREATE_CHAIN_ATTRIBUTE_U16_RW(led_bit_counter, 5);
CREATE_CHAIN_ATTRIBUTE_U8_RW(latch_length, 5);
CREATE_CHAIN_ATTRIBUTE_U8_RW(last_address, 5);
CREATE_CHAIN_ATTRIBUTE_U8_RW(bit_configs, 5);
CREATE_CHAIN_ATTRIBUTE_U8_RW(word_width_bits, 5);
CREATE_CHAIN_ATTRIBUTE_RO(ii, 5);
CREATE_CHAIN_ATTRIBUTE_U8_RW(address_counter, 5);

// Bitplane 6 (Chain 3, frame 0)
CREATE_CHAIN_ATTRIBUTE_PTR_RO(data_pointer, 6);
CREATE_CHAIN_ATTRIBUTE_RW(latch_on, 6);
CREATE_CHAIN_ATTRIBUTE_RW(next_sync, 6);
CREATE_CHAIN_ATTRIBUTE_RO(shift_reg_out, 6);
CREATE_CHAIN_ATTRIBUTE_RO(shift_reg_in, 6);
CREATE_CHAIN_ATTRIBUTE_U16_RW(number_of_frames, 6);
CREATE_CHAIN_ATTRIBUTE_U16_RW(number_of_chain_leds, 6);
CREATE_CHAIN_ATTRIBUTE_U16_RW(led_bit_counter, 6);
CREATE_CHAIN_ATTRIBUTE_U8_RW(latch_length, 6);
CREATE_CHAIN_ATTRIBUTE_U8_RW(last_address, 6);
CREATE_CHAIN_ATTRIBUTE_U8_RW(bit_configs, 6);
CREATE_CHAIN_ATTRIBUTE_U8_RW(word_width_bits, 6);
CREATE_CHAIN_ATTRIBUTE_RO(ii, 6);
CREATE_CHAIN_ATTRIBUTE_U8_RW(address_counter, 6);

// Bitplane 7 (Chain 3, frame 1)
CREATE_CHAIN_ATTRIBUTE_PTR_RO(data_pointer, 7);
CREATE_CHAIN_ATTRIBUTE_RW(latch_on, 7);
CREATE_CHAIN_ATTRIBUTE_RW(next_sync, 7);
CREATE_CHAIN_ATTRIBUTE_RO(shift_reg_out, 7);
CREATE_CHAIN_ATTRIBUTE_RO(shift_reg_in, 7);
CREATE_CHAIN_ATTRIBUTE_U16_RW(number_of_frames, 7);
CREATE_CHAIN_ATTRIBUTE_U16_RW(number_of_chain_leds, 7);
CREATE_CHAIN_ATTRIBUTE_U16_RW(led_bit_counter, 7);
CREATE_CHAIN_ATTRIBUTE_U8_RW(latch_length, 7);
CREATE_CHAIN_ATTRIBUTE_U8_RW(last_address, 7);
CREATE_CHAIN_ATTRIBUTE_U8_RW(bit_configs, 7);
CREATE_CHAIN_ATTRIBUTE_U8_RW(word_width_bits, 7);
CREATE_CHAIN_ATTRIBUTE_RO(ii, 7);
CREATE_CHAIN_ATTRIBUTE_U8_RW(address_counter, 7);

static const struct attribute *uio_pruss_fb_led_pru_attrs[] = {
	&dev_attr_run.attr,
	&dev_attr_run_0.attr,
	&dev_attr_run_1.attr,
	&dev_attr_run_led_diagnostics.attr,
	&dev_attr_led_errors_count.attr,
	&dev_attr_not_connected_errors_count.attr,
	&dev_attr_chain_feedback_0.attr,
	&dev_attr_chain_feedback_1.attr,
	&dev_attr_chain_feedback_2.attr,
	&dev_attr_chain_feedback_3.attr,
	&dev_attr_led_conf_regs_pru0.attr,
	&dev_attr_led_conf_regs_pru1.attr,
	&dev_attr_led_cfg_regs_pru.attr,
	&dev_attr_led_debug_pru0.attr,
	&dev_attr_led_pru0_show_registers.attr,
	&dev_attr_led_control_regs_pru0.attr,
	&dev_attr_led_status_regs_pru0.attr,
	&dev_attr_led_cycle_regs_pru0.attr,
	&dev_attr_led_stall_regs_pru0.attr,
	// Chain 0, frame 0
	&dev_attr_data_pointer_0.attr,
	&dev_attr_latch_on_0.attr,
	&dev_attr_next_sync_0.attr,
	&dev_attr_shift_reg_out_0.attr,
	&dev_attr_shift_reg_in_0.attr,
	&dev_attr_number_of_frames_0.attr,
	&dev_attr_number_of_chain_leds_0.attr,
	&dev_attr_led_bit_counter_0.attr,
	&dev_attr_latch_length_0.attr,
	&dev_attr_last_address_0.attr,
	&dev_attr_bit_configs_0.attr,
	&dev_attr_word_width_bits_0.attr,
	&dev_attr_ii_0.attr,
	&dev_attr_address_counter_0.attr,
	// Chain 0, frame 1
	&dev_attr_data_pointer_1.attr,
	&dev_attr_latch_on_1.attr,
	&dev_attr_next_sync_1.attr,
	&dev_attr_shift_reg_out_1.attr,
	&dev_attr_shift_reg_in_1.attr,
	&dev_attr_number_of_frames_1.attr,
	&dev_attr_number_of_chain_leds_1.attr,
	&dev_attr_led_bit_counter_1.attr,
	&dev_attr_latch_length_1.attr,
	&dev_attr_last_address_1.attr,
	&dev_attr_bit_configs_1.attr,
	&dev_attr_word_width_bits_1.attr,
	&dev_attr_ii_1.attr,
	&dev_attr_address_counter_1.attr,
	// Chain 1, frame 0
	&dev_attr_data_pointer_2.attr,
	&dev_attr_latch_on_2.attr,
	&dev_attr_next_sync_2.attr,
	&dev_attr_shift_reg_out_2.attr,
	&dev_attr_shift_reg_in_2.attr,
	&dev_attr_number_of_frames_2.attr,
	&dev_attr_number_of_chain_leds_2.attr,
	&dev_attr_led_bit_counter_2.attr,
	&dev_attr_latch_length_2.attr,
	&dev_attr_last_address_2.attr,
	&dev_attr_bit_configs_2.attr,
	&dev_attr_word_width_bits_2.attr,
	&dev_attr_ii_2.attr,
	&dev_attr_address_counter_2.attr,
	// Chain 1, frame 1
	&dev_attr_data_pointer_3.attr,
	&dev_attr_latch_on_3.attr,
	&dev_attr_next_sync_3.attr,
	&dev_attr_shift_reg_out_3.attr,
	&dev_attr_shift_reg_in_3.attr,
	&dev_attr_number_of_frames_3.attr,
	&dev_attr_number_of_chain_leds_3.attr,
	&dev_attr_led_bit_counter_3.attr,
	&dev_attr_latch_length_3.attr,
	&dev_attr_last_address_3.attr,
	&dev_attr_bit_configs_3.attr,
	&dev_attr_word_width_bits_3.attr,
	&dev_attr_ii_3.attr,
	&dev_attr_address_counter_3.attr,
	// Chain 2, frame 0
	&dev_attr_data_pointer_4.attr,
	&dev_attr_latch_on_4.attr,
	&dev_attr_next_sync_4.attr,
	&dev_attr_shift_reg_out_4.attr,
	&dev_attr_shift_reg_in_4.attr,
	&dev_attr_number_of_frames_4.attr,
	&dev_attr_number_of_chain_leds_4.attr,
	&dev_attr_led_bit_counter_4.attr,
	&dev_attr_latch_length_4.attr,
	&dev_attr_last_address_4.attr,
	&dev_attr_bit_configs_4.attr,
	&dev_attr_word_width_bits_4.attr,
	&dev_attr_ii_4.attr,
	&dev_attr_address_counter_4.attr,
	// Chain 2, frame 1
	&dev_attr_data_pointer_5.attr,
	&dev_attr_latch_on_5.attr,
	&dev_attr_next_sync_5.attr,
	&dev_attr_shift_reg_out_5.attr,
	&dev_attr_shift_reg_in_5.attr,
	&dev_attr_number_of_frames_5.attr,
	&dev_attr_number_of_chain_leds_5.attr,
	&dev_attr_led_bit_counter_5.attr,
	&dev_attr_latch_length_5.attr,
	&dev_attr_last_address_5.attr,
	&dev_attr_bit_configs_5.attr,
	&dev_attr_word_width_bits_5.attr,
	&dev_attr_ii_5.attr,
	&dev_attr_address_counter_5.attr,
	// Chain 3, frame 0
	&dev_attr_data_pointer_6.attr,
	&dev_attr_latch_on_6.attr,
	&dev_attr_next_sync_6.attr,
	&dev_attr_shift_reg_out_6.attr,
	&dev_attr_shift_reg_in_6.attr,
	&dev_attr_number_of_frames_6.attr,
	&dev_attr_number_of_chain_leds_6.attr,
	&dev_attr_led_bit_counter_6.attr,
	&dev_attr_latch_length_6.attr,
	&dev_attr_last_address_6.attr,
	&dev_attr_bit_configs_6.attr,
	&dev_attr_word_width_bits_6.attr,
	&dev_attr_ii_6.attr,
	&dev_attr_address_counter_6.attr,
	// Chain 3, frame 1
	&dev_attr_data_pointer_7.attr,
	&dev_attr_latch_on_7.attr,
	&dev_attr_next_sync_7.attr,
	&dev_attr_shift_reg_out_7.attr,
	&dev_attr_shift_reg_in_7.attr,
	&dev_attr_number_of_frames_7.attr,
	&dev_attr_number_of_chain_leds_7.attr,
	&dev_attr_led_bit_counter_7.attr,
	&dev_attr_latch_length_7.attr,
	&dev_attr_last_address_7.attr,
	&dev_attr_bit_configs_7.attr,
	&dev_attr_word_width_bits_7.attr,
	&dev_attr_ii_7.attr,
	&dev_attr_address_counter_7.attr,
        NULL,
};

static const struct attribute_group pruss_device_attr_group = {
        .attrs = (struct attribute **) uio_pruss_fb_led_pru_attrs,
};

int uio_pruss_fb_led_create_sysfs_pruss_nodes(struct platform_device *pdev)
{
	int retval = -ENOMEM;
	
	/* Create the sysfs interface for the driver */
	retval = sysfs_create_group(&pdev->dev.kobj, &pruss_device_attr_group);
	
	return retval;
}
EXPORT_SYMBOL(uio_pruss_fb_led_create_sysfs_pruss_nodes);

void uio_pruss_fb_led_remove_sysfs_pruss_nodes(struct platform_device *pdev)
{
	sysfs_remove_group(&pdev->dev.kobj, &pruss_device_attr_group);
}
EXPORT_SYMBOL(uio_pruss_fb_led_remove_sysfs_pruss_nodes);

int uio_pruss_fb_led_load_fw(struct platform_device *pdev, tprussdrv *data)
{
	const struct firmware *fw0, *fw1;
	int retval = -ENODEV;
	
	retval = request_firmware(&fw0, FW0_NAME, &pdev->dev);
	if (retval < 0) {
	    dev_err(&pdev->dev, "error requesting firmware (%d)\n", retval);
	    return retval;
	}
	dev_info(&pdev->dev, "%s (PRU0) size %td downloading...\n", FW0_NAME, fw0->size);
    
	retval = request_firmware(&fw1, FW1_NAME, &pdev->dev);
	if (retval < 0) {
	    dev_err(&pdev->dev, "error requesting firmware (%d)\n", retval);
	    goto err1;
	}
	dev_info(&pdev->dev, "%s (PRU1) size %td downloading...\n", FW1_NAME, fw1->size);
	
	/*
	* pru_load returns
	*   0: OK
	*   1: failure
	* It looks like the pru_lib is not developed using kernel types.
	*/
	retval = pru_load(PRU_NUM0, (unsigned int *)fw0->data,
                           fw0->size, data);

	if (retval) {
	    retval = -ENXIO;
	    dev_err(&pdev->dev, "cannot load firmware (%d)\n", retval);
	    goto err2;
	}

	retval = pru_load(PRU_NUM1, (unsigned int *)fw1->data,
                          fw1->size, data);
//			(fw1->size/sizeof(unsigned int)), &data->pru_iomap);
	if (retval) {
	    retval = -ENXIO;
	    dev_err(&pdev->dev, "cannot load firmware (%d)\n", retval);
	    goto err2;
	}
    
	/* User must start the PRUSS */
	data->pruss_running = 0;
	
	release_firmware(fw0);
	release_firmware(fw1);
	
	return 0;
	
err2:
	release_firmware(fw1);
err1:
	release_firmware(fw0);
	
	return retval;

}
EXPORT_SYMBOL(uio_pruss_fb_led_load_fw);

int uio_pruss_fb_led_load_diagnostics_fw(struct platform_device *pdev, tprussdrv *data)
{
	const struct firmware *fw0, *fw1;
	int retval = -ENODEV;

	retval = request_firmware(&fw0, FW0_LED_DIAG_NAME, &pdev->dev);
	if (retval < 0) {
	    dev_err(&pdev->dev, "error requesting firmware (%d)\n", retval);
	    return retval;
	}
	dev_info(&pdev->dev, "%s (PRU0) size %td downloading...\n", FW0_LED_DIAG_NAME, fw0->size);
    
	retval = request_firmware(&fw1, FW1_LED_DIAG_NAME, &pdev->dev);
	if (retval < 0) {
	    dev_err(&pdev->dev, "error requesting firmware (%d)\n", retval);
	    goto err1;
	}
	dev_info(&pdev->dev, "%s (PRU1) size %td downloading...\n", FW1_LED_DIAG_NAME, fw1->size);
	
	/*
	* pru_load returns
	*   0: OK
	*   1: failure
	* It looks like the pru_lib is not developed using kernel types.
	*/
	retval = pru_load(PRU_NUM0, (unsigned int *)fw0->data, fw0->size, data);
	if (retval) {
	    retval = -ENXIO;
	    dev_err(&pdev->dev, "cannot load firmware (%d)\n", retval);
	    goto err2;
	}

	retval = pru_load(PRU_NUM1, (unsigned int *)fw1->data, fw1->size, data);
	if (retval) {
	    retval = -ENXIO;
	    dev_err(&pdev->dev, "cannot load firmware (%d)\n", retval);
	    goto err2;
	}
	
	release_firmware(fw0);
	release_firmware(fw1);
	
	return 0;
	
err2:
	release_firmware(fw1);
err1:
	release_firmware(fw0);
	
	return retval;
}
EXPORT_SYMBOL(uio_pruss_fb_led_load_diagnostics_fw);

int uio_pruss_fb_led_wait_frame_sync(void)
{
    if (wait_event_interruptible(pru0_wait_queue, (pru0_fr_done == 1)))
	return -ERESTARTSYS;
    pru0_fr_done = 0;

    return 0;
}
EXPORT_SYMBOL(uio_pruss_fb_led_wait_frame_sync);


#if 0



/*
0x01C30000 - 0x01C301FF Data RAM 0
0x01C30200 - 0x01C31FFF Reserved
0x01C32000 - 0x01C321FF Data RAM 1
0x01C32200 - 0x01C33FFF Reserved
0x01C34000 - 0x01C36FFF INTC Registers
0x01C37000 - 0x01C373FF PRU0 Control Registers
0x01C37400 - 0x01C377FF PRU0 Debug Registers
0x01C37800 - 0x01C37BFF PRU1 Control Registers
0x01C37C00 - 0x01C37FFF PRU1 Debug Registers
0x01C38000 - 0x01C38FFF PRU0 Instruction RAM
0x01C39000 - 0x01C3BFFF Reserved
0x01C3C000 - 0x01C3CFFF PRU1 Instruction RAM
0x01C3D000 - 0x01C3FFFF Reserved
*/
/*
 * 3 PRU_EVTOUT0 PRUSS Interrupt
 * 4 PRU_EVTOUT1 PRUSS Interrupt
 * 5 PRU_EVTOUT2 PRUSS Interrupt
 * 6 PRU_EVTOUT3 PRUSS Interrupt
 * 7 PRU_EVTOUT4 PRUSS Interrupt
 * 8 PRU_EVTOUT5 PRUSS Interrupt
 * 9 PRU_EVTOUT6 PRUSS Interrupt
 * 10 PRU_EVTOUT7 PRUSS Interrupt
*/

static irqreturn_t lpp01_led_pruss_irq(int irq, void *dev_id)
{
	u32 u32_reg_val = 0;
	u32 u32_ack_reg_val = 0;
	
	// SECR2
	lpp01_led_pru_ram_read_data_u32(0x00004000 + 0x284, (u32*)&u32_reg_val, 1, lpp01_pru_iomap);
	
	// SICR
	if (irq == 3)
	    u32_ack_reg_val = 34;
	else
	    u32_ack_reg_val = 35;
	lpp01_led_pru_ram_write_data_u32(0x00004000 + 0x24, (u32*)&u32_ack_reg_val, 1, lpp01_pru_iomap);
	
	pru0_fr_done = 1;
	wake_up_interruptible(&pru0_wait_queue);

	return IRQ_HANDLED;
}

/* Function that only initializes the PRU subsystem: clock, RAM... */
int lpp01_led_pruss_init(struct platform_device *pdev, struct lpp01_led_pru_data *data)
{
	int ret = -ENODEV;
	struct resource *regs_pru_base, *regs_l3ram, *regs_psc0, *regs_psc1;

	/* Power on PRU in case its not done as part of boot-loader */
	data->pruss_clk = clk_get(&pdev->dev, "pruss");
	if (IS_ERR(data->pruss_clk)) {
		printk(KERN_ERR	"%s: no PRUSS clock available\n", __func__);
		ret = PTR_ERR(data->pruss_clk);
		return ret;
	}
	data->pruss_clk_freq = clk_get_rate(data->pruss_clk);
	clk_enable(data->pruss_clk);

	/* See arch/arm/mach-davinci/lpp01.c for resources */
	regs_pru_base = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!regs_pru_base) {
		dev_err(&pdev->dev, "no memory resource specified for PRUs base\n");
		ret = -EBUSY;
		goto out1;
	}

	regs_l3ram = platform_get_resource(pdev, IORESOURCE_MEM, 1);
	if (!regs_l3ram) {
		dev_err(&pdev->dev, "no memory resource specified for L3RAM\n");
		ret = -EBUSY;
		goto out1;
	}
	
	regs_psc0 = platform_get_resource(pdev, IORESOURCE_MEM, 2);
	if (!regs_psc0) {
		dev_err(&pdev->dev, "no memory resource specified for PSC0\n");
		ret = -EBUSY;
		goto out1;
	}
	
	regs_psc1 = platform_get_resource(pdev, IORESOURCE_MEM, 3);
	if (!regs_psc1) {
		dev_err(&pdev->dev, "no memory resource specified for PSC1\n");
		ret = -EBUSY;
		goto out1;
	}
	
	if (!request_mem_region(regs_pru_base->start, resource_size(regs_pru_base),
			    dev_name(&pdev->dev))) {
		dev_err(&pdev->dev, "pru memory region already claimed!\n");
		ret = -EBUSY;
		goto out1;
	}
	
	if (!request_mem_region(regs_psc0->start, resource_size(regs_psc0),
			    dev_name(&pdev->dev))) {
		dev_err(&pdev->dev, "PSC0 memory region already claimed!\n");
		ret = -EBUSY;
		goto out2;
	}
	
	if (!request_mem_region(regs_psc1->start, resource_size(regs_psc1),
			    dev_name(&pdev->dev))) {
		dev_err(&pdev->dev, "PSC1 memory region already claimed!\n");
		ret = -EBUSY;
		goto out3;
	}
	
	//printk("regs_pru_base->start: 0x%x, regs_pru_base->end: 0x%x; size %d\n",
    //	   regs_pru_base->start, regs_pru_base->end, resource_size(regs_pru_base));
	data->pru_iomap.pru_io_addr = ioremap(regs_pru_base->start,
					    resource_size(regs_pru_base));
	if (!data->pru_iomap.pru_io_addr) {
		dev_err(&pdev->dev, "ioremap failed\n");
		ret = -ENOMEM;
		goto out4;
	}
	
	/* Reset the Data RAM of the PRUS */
	
	memset(data->pru_iomap.pru_io_addr + DATA_RAM_OFFSET_0, 0, DATA_RAM_SIZE_0);
	memset(data->pru_iomap.pru_io_addr + DATA_RAM_OFFSET_1, 0, DATA_RAM_SIZE_1);
	
	data->pru_iomap.syscfg_io_addr = IO_ADDRESS(DA8XX_SYSCFG0_BASE);
	if (!data->pru_iomap.syscfg_io_addr) {
		dev_err(&pdev->dev, " PRU ioremap failed\n");
		ret = -ENOMEM;
		goto out5;
	}
	
	data->pru_iomap.psc0_io_addr = ioremap(regs_psc0->start,
					    resource_size(regs_psc0));
	if (!data->pru_iomap.psc0_io_addr) {
		dev_err(&pdev->dev, "PSC0 ioremap failed\n");
		ret = -ENOMEM;
		goto out5;
	}
	
	data->pru_iomap.psc1_io_addr = ioremap(regs_psc1->start,
					    resource_size(regs_psc1));
	if (!data->pru_iomap.psc1_io_addr) {
		dev_err(&pdev->dev, "PSC1 ioremap failed\n");
		ret = -ENOMEM;
		goto out6;
	}

    //     printk("regs_l3ram->start: 0x%x, regs_l3ram->end: 0x%x; size %d\n",
    // 	   regs_l3ram->start, regs_l3ram->end, resource_size(regs_l3ram));
	dma_phys_addr = regs_l3ram->start;
	dma_vaddr_buff = ioremap(regs_l3ram->start,
				resource_size(regs_l3ram));
	if (!dma_vaddr_buff) {
		dev_err(&pdev->dev, "Failed to allocate shared ram.\n");
		ret = -EFAULT;
		goto out7;
	}
	
	data->pru_iomap.pFifoBufferPhysBase = (void *)dma_phys_addr;
	data->pru_iomap.pFifoBufferVirtBase = (void *)dma_vaddr_buff;
	
	/* Register two IRQs */
	ret = request_irq(platform_get_irq(pdev, 0), lpp01_led_pruss_irq, IRQF_SHARED, "pru0_fr_sync", (void *)data) ;
	if (ret < 0) {
		dev_err(&pdev->dev, "Failed to register %d.\n", platform_get_irq(pdev, 0));
		ret = -EFAULT;
		goto out7;
	}
	
	ret = request_irq(platform_get_irq(pdev, 1), lpp01_led_pruss_irq, IRQF_SHARED, "pru1_fr_sync", (void *)data) ;
	if (ret < 0) {
		dev_err(&pdev->dev, "Failed to register %d.\n", platform_get_irq(pdev, 1));
		ret = -EFAULT;
		goto out8;
	}
	
	return 0;
	
out8:
	free_irq(platform_get_irq(pdev, 1), data);
out7:
	iounmap(data->pru_iomap.psc1_io_addr);
out6:
	iounmap(data->pru_iomap.psc0_io_addr);
out5:
	iounmap(data->pru_iomap.pru_io_addr);
out4:
	release_mem_region(regs_pru_base->start, resource_size(regs_psc1));
out3:
	release_mem_region(regs_pru_base->start, resource_size(regs_psc0));
out2:
	release_mem_region(regs_pru_base->start, resource_size(regs_pru_base));
out1:
	clk_put(data->pruss_clk);
	clk_disable(data->pruss_clk);
	return ret;
}
EXPORT_SYMBOL(lpp01_led_pruss_init);

/* Release the PRU subsystem */
void lpp01_led_pruss_free(struct platform_device *pdev, struct lpp01_led_pru_data *data)
{
	struct resource *regs_pru_base, *regs_psc0, *regs_psc1;
	
	regs_pru_base = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!regs_pru_base) {
		dev_err(&pdev->dev, "no memory resource specified for PRUs base\n");
	}
	
	regs_psc0 = platform_get_resource(pdev, IORESOURCE_MEM, 2);
	if (!regs_psc0) {
		dev_err(&pdev->dev, "no memory resource specified for PSC0 base\n");
	}
	
	regs_psc1 = platform_get_resource(pdev, IORESOURCE_MEM, 3);
	if (!regs_psc1) {
		dev_err(&pdev->dev, "no memory resource specified for PSC1 base\n");
	}
	
	free_irq(platform_get_irq(pdev, 1), data);
	free_irq(platform_get_irq(pdev, 0), data);
	
	iounmap(data->pru_iomap.psc1_io_addr);
	iounmap(data->pru_iomap.psc0_io_addr);
	iounmap(data->pru_iomap.pFifoBufferVirtBase);
	iounmap(data->pru_iomap.pru_io_addr);
	
	release_mem_region(regs_psc0->start, resource_size(regs_psc1));
	release_mem_region(regs_psc1->start, resource_size(regs_psc0));
	release_mem_region(regs_pru_base->start, resource_size(regs_pru_base));

	clk_put(data->pruss_clk);
	clk_disable(data->pruss_clk);
}
EXPORT_SYMBOL(lpp01_led_pruss_free);
#endif
