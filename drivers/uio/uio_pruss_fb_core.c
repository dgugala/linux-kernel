/*
 *  Virtual frame buffer device over UIO PRUSS
 *
 *      Copyright (C) 2019 Teleste Oy
 *
 *  This file is subject to the terms and conditions of the GNU General Public
 *  License. See the file COPYING in the main directory of this archive for
 *  more details.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/miscdevice.h>

#include <linux/fb.h>
#include <linux/init.h>
#include <linux/uaccess.h>
#include <linux/ioctl.h>

#include "prussdrv.h"
#include "uio_pruss_fb_led_pru.h"
#include "uio_pruss_fb_led_conv.h"
#include "uio_pruss_fb_core.h"
    /*
     *  RAM we reserve for the frame buffer. This defines the maximum screen
     *  size
     *
     *  The default can be overridden if the driver is compiled as a module
     */

#define DEFAULT_X_RES	640
#define DEFAULT_Y_RES	480
#define VIDEOMEMSIZE	(2*DEFAULT_X_RES*DEFAULT_Y_RES)	/* 300 KB */

#define UIO_PRUSS_FB_IOCTL_REFRESH _IO('r', 1)

static void *videomemory;
static u_long videomemorysize = VIDEOMEMSIZE;

static void *bitplanememory;  // 2MB of continuos RAM reserved

/* Sync device uses this pointer internally. */
static struct fb_info *internal_fb_info;
/* Used in character devices to check status of diagnostics. */
static tprussdrv *uio_pruss_fb_pru_info;

/** Internal pruss driver pointer needs to be initialized before any
 *  calls to the framebuffer device. */
void uio_pruss_fb_core_set_prussdrv(tprussdrv* drv)
{
        uio_pruss_fb_pru_info = drv;
}
EXPORT_SYMBOL(uio_pruss_fb_core_set_prussdrv);


#undef USE_VMALLOC

#ifdef USE_VMALLOC
/**********************************************************************
 *
 * Memory management
 *
 **********************************************************************/
static void *rvmalloc(unsigned long size)
{
	void *mem;
	unsigned long adr;

	size = PAGE_ALIGN(size);
	mem = vmalloc_32(size);
	if (!mem)
		return NULL;

	memset(mem, 0, size); /* Clear the ram out, no junk to the user */
	adr = (unsigned long) mem;
	while (size > 0) {
		SetPageReserved(vmalloc_to_page((void *)adr));
		adr += PAGE_SIZE;
		size -= PAGE_SIZE;
	}

	return mem;
}

static void rvfree(void *mem, unsigned long size)
{
	unsigned long adr;

	if (!mem)
		return;

	adr = (unsigned long) mem;
	while ((long) size > 0) {
		ClearPageReserved(vmalloc_to_page((void *)adr));
		adr += PAGE_SIZE;
		size -= PAGE_SIZE;
	}
	vfree(mem);
}
#endif /* USE_VMALLOC */


/***************************************************************************************/
/*
 * Sync device
 */

static int led_sync_ctrl_open(struct inode *inode, struct file *filp)
{
	return 0;
}

static int led_sync_ctrl_release(struct inode *inode, struct file *filp)
{
	return 0;
}

static ssize_t led_sync_ctrl_read(struct file *filp, char __user *buf,
				  size_t count, loff_t *f_pos)
{
	static char tempbuf[] = "F0";

	if (uio_pruss_fb_led_wait_frame_sync())
	    return -ERESTARTSYS;
	if (copy_to_user(buf, (unsigned char *)tempbuf, 2))
	    return -EFAULT;

	return 2;
}

#define PRU_DIAGNOSTICS_MODE(x) ((x)->led_diagnostics_running)
#define PRU_NORMAL_MODE(x) !(PRU_DIAGNOSTICS_MODE(x))

static ssize_t led_sync_ctrl_write(struct file *filp, const char __user *buf,
				   size_t count, loff_t *f_pos)
{
	if (count > 0) {
		if (internal_fb_info && uio_pruss_fb_pru_info && 
                    PRU_NORMAL_MODE(uio_pruss_fb_pru_info) ) {
			uio_pruss_fb_led_fb_memcpy_tobitplane(
					internal_fb_info->screen_base,
					internal_fb_info->screen_size,
					internal_fb_info->fix.line_length);
		}
		return count;
	}

	return -EFAULT;
}

static struct file_operations led_sync_ctrl_fops = {
    .read =	led_sync_ctrl_read,
    .write =	led_sync_ctrl_write,
    .open =	led_sync_ctrl_open,
    .release =	led_sync_ctrl_release,
    .owner = 	THIS_MODULE
};

static struct miscdevice led_sync_miscdev_ctrl = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = "led_sync",
    .fops = &led_sync_ctrl_fops,
};

/***************************************************************************************/

static struct fb_var_screeninfo uio_pruss_fb_default = {
	.xres =		DEFAULT_X_RES,
	.yres =		DEFAULT_Y_RES,
	.xres_virtual =	DEFAULT_X_RES,
	.yres_virtual =	DEFAULT_Y_RES,
	.bits_per_pixel = 16,
	.red =		{ 0, 5, 0 },
      	.green =	{ 0, 6, 0 },
      	.blue =		{ 0, 5, 0 },
      	.activate =	FB_ACTIVATE_NOW,
      	.height =	-1,
      	.width =	-1,
      	.pixclock =	20000,
      	.left_margin =	64,
      	.right_margin =	64,
      	.upper_margin =	32,
      	.lower_margin =	32,
      	.hsync_len =	64,
      	.vsync_len =	2,
      	.vmode =	FB_VMODE_NONINTERLACED,
};

static struct fb_fix_screeninfo uio_pruss_fb_fix = {
	.id =		"NGLPP FB",
	.type =		FB_TYPE_PACKED_PIXELS,
	.visual =	FB_VISUAL_PSEUDOCOLOR,
	.xpanstep =	1,
	.ypanstep =	1,
	.ywrapstep =	1,
	.accel =	FB_ACCEL_NONE,
};

static int uio_pruss_fb_check_var(struct fb_var_screeninfo *var,
			 struct fb_info *info);
static int uio_pruss_fb_set_par(struct fb_info *info);
static int uio_pruss_fb_setcolreg(u_int regno, u_int red, u_int green, u_int blue,
			 u_int transp, struct fb_info *info);
static int uio_pruss_fb_pan_display(struct fb_var_screeninfo *var,
			   struct fb_info *info);
static int uio_pruss_fb_mmap(struct fb_info *info,
		    struct vm_area_struct *vma);
static ssize_t uio_pruss_fb_write(struct fb_info *info, const char __user *buf,
			     size_t count, loff_t *ppos);
static ssize_t uio_pruss_fb_read(struct fb_info *info, char __user *buf,
			    size_t count, loff_t *ppos);
static int uio_pruss_fb_blank(int blank_mode, struct fb_info *info);
static int uio_pruss_fb_ioctl(struct fb_info *info, unsigned int cmd, unsigned long arg);


static struct fb_ops uio_pruss_fb_ops = {
	.owner		= THIS_MODULE,
	.fb_read        = uio_pruss_fb_read,
	.fb_write       = uio_pruss_fb_write,
	.fb_check_var	= uio_pruss_fb_check_var,
	.fb_set_par	= uio_pruss_fb_set_par,
	.fb_setcolreg	= uio_pruss_fb_setcolreg,
	.fb_pan_display	= uio_pruss_fb_pan_display,
	.fb_fillrect	= cfb_fillrect,
	.fb_copyarea	= cfb_copyarea,
	.fb_imageblit	= cfb_imageblit,
	.fb_mmap	= uio_pruss_fb_mmap,
	.fb_blank	= uio_pruss_fb_blank,
	.fb_ioctl	= uio_pruss_fb_ioctl
};

    /*
     *  Internal routines
     */

static u_long get_line_length(int xres_virtual, int bpp)
{
	
        u_long length;

//         printk("%s: get_line_len xres %u, bpp %u\n", __FUNCTION__, xres_virtual, bpp);
	
	if (bpp < 8)
	    bpp = 8;
        length = xres_virtual * bpp;
        length = (length + 31) & ~31;
        length >>= 3;
        if (length % 4 != 0) {
          length += (4 - length % 4);
        }
        return (length);
}

    /*
     *  Setting the video mode has been split into two parts.
     *  First part, xxxfb_check_var, must not write anything
     *  to hardware, it should only verify and adjust var.
     *  This means it doesn't alter par but it does use hardware
     *  data from it to check this var. 
     */

static int uio_pruss_fb_check_var(struct fb_var_screeninfo *var,
			 struct fb_info *info)
{
	u_long line_length;

	/*
	 *  FB_VMODE_CONUPDATE and FB_VMODE_SMOOTH_XPAN are equal!
	 *  as FB_VMODE_SMOOTH_XPAN is only used internally
	 */

	if (var->vmode & FB_VMODE_CONUPDATE) {
		var->vmode |= FB_VMODE_YWRAP;
		var->xoffset = info->var.xoffset;
		var->yoffset = info->var.yoffset;
	}

	/*
	 *  Some very basic checks
	 */
	if (!var->xres)
		var->xres = 1;
	if (!var->yres)
		var->yres = 1;
	if (var->xres > var->xres_virtual)
		var->xres_virtual = var->xres;
	if (var->yres > var->yres_virtual)
		var->yres_virtual = var->yres;
	if (var->bits_per_pixel <= 1)
		var->bits_per_pixel = 1;
	else if (var->bits_per_pixel <= 8)
		var->bits_per_pixel = 8;
	else if (var->bits_per_pixel <= 16)
		var->bits_per_pixel = 16;
	else if (var->bits_per_pixel <= 24)
		var->bits_per_pixel = 24;
	else if (var->bits_per_pixel <= 32)
		var->bits_per_pixel = 32;
	else
		return -EINVAL;

	if (var->xres_virtual < var->xoffset + var->xres)
		var->xres_virtual = var->xoffset + var->xres;
	if (var->yres_virtual < var->yoffset + var->yres)
		var->yres_virtual = var->yoffset + var->yres;

	/*
	 *  Memory limit
	 */
	line_length =
	    get_line_length(var->xres_virtual, var->bits_per_pixel);
	if (line_length * var->yres_virtual > videomemorysize)
		return -ENOMEM;

	/*
	 * Now that we checked it we alter var. The reason being is that the video
	 * mode passed in might not work but slight changes to it might make it 
	 * work. This way we let the user know what is acceptable.
	 */
	switch (var->bits_per_pixel) {
	case 1:
	case 8:
		var->red.offset = 0;
		var->red.length = 8;
		var->green.offset = 0;
		var->green.length = 8;
		var->blue.offset = 0;
		var->blue.length = 8;
		var->transp.offset = 0;
		var->transp.length = 0;
		break;
	case 16:		/* RGBA 5551 */
		if (var->transp.length) {
			var->red.offset = 0;
			var->red.length = 5;
			var->green.offset = 5;
			var->green.length = 5;
			var->blue.offset = 10;
			var->blue.length = 5;
			var->transp.offset = 15;
			var->transp.length = 1;
		} else {	/* RGB 565 */
			var->red.offset = 0;
			var->red.length = 5;
			var->green.offset = 5;
			var->green.length = 6;
			var->blue.offset = 11;
			var->blue.length = 5;
			var->transp.offset = 0;
			var->transp.length = 0;
		}
		break;
	case 24:		/* RGB 888 */
		var->red.offset = 0;
		var->red.length = 8;
		var->green.offset = 8;
		var->green.length = 8;
		var->blue.offset = 16;
		var->blue.length = 8;
		var->transp.offset = 0;
		var->transp.length = 0;
		break;
	case 32:		/* RGBA 8888 */
		var->red.offset = 0;
		var->red.length = 8;
		var->green.offset = 8;
		var->green.length = 8;
		var->blue.offset = 16;
		var->blue.length = 8;
		var->transp.offset = 24;
		var->transp.length = 8;
		break;
	}
	var->red.msb_right = 0;
	var->green.msb_right = 0;
	var->blue.msb_right = 0;
	var->transp.msb_right = 0;

	return 0;
}

/* This routine actually sets the video mode. It's in here where we
 * the hardware state info->par and fix which can be affected by the 
 * change in par. For this driver it doesn't do much. 
 */
static int uio_pruss_fb_set_par(struct fb_info *info)
{
	printk("%s: Start\n", __FUNCTION__);
	info->fix.line_length = get_line_length(info->var.xres_virtual,
						info->var.bits_per_pixel);
	switch(info->var.bits_per_pixel) {
	case 1: info->fix.visual = FB_VISUAL_MONO10;break;
	case 8: info->fix.visual = FB_VISUAL_PSEUDOCOLOR;break;
	case 16: info->fix.visual = FB_VISUAL_DIRECTCOLOR;break;
	case 24: info->fix.visual = FB_VISUAL_DIRECTCOLOR;break;
	case 32: info->fix.visual = FB_VISUAL_DIRECTCOLOR;break;
	default: return -EINVAL;
        }
        return 0;
}

    /*
     *  Set a single color register. The values supplied are already
     *  rounded down to the hardware's capabilities (according to the
     *  entries in the var structure). Return != 0 for invalid regno.
     */

static int uio_pruss_fb_setcolreg(u_int regno, u_int red, u_int green, u_int blue,
			 u_int transp, struct fb_info *info)
{
	if (regno >= 256)	/* no. of hw registers */
		return 1;
	/*
	 * Program hardware... do anything you want with transp
	 */

	/* grayscale works only partially under directcolor */
	if (info->var.grayscale) {
		/* grayscale = 0.30*R + 0.59*G + 0.11*B */
		red = green = blue =
		    (red * 77 + green * 151 + blue * 28) >> 8;
	}

	/* Directcolor:
	 *   var->{color}.offset contains start of bitfield
	 *   var->{color}.length contains length of bitfield
	 *   {hardwarespecific} contains width of RAMDAC
	 *   cmap[X] is programmed to (X << red.offset) | (X << green.offset) | (X << blue.offset)
	 *   RAMDAC[X] is programmed to (red, green, blue)
	 *
	 * Pseudocolor:
	 *    var->{color}.offset is 0 unless the palette index takes less than
	 *                        bits_per_pixel bits and is stored in the upper
	 *                        bits of the pixel value
	 *    var->{color}.length is set so that 1 << length is the number of available
	 *                        palette entries
	 *    cmap is not used
	 *    RAMDAC[X] is programmed to (red, green, blue)
	 *
	 * Truecolor:
	 *    does not use DAC. Usually 3 are present.
	 *    var->{color}.offset contains start of bitfield
	 *    var->{color}.length contains length of bitfield
	 *    cmap is programmed to (red << red.offset) | (green << green.offset) |
	 *                      (blue << blue.offset) | (transp << transp.offset)
	 *    RAMDAC does not exist
	 */
#define CNVT_TOHW(val,width) ((((val)<<(width))+0x7FFF-(val))>>16)
	switch (info->fix.visual) {
	case FB_VISUAL_TRUECOLOR:
	case FB_VISUAL_PSEUDOCOLOR:
		red = CNVT_TOHW(red, info->var.red.length);
		green = CNVT_TOHW(green, info->var.green.length);
		blue = CNVT_TOHW(blue, info->var.blue.length);
		transp = CNVT_TOHW(transp, info->var.transp.length);
		break;
	case FB_VISUAL_DIRECTCOLOR:
		red = CNVT_TOHW(red, 8);	/* expect 8 bit DAC */
		green = CNVT_TOHW(green, 8);
		blue = CNVT_TOHW(blue, 8);
		/* hey, there is bug in transp handling... */
		transp = CNVT_TOHW(transp, 8);
		break;
	}
#undef CNVT_TOHW
	/* Truecolor has hardware independent palette */
	if (info->fix.visual == FB_VISUAL_TRUECOLOR) {
		u32 v;

		if (regno >= 16)
			return 1;

		v = (red << info->var.red.offset) |
		    (green << info->var.green.offset) |
		    (blue << info->var.blue.offset) |
		    (transp << info->var.transp.offset);
		switch (info->var.bits_per_pixel) {
		case 8:
			break;
		case 16:
			((u32 *) (info->pseudo_palette))[regno] = v;
			break;
		case 24:
		case 32:
			((u32 *) (info->pseudo_palette))[regno] = v;
			break;
		}
		return 0;
	}
	return 0;
}

    /*
     *  Pan or Wrap the Display
     *
     *  This call looks only at xoffset, yoffset and the FB_VMODE_YWRAP flag
     */

static int uio_pruss_fb_pan_display(struct fb_var_screeninfo *var,
			   struct fb_info *info)
{
	if (var->vmode & FB_VMODE_YWRAP) {
		if (var->yoffset < 0
		    || var->yoffset >= info->var.yres_virtual
		    || var->xoffset)
			return -EINVAL;
	} else {
		if (var->xoffset + var->xres > info->var.xres_virtual ||
		    var->yoffset + var->yres > info->var.yres_virtual)
			return -EINVAL;
	}
	info->var.xoffset = var->xoffset;
	info->var.yoffset = var->yoffset;
	if (var->vmode & FB_VMODE_YWRAP)
		info->var.vmode |= FB_VMODE_YWRAP;
	else
		info->var.vmode &= ~FB_VMODE_YWRAP;
	return 0;
}

    /*
     *  Most drivers don't need their own mmap function 
     */

static int uio_pruss_fb_mmap(struct fb_info *info,
		    struct vm_area_struct *vma)
{
	unsigned long start = vma->vm_start;
	unsigned long size = vma->vm_end - vma->vm_start;
	unsigned long offset = vma->vm_pgoff << PAGE_SHIFT;
	unsigned long page, pos;

	if (offset + size > info->fix.smem_len) {
		return -EINVAL;
	}

	pos = (unsigned long)info->fix.smem_start + offset;

	while (size > 0) {
		page = vmalloc_to_pfn((void *)pos);
		if (remap_pfn_range(vma, start, page, PAGE_SIZE, PAGE_SHARED)) {
			return -EAGAIN;
		}
		start += PAGE_SIZE;
		pos += PAGE_SIZE;
		if (size > PAGE_SIZE)
			size -= PAGE_SIZE;
		else
			size = 0;
	}

	vma->vm_flags |= (VM_DONTEXPAND | VM_DONTDUMP);
	return 0;

}

static int uio_pruss_fb_blank(int blank_mode, struct fb_info *info)
{
	switch(blank_mode) {

        case FB_BLANK_NORMAL:         /* Normal blanking */
        case FB_BLANK_VSYNC_SUSPEND:  /* VESA blank (vsync off) */
        case FB_BLANK_HSYNC_SUSPEND:  /* VESA blank (hsync off) */
        case FB_BLANK_POWERDOWN:      /* Poweroff */
	    /* TODO: poweroff */
        case FB_BLANK_UNBLANK: /* Unblanking */
	    /* TODO: poweron */
	    break;
        }
        
	return 0;
}


static int uio_pruss_fb_ioctl(struct fb_info *info, unsigned int cmd,
			 unsigned long arg)
{
	//void __user *argp = (void __user *)arg;
	//u32 val;
	int retval = -EFAULT;

	switch (cmd) {
	    
	    case UIO_PRUSS_FB_IOCTL_REFRESH:
		//printk("%s: %d\n", __FUNCTION__, cmd);

		if (internal_fb_info && uio_pruss_fb_pru_info &&
			PRU_NORMAL_MODE(uio_pruss_fb_pru_info)) {
			uio_pruss_fb_led_fb_memcpy_tobitplane(
					internal_fb_info->screen_base,
		  			internal_fb_info->screen_size,
					internal_fb_info->fix.line_length);
			retval = 0;

		}
		break;

	    default:
		retval = -ENOIOCTLCMD;
		break;
	}
	return retval;
}

static ssize_t
uio_pruss_fb_write(struct fb_info *info, const char __user *buf, size_t count, loff_t *ppos)
{
	unsigned long p = *ppos;
	u8 *buffer, *src;
	u8 __iomem *dst;
	int c, cnt = 0, err = 0;
	unsigned long total_size;

	if (!info || !info->screen_base)
		return -ENODEV;

	if (info->state != FBINFO_STATE_RUNNING)
		return -EPERM;
	
	total_size = info->screen_size;

	if (total_size == 0)
		total_size = info->fix.smem_len;

	if (p > total_size)
		return -EFBIG;

	if (count > total_size) {
		err = -EFBIG;
		count = total_size;
	}

	if (count + p > total_size) {
		if (!err)
			err = -ENOSPC;

		count = total_size - p;
	}

	buffer = kmalloc((count > PAGE_SIZE) ? PAGE_SIZE : count,
			 GFP_KERNEL);
	if (!buffer)
		return -ENOMEM;

	dst = (u8 __iomem *) (info->screen_base + p);

	if (info->fbops->fb_sync)
		info->fbops->fb_sync(info);

	while (count) {
		c = (count > PAGE_SIZE) ? PAGE_SIZE : count;
		src = buffer;

		if (copy_from_user(src, buf, c)) {
			err = -EFAULT;
			break;
		}

		fb_memcpy_tofb(dst, src, c);
		dst += c;
		src += c;
		*ppos += c;
		buf += c;
		cnt += c;
		count -= c;
	}

	kfree(buffer);
	
 	//printk("%s: %d\n", __FUNCTION__, cnt);
	//fb_memcpy_tofb(bitplane, videomemory, videomemorysize);
	// Copy new data from framebuffer to bitplane memory

	uio_pruss_fb_led_fb_memcpy_tobitplane(info->screen_base, info->screen_size, info->fix.line_length);

	return (cnt) ? cnt : err;
}

static ssize_t
uio_pruss_fb_read(struct fb_info *info, char __user *buf, size_t count, loff_t *ppos)
{
	unsigned long p = *ppos;
	u8 *buffer, *dst;
	u8 __iomem *src;
	int c, cnt = 0, err = 0;
	unsigned long total_size;

	if (!info || ! info->screen_base)
		return -ENODEV;

	if (info->state != FBINFO_STATE_RUNNING)
		return -EPERM;
	
	total_size = info->screen_size;

	if (total_size == 0)
		total_size = info->fix.smem_len;

	if (p >= total_size)
		return 0;

	if (count >= total_size)
		count = total_size;

	if (count + p > total_size)
		count = total_size - p;

	buffer = kmalloc((count > PAGE_SIZE) ? PAGE_SIZE : count,
			 GFP_KERNEL);
	if (!buffer)
		return -ENOMEM;

	src = (u8 __iomem *) (info->screen_base + p);

	if (info->fbops->fb_sync)
		info->fbops->fb_sync(info);

	while (count) {
		c  = (count > PAGE_SIZE) ? PAGE_SIZE : count;
		dst = buffer;
		fb_memcpy_fromfb(dst, src, c);
		dst += c;
		src += c;

		if (copy_to_user(buf, buffer, c)) {
			err = -EFAULT;
			break;
		}
		*ppos += c;
		buf += c;
		cnt += c;
		count -= c;
	}

	kfree(buffer);
	
// 	printk("%s: %d\n", __FUNCTION__, cnt);

	return (err) ? err : cnt;
}



struct uio_pruss_fb_info *uio_pruss_fb_core_create(struct platform_device *dev) {
        struct uio_pruss_fb_info *info;
	int ret = 0;

	info = kzalloc(sizeof(struct uio_pruss_fb_info), GFP_KERNEL);
        if (!info) {
	        printk(KERN_INFO
        	       "%s: Framebuffer memory allocation failed.", __FUNCTION__);
                return NULL;
	}

	videomemory = ioremap(FB_START_ADDR, videomemorysize);
	if (!videomemory)
		goto err_free_info;

	memset(videomemory, 0, videomemorysize);

	bitplanememory = ioremap(PRUS_BITPLANES_START_ADDR, PRUS_BITPLANES_MEM_SZ);
	if (!bitplanememory)
		goto err_unmap_videomemory;


        info->fb_info = framebuffer_alloc(sizeof(u32) * 256, &dev->dev);
        if (!info->fb_info)
                goto err_unmap_bitplanememory;

	info->fb_info->screen_base = (char __iomem *)videomemory;
	info->fb_info->fbops = &uio_pruss_fb_ops;

	ret = fb_find_mode(&info->fb_info->var, info->fb_info, NULL,
			      NULL, 0, NULL, 16);

	if (!ret || (ret == 4))
		info->fb_info->var = uio_pruss_fb_default;
	
	uio_pruss_fb_fix.smem_start = (unsigned long) videomemory;
	uio_pruss_fb_fix.smem_len = videomemorysize;
	uio_pruss_fb_fix.line_length = get_line_length(info->fb_info->var.xres_virtual,
						  info->fb_info->var.bits_per_pixel);
	info->fb_info->fix = uio_pruss_fb_fix;
	info->fb_info->pseudo_palette = info->fb_info->par;
	info->fb_info->par = NULL;
	info->fb_info->flags = FBINFO_FLAG_DEFAULT;


        ret = fb_alloc_cmap(&info->fb_info->cmap, 256, 0);
        if (ret < 0)
                goto err_release_framebuffer;

	ret = register_framebuffer(info->fb_info);
	if (ret < 0)
		goto err_dealloc_cmap;

	internal_fb_info = info->fb_info;

	return info;

err_unregister_framebuffer:
	unregister_framebuffer(info->fb_info);
err_dealloc_cmap:
	fb_dealloc_cmap(&info->fb_info->cmap);
err_release_framebuffer:
	framebuffer_release(info->fb_info);
err_unmap_bitplanememory:
        iounmap(bitplanememory);
err_unmap_videomemory:
        iounmap(videomemory);

err_free_info:
	kfree(info);
	return NULL;
}

EXPORT_SYMBOL(uio_pruss_fb_core_create);

void* uio_pruss_fb_core_get_bitplanememory_pointer(void) {
	return bitplanememory;
}
EXPORT_SYMBOL(uio_pruss_fb_core_get_bitplanememory_pointer);


void uio_pruss_fb_core_remove(struct uio_pruss_fb_info* info) {
	/** Make sure that internal pointers are null after calling remove. */
	internal_fb_info = NULL;
	uio_pruss_fb_pru_info = NULL;
	
	if (info) {
		if (info->fb_info) {
                //misc_deregister(&led_sync_miscdev_ctrl);
                	unregister_framebuffer(info->fb_info);
	                fb_dealloc_cmap(&info->fb_info->cmap);
        	        framebuffer_release(info->fb_info);
		} else {
		        printk(KERN_INFO
        		       "%s: No framebuffer (fb_info) that can be released", __FUNCTION__);

		}

		if (bitplanememory)
                	iounmap(bitplanememory);
		if (videomemory)
                	iounmap(videomemory);


                kfree(info);
	} else {
	        printk(KERN_INFO
        	       "%s: No framebuffer device available but remove called.", __FUNCTION__);

	}
}
EXPORT_SYMBOL(uio_pruss_fb_core_remove);

