#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/firmware.h>


#include "uio_pruss_fan_config.h"

static t_fan_config uio_pruss_fan_configs[NUM_OF_FANS];
static t_fan_feedback uio_pruss_fan_feedbacks[NUM_OF_FANS];

void uio_pruss_fan_init_configs(void) 
{
	u8 i = 0;
	t_fan_config *conf = NULL;
	u8 config_size = sizeof(t_fan_config)/sizeof(u32);
	u32 fan_data_offset = 0x00;

	/* Initial values of the FAN control values */
	for (i = 0; i < NUM_OF_FANS; i++) {
		conf = &uio_pruss_fan_configs[i];
		conf->duty_cycle = DUTY_CYCLE_DEF;
		conf->period = PERIOD_DEF;
		conf->enable = ENABLE_DEF;

		uio_pruss_fan_feedbacks[i].tacho = TACHO_DEF;
	}
}
EXPORT_SYMBOL(uio_pruss_fan_init_configs);

t_fan_config *uio_pruss_fan_get_config(unsigned int fan_number)
{
	return (t_fan_config *)(&uio_pruss_fan_configs[fan_number]);
}
EXPORT_SYMBOL(uio_pruss_fan_get_config);

t_fan_feedback *uio_pruss_fan_get_feedback(unsigned int fan_number)
{
	return (t_fan_feedback *)(&uio_pruss_fan_feedbacks[fan_number]);
}
EXPORT_SYMBOL(uio_pruss_fan_get_feedback);
