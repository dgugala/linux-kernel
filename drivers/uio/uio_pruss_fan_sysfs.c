#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/firmware.h>

#include "uio_pruss_fan_config.h"
#include "uio_pruss_fan_mem.h"
#include "uio_pruss_fan_fw.h"

/**********************************************************************
 *
 * SYSFS attribute definitions
 *
 **********************************************************************/

static ssize_t uio_pruss_fan_run_show(struct device *dev,
                              struct device_attribute *attr,
                              char *buf)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);

	return sprintf(buf, "%d\n", data->pruss_running);
}


static ssize_t uio_pruss_fan_run_store(struct device *dev,
                               struct device_attribute *attr,
                               const char *buf, size_t len)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
	int ret = 0;
	printk(KERN_INFO "LOAD PRU\n");
	if (sysfs_streq(buf, "1")) {
		ret = uio_pruss_fan_fw_start(dev, PRU_NUM0);
		if (ret) {
			return len;
		}

		ret = uio_pruss_fan_fw_start(dev, PRU_NUM1);
		if (ret) {
			return len;
		}

	} else if (sysfs_streq(buf, "0")) {
		pru_disable(data);
		data->pruss_running = 0;
		data->pruss_0_running = 0;
		data->pruss_1_running = 0;
	}

	return len;
}
static DEVICE_ATTR(fan_run, S_IRUGO | S_IWUSR, uio_pruss_fan_run_show,
               uio_pruss_fan_run_store);

static ssize_t uio_pruss_fan_run_0_show(struct device *dev,
                              struct device_attribute *attr,
                              char *buf)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);

	return sprintf(buf, "%d\n", data->pruss_0_running);
}

static ssize_t uio_pruss_fan_run_0_store(struct device *dev,
                               struct device_attribute *attr,
                               const char *buf, size_t len)
{
	int ret = 0;
	
	if (sysfs_streq(buf, "1")) { 
		ret = uio_pruss_fan_fw_start(dev, PRU_NUM0);
		if (ret) {
			return len;
		}

	} else if (sysfs_streq(buf, "0")) {
		ret = uio_pruss_fan_fw_stop(dev, PRU_NUM0);
		if (ret) {
			return len;
		}
	}
	return len;
}
static DEVICE_ATTR(fan_run_0, S_IRUGO | S_IWUSR, uio_pruss_fan_run_0_show,
               uio_pruss_fan_run_0_store);

static ssize_t uio_pruss_fan_run_1_show(struct device *dev,
                              struct device_attribute *attr,
                              char *buf)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);

	return sprintf(buf, "%d\n", data->pruss_1_running);
}

static ssize_t uio_pruss_fan_run_1_store(struct device *dev,
                               struct device_attribute *attr,
                               const char *buf, size_t len)
{
	int ret = 0;
	
	if (sysfs_streq(buf, "1")) {
		ret = uio_pruss_fan_fw_start(dev, PRU_NUM1);
		if (ret) {
			return len;
		}

	} else if (sysfs_streq(buf, "0")) {
		ret = uio_pruss_fan_fw_stop(dev, PRU_NUM1);
		if (ret) {
			return len;
		}
	}
	return len;
}
static DEVICE_ATTR(fan_run_1, S_IRUGO | S_IWUSR, uio_pruss_fan_run_1_show,
               uio_pruss_fan_run_1_store);


static ssize_t uio_pruss_fan_info_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	t_fan_config* fan = NULL;
	t_fan_feedback* fb = NULL;
	u16 i = 0;
    ssize_t len = 0;

	for (i = 0; i < NUM_OF_FANS; i++) {
		fan = uio_pruss_fan_get_config(i);
		fb = uio_pruss_fan_get_feedback(i);
		len += sprintf(&buf[len], "FAN %d:\n", i);
		len += sprintf(&buf[len], "Duty cycle: %d\n", fan->duty_cycle);
		len += sprintf(&buf[len], "Period: %d\n", fan->period);
		len += sprintf(&buf[len], "Enable: %d\n", fan->enable);
		if (fb->tacho == 0xBEEF) {
			len += sprintf(&buf[len], "Tacho: Not available\n");	
		} else {
			len += sprintf(&buf[len], "Tacho: %d\n", fb->tacho);
		}
	}
	
	return len;
}
static DEVICE_ATTR(fan_info_regs, S_IRUGO | S_IWUSR, uio_pruss_fan_info_show, NULL);

static ssize_t uio_pruss_fan_sysfs_tacho_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	t_fan_feedback* fb = NULL;
	u16 i = 0;
    ssize_t len = 0;

	for (i = 0; i < NUM_OF_FANS; i++) {
		fb = uio_pruss_fan_get_feedback(i);
		if (fb->tacho == 0xBEEF) {
			len += sprintf(&buf[len], "Tacho %d: Not available\n", i);	
		} else {
			len += sprintf(&buf[len], "Tacho %d: %d\n", i, fb->tacho);
		}
	}
	
	return len;
}
static DEVICE_ATTR(fan_tacho_regs, S_IRUGO | S_IWUSR, uio_pruss_fan_sysfs_tacho_show,
               NULL);


static ssize_t uio_pruss_fan_pru_cfg_regs_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
        ssize_t len = 0;
	unsigned int cfg_register = prussdrv_pru_get_cfg_syscfg_reg(data);

	len += sprintf(&buf[len], "PRUSS Config:\n");
	len += sprintf(&buf[len], "\t0x%08x\n", cfg_register);

	if (cfg_register & 0x0003)
		len += sprintf(&buf[len], "\tIDLE_MODE: Reserved\n");
	else if (cfg_register & 0x0002)
		len += sprintf(&buf[len], "\tIDLE_MODE: Smart-idle mode\n");
	else if (cfg_register & 0x0001)
		len += sprintf(&buf[len], "\tIDLE_MODE: No-idle mode\n");
	else
		len += sprintf(&buf[len], "\tIDLE_MODE: Force-idle mode\n");
	if (cfg_register & 0x000C)
		len += sprintf(&buf[len], "\tSTANDBY_MODE: Reserved\n");
	else if (cfg_register & 0x0008)
		len += sprintf(&buf[len], "\tSTANDBY_MODE: Smart standby mode\n");
	else if (cfg_register & 0x0004)
		len += sprintf(&buf[len], "\tSTANDBY_MODE: No standby mode\n");
	else
		len += sprintf(&buf[len], "\tSTANDY_MODE: Force standby mode\n");

	if (cfg_register & 0x0010)
		len += sprintf(&buf[len], "\tSTANDBY_INIT: Initial standby sequence\n");
	else
		len += sprintf(&buf[len], "\tSTANDBY_INIT: Enable OCP master ports\n");

	if (cfg_register & 0x0020)
		len += sprintf(&buf[len], "\tSUB_MWAIT: Wait until 0\n");
	else
		len += sprintf(&buf[len], "\tSUB_MWAIT: Ready for Transaction\n");

	return len;
}
static DEVICE_ATTR(fan_cfg_regs_pru, S_IRUGO | S_IWUSR, uio_pruss_fan_pru_cfg_regs_show, NULL);

static ssize_t uio_pruss_fan_pru_control_regs_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf, int pru_num)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
    ssize_t len = 0;
	unsigned int control_register = prussdrv_pru_get_controlreg(pru_num, data);

	if (pru_num == PRU_NUM0) {
		len += sprintf(&buf[len], "PRU 0:\n");
	} else {
		len += sprintf(&buf[len], "PRU 1:\n");
	}
	
	len += sprintf(&buf[len], "\t0x%08x\n", control_register);
	if (control_register & 0x0001)
		len += sprintf(&buf[len], "\tSOFT_RST_N: RUN\n");
	else
		len += sprintf(&buf[len], "\tSOFT_RST_N: RESET\n");
	if (control_register & 0x0002)
		len += sprintf(&buf[len], "\tEN: Enabled\n");
	else
		len += sprintf(&buf[len], "\tEN: Disabled\n");
	if (control_register & 0x0004)
		len += sprintf(&buf[len], "\tSLEEPING: ASLEEP\n");
	else
		len += sprintf(&buf[len], "\tSLEEPING: NOT ASLEEP\n");
	if (control_register & 0x0008)
		len += sprintf(&buf[len], "\tCTR_EN: Counters enabled\n");
	else
		len += sprintf(&buf[len], "\tCTR_EN: Counters disabled\n");
	if (control_register & 0x0100)
		len += sprintf(&buf[len], "\tSINGLE_STEP: SINGLE\n");
	else
		len += sprintf(&buf[len], "\tSINGLE_STEP: FREE RUN\n");
	if (control_register & 0x8000)
		len += sprintf(&buf[len], "\tRUNSTATE: RUNNING\n");
	else
		len += sprintf(&buf[len], "\tRUNSTATE: HALTED\n");
	len += sprintf(&buf[len], "\tPC: 0x%04x\n", (control_register >> 16));

	return len;

}


static ssize_t uio_pruss_fan_pru_debug_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf, int pru_num)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
    ssize_t len = 0;
	unsigned int control_register = prussdrv_pru_get_controlreg(pru_num, data);

	if (pru_num == PRU_NUM0) {
		len += sprintf(&buf[len], "PRU 0:\n");
	} else {
		len += sprintf(&buf[len], "PRU 1:\n");
	}

	if (control_register & 0x0100)
		len += sprintf(&buf[len], "\tSINGLE_STEP: SINGLE\n");
	else
		len += sprintf(&buf[len], "\tSINGLE_STEP: FREE RUN\n");
	return len;
}

static ssize_t uio_pruss_fan_pru_registers_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf, int pru_num)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
        ssize_t len = 0;
	unsigned int *debug_register_base = prussdrv_pru_get_debug_register_base(pru_num, data);
	int i = 0;

	if (pru_num == PRU_NUM0) {
		len += sprintf(&buf[len], "PRU 0 Registers:\n");
	} else {
		len += sprintf(&buf[len], "PRU 1 Registers:\n");
	}

	for (i = 0; i < 32; i++) {
		len += sprintf(&buf[len], "\tR%d: %08x\n", i, *debug_register_base );
		debug_register_base++;
	}

	return len;
}


static ssize_t uio_pruss_fan_pru_debug_store(struct device *dev,
					      struct device_attribute *attr,
					      const char *buf, size_t len, int pru_num)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
	unsigned int *control_register = prussdrv_pru_get_controlreg_pointer(pru_num, data);

	int tmp = 0;

	sscanf(buf, "%d", &tmp);

	if (tmp == 0)
	{
		*control_register = (*control_register & 0xFFFFFEFF) | 0x2;
	} else {
		*control_register = (*control_register | 0x100);
	}
	return len;
}

static ssize_t uio_pruss_fan_pru_status_regs_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf, int pru_num)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
        ssize_t len = 0;
	unsigned int control_register = prussdrv_pru_get_statusreg(pru_num, data);

    if (pru_num == PRU_NUM0){
		len += sprintf(&buf[len], "PRU 0:\n");
	} else {
		len += sprintf(&buf[len], "PRU 1:\n");
	}
	len += sprintf(&buf[len], "\t0x%08x\n", control_register);
	len += sprintf(&buf[len], "\tPC: 0x%04x\n", (control_register & 0xFFFF));

	return len;
}

static ssize_t uio_pruss_fan_pru_cycle_regs_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf, int pru_num)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
    ssize_t len = 0;
	unsigned int control_register = prussdrv_pru_get_cyclereg(pru_num, data);

    if (pru_num == PRU_NUM0) {
		len += sprintf(&buf[len], "PRU 0:\n");
	} else {
		len += sprintf(&buf[len], "PRU 1:\n");
	}
	len += sprintf(&buf[len], "\t0x%08x\n", control_register);

	return len;
}

static ssize_t uio_pruss_fan_pru_control_regs_0_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	#if 1
	return uio_pruss_fan_pru_control_regs_show(dev, attr, buf, PRU_NUM0);
	#else
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
        ssize_t len = 0;
	unsigned int control_register = prussdrv_pru_get_controlreg(PRU_NUM0, data);

	len += sprintf(&buf[len], "PRU 0:\n");
	len += sprintf(&buf[len], "\t0x%08x\n", control_register);
	if (control_register & 0x0001)
		len += sprintf(&buf[len], "\tSOFT_RST_N: RUN\n");
	else
		len += sprintf(&buf[len], "\tSOFT_RST_N: RESET\n");
	if (control_register & 0x0002)
		len += sprintf(&buf[len], "\tEN: Enabled\n");
	else
		len += sprintf(&buf[len], "\tEN: Disabled\n");
	if (control_register & 0x0004)
		len += sprintf(&buf[len], "\tSLEEPING: ASLEEP\n");
	else
		len += sprintf(&buf[len], "\tSLEEPING: NOT ASLEEP\n");
	if (control_register & 0x0008)
		len += sprintf(&buf[len], "\tCTR_EN: Counters enabled\n");
	else
		len += sprintf(&buf[len], "\tCTR_EN: Counters disabled\n");
	if (control_register & 0x0100)
		len += sprintf(&buf[len], "\tSINGLE_STEP: SINGLE\n");
	else
		len += sprintf(&buf[len], "\tSINGLE_STEP: FREE RUN\n");
	if (control_register & 0x8000)
		len += sprintf(&buf[len], "\tRUNSTATE: RUNNING\n");
	else
		len += sprintf(&buf[len], "\tRUNSTATE: HALTED\n");
	len += sprintf(&buf[len], "\tPC: 0x%04x\n", (control_register >> 16));

	return len;
	#endif
}
static DEVICE_ATTR(fan_control_regs_pru0, S_IRUGO | S_IWUSR, uio_pruss_fan_pru_control_regs_0_show,
               NULL);

static ssize_t uio_pruss_fan_pru_control_regs_1_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	return uio_pruss_fan_pru_control_regs_show(dev, attr, buf, PRU_NUM1);
}
static DEVICE_ATTR(fan_control_regs_pru1, S_IRUGO | S_IWUSR, uio_pruss_fan_pru_control_regs_1_show,
               NULL);



static ssize_t uio_pruss_fan_pru_debug_0_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	#if 1
	return uio_pruss_fan_pru_debug_show(dev, attr, buf, PRU_NUM0);
	#else
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
        ssize_t len = 0;
	unsigned int control_register = prussdrv_pru_get_controlreg(PRU_NUM0, data);

	len += sprintf(&buf[len], "PRU 0:\n");
	if (control_register & 0x0100)
		len += sprintf(&buf[len], "\tSINGLE_STEP: SINGLE\n");
	else
		len += sprintf(&buf[len], "\tSINGLE_STEP: FREE RUN\n");
	return len;
    #endif
}

static ssize_t uio_pruss_fan_pru_debug_0_store(struct device *dev,
					      struct device_attribute *attr,
					      const char *buf, size_t len)
{
	#if 1
	return uio_pruss_fan_pru_debug_store(dev, attr, buf, len, PRU_NUM0);
	#else

	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
	unsigned int *control_register = prussdrv_pru_get_controlreg_pointer(PRU_NUM0, data);

	int tmp = 0;

	sscanf(buf, "%d", &tmp);

	if (tmp == 0)
	{
		*control_register = (*control_register & 0xFFFFFEFF) | 0x2;
	} else {
		*control_register = (*control_register | 0x100);
	}
	return len;
    #endif
}
static DEVICE_ATTR(fan_debug_pru0, S_IRUGO | S_IWUSR,
			uio_pruss_fan_pru_debug_0_show,
			uio_pruss_fan_pru_debug_0_store);
#if 0
static ssize_t uio_pruss_fan_pru_debug_1_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	return uio_pruss_fan_pru_debug_show(dev, attr, buf, PRU_NUM1);
}

static ssize_t uio_pruss_fan_pru_debug_1_store(struct device *dev,
					      struct device_attribute *attr,
					      const char *buf, size_t len)
{
	return uio_pruss_fan_pru_debug_store(dev, attr, buf, len, PRU_NUM1);
}
static DEVICE_ATTR(fan_debug_pru1, S_IRUGO | S_IWUSR,
			uio_pruss_fan_pru_debug_1_show,
			uio_pruss_fan_pru_debug_1_store);
#endif
static ssize_t uio_pruss_fan_pru_registers_0_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	#if 1
	return uio_pruss_fan_pru_registers_show(dev, attr, buf, PRU_NUM0);
	#else
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
        ssize_t len = 0;
	unsigned int *debug_register_base = prussdrv_pru_get_debug_register_base(PRU_NUM0, data);
	int i = 0;

	len += sprintf(&buf[len], "PRU 0 Registers:\n");

	for (i = 0; i < 32; i++) {
		len += sprintf(&buf[len], "\tR%d: %08x\n", i, *debug_register_base );
		debug_register_base++;
	}

	return len;
	#endif
}
static DEVICE_ATTR(fan_pru0_show_registers, S_IRUGO | S_IWUSR,
			uio_pruss_fan_pru_registers_0_show,
			NULL);


static ssize_t uio_pruss_fan_pru_debug_1_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	return uio_pruss_fan_pru_debug_show(dev, attr, buf, PRU_NUM1);
}

static ssize_t uio_pruss_fan_pru_debug_1_store(struct device *dev,
					      struct device_attribute *attr,
					      const char *buf, size_t len)
{
	return uio_pruss_fan_pru_debug_store(dev, attr, buf, len, PRU_NUM1);
}
static DEVICE_ATTR(fan_debug_pru1, S_IRUGO | S_IWUSR,
			uio_pruss_fan_pru_debug_1_show,
			uio_pruss_fan_pru_debug_1_store);

static ssize_t uio_pruss_fan_pru_registers_1_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	return uio_pruss_fan_pru_registers_show(dev, attr, buf, PRU_NUM1);
}
static DEVICE_ATTR(fan_pru1_show_registers, S_IRUGO | S_IWUSR,
			uio_pruss_fan_pru_registers_1_show,
			NULL);




static ssize_t uio_pruss_fan_pru_status_regs_0_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	#if 1
	return uio_pruss_fan_pru_status_regs_show(dev, attr, buf, PRU_NUM0);
	#else
	
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
        ssize_t len = 0;
	unsigned int control_register = prussdrv_pru_get_statusreg(PRU_NUM0, data);

	len += sprintf(&buf[len], "PRU 0:\n");
	len += sprintf(&buf[len], "\t0x%08x\n", control_register);
	len += sprintf(&buf[len], "\tPC: 0x%04x\n", (control_register & 0xFFFF));

	return len;
    #endif
}
static DEVICE_ATTR(fan_status_regs_pru0, S_IRUGO | S_IWUSR, uio_pruss_fan_pru_status_regs_0_show,
               NULL);

static ssize_t uio_pruss_fan_pru_status_regs_1_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	return uio_pruss_fan_pru_status_regs_show(dev, attr, buf, PRU_NUM1);
}
static DEVICE_ATTR(fan_status_regs_pru1, S_IRUGO | S_IWUSR, uio_pruss_fan_pru_status_regs_1_show,
               NULL);


static ssize_t uio_pruss_fan_pru_cycle_regs_0_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	#if 1
	return uio_pruss_fan_pru_cycle_regs_show(dev, attr, buf, PRU_NUM0);
	#else
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
        ssize_t len = 0;
	unsigned int control_register = prussdrv_pru_get_cyclereg(PRU_NUM0, data);

	len += sprintf(&buf[len], "PRU 0:\n");
	len += sprintf(&buf[len], "\t0x%08x\n", control_register);

	return len;
    #endif
}
static DEVICE_ATTR(fan_cycle_regs_pru0, S_IRUGO | S_IWUSR, uio_pruss_fan_pru_cycle_regs_0_show,
               NULL);

static ssize_t uio_pruss_fan_pru_cycle_regs_1_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	return uio_pruss_fan_pru_cycle_regs_show(dev, attr, buf, PRU_NUM1);
}
static DEVICE_ATTR(fan_cycle_regs_pru1, S_IRUGO | S_IWUSR, uio_pruss_fan_pru_cycle_regs_1_show,
               NULL);

static ssize_t uio_pruss_fan_pru_stall_regs_0_show(struct device *dev,
					      struct device_attribute *attr,
					      char *buf)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
        ssize_t len = 0;
	unsigned int control_register = prussdrv_pru_get_stallreg(PRU_NUM0, data);

	len += sprintf(&buf[len], "PRU 0:\n");
	len += sprintf(&buf[len], "\t0x%08x\n", control_register);

	return len;
}
static DEVICE_ATTR(fan_stall_regs_pru0, S_IRUGO | S_IWUSR, uio_pruss_fan_pru_stall_regs_0_show,
               NULL);



static unsigned int get_fan_tacho_offset(unsigned int fan_channel_number)
{
	//printk(KERN_INFO "get offset: %d\n", bitplane_number);
	if (fan_channel_number < NUM_OF_FANS) {
		return FAN_TACHO_OFFSET + (fan_channel_number * sizeof(u32));
	}

	printk(KERN_INFO "FATAL: fan channel number is out of range.\n");
	return 0;
}

static unsigned int get_fan_offset(unsigned int fan_channel_number)
{
	//printk(KERN_INFO "get offset: %\n", bitplane_number);
	if (fan_channel_number < NUM_OF_FANS) {
		return FAN_CONFIG_OFFSET + (fan_channel_number * (sizeof(t_fan_config)/sizeof(u32)));
	}

	//printk(KERN_INFO "FATAL: fan channel number is out of range.\n");
	return 0;

}


static void read_fan_data(unsigned int fan_channel_number, u8 config_size, tprussdrv* data)
{
	
	uio_pruss_fan_mem_read_data_u32( get_fan_offset(fan_channel_number),
				    (u32 *)(uio_pruss_fan_get_config(fan_channel_number)),
				    config_size, data, PRUSS0_PRU0_DATARAM);

}

static void read_fan_tacho_data(unsigned int fan_channel_number, u8 config_size, tprussdrv* data)
{
	uio_pruss_fan_mem_read_data_u32( 
					get_fan_tacho_offset(fan_channel_number),
					(u32 *)(uio_pruss_fan_get_feedback(fan_channel_number)),
					config_size, data, PRUSS0_PRU1_DATARAM);
}


static void write_fan_data(unsigned int fan_channel_number, u8 config_size, tprussdrv* data)
{
	uio_pruss_fan_mem_write_data_u32( 
				get_fan_offset(fan_channel_number),
				(u32 *)(uio_pruss_fan_get_config(fan_channel_number)),
				config_size, data, PRUSS0_PRU0_DATARAM);

}

#define CREATE_CHAIN_ATTRIBUTE_TACHO_RO(attrname, fan_channel_number) \
static ssize_t uio_pruss_fan_##attrname##_##fan_channel_number##_show(struct device *dev, \
                              struct device_attribute *attr, \
                              char *buf) \
{ \
        u8 config_size = (sizeof(t_fan_feedback)/sizeof(u32)) * NUM_OF_FANS; \
        tprussdrv *data = DEV_GET_PRUSS_DATA(dev);\
 \
    read_fan_tacho_data(0, config_size, data); \
	return sprintf(buf, "%#x\n", uio_pruss_fan_get_feedback(fan_channel_number)->attrname); \
} \
static DEVICE_ATTR(fan_##attrname##_##fan_channel_number, S_IRUGO | S_IWUSR, \
                uio_pruss_fan_##attrname##_##fan_channel_number##_show, NULL);

#define CREATE_CHAIN_ATTRIBUTE_RO(attrname, fan_channel_number) \
static ssize_t uio_pruss_fan_##attrname##_##fan_channel_number##_show(struct device *dev, \
                              struct device_attribute *attr, \
                              char *buf) \
{ \
	u8 config_size = (sizeof(t_fan_config)/sizeof(u32)) * NUM_OF_FANS; \
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);\
\
	read_fan_data(0, config_size, data); \
	return sprintf(buf, "%#x\n", uio_pruss_fan_get_config(fan_channel_number)->attrname); \
} \
static DEVICE_ATTR(fan_##attrname##_##fan_channel_number, S_IRUGO | S_IWUSR, \
		uio_pruss_fan_##attrname##_##fan_channel_number##_show, NULL);

#define CREATE_CHAIN_ATTRIBUTE_PTR_RO(attrname, fan_channel_number) \
static ssize_t uio_pruss_fan_##attrname##_##fan_channel_number##_show(struct device *dev, \
                              struct device_attribute *attr, \
                              char *buf) \
{ \
	u8 config_size = (sizeof(t_fan_config)/sizeof(u32)) * NUM_OF_FANS; \
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);\
\
	read_fan_data(0, config_size, data); \
	return sprintf(buf, "%p\n", uio_pruss_fan_get_config(fan_channel_number)->attrname); \
} \
static DEVICE_ATTR(attrname##_##fan_channel_number, S_IRUGO | S_IWUSR, \
		uio_pruss_fb_led_pru_##attrname##_##bitplane##_show, NULL);


#define CREATE_CHAIN_ATTRIBUTE_RW(attrname, fan_channel_number) \
static ssize_t uio_pruss_fan_##attrname##_##fan_channel_number##_show(struct device *dev, \
                              struct device_attribute *attr, \
                              char *buf) \
{ \
	u8 config_size = (sizeof(t_fan_config)/sizeof(u32))*NUM_OF_FANS; \
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);\
\
	read_fan_data(0, config_size, data); \
	return sprintf(buf, "%#x\n", uio_pruss_fan_get_config(fan_channel_number)->attrname); \
} \
static ssize_t uio_pruss_fan_##attrname##_##fan_channel_number##_store(struct device *dev, \
                               struct device_attribute *attr, \
                               const char *buf, size_t len) \
{ \
	u8 config_size = (sizeof(t_fan_config)/sizeof(u32)) * NUM_OF_FANS; \
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);\
\
	sscanf(buf, "0x%x", (u32 *)uio_pruss_fan_get_config(fan_channel_number)->attrname); \
\
	write_fan_data(0, config_size, data); \
	return len; \
} \
static DEVICE_ATTR(fan_##attrname##_##fan_channel_number, S_IRUGO | S_IWUSR, \
		uio_pruss_fan_##attrname##_##fan_channel_number##_show, \
		uio_pruss_fan_##attrname##_##fan_channel_number##_store);
		
		
#define CREATE_CHAIN_ATTRIBUTE_U16_RW(attrname, fan_channel_number) \
static ssize_t uio_pruss_fan_##attrname##_##fan_channel_number##_show(struct device *dev, \
                              struct device_attribute *attr, \
                              char *buf) \
{ \
	u8 config_size = (sizeof(t_fan_config)/sizeof(u32))*NUM_OF_FANS; \
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);\
\
	read_fan_data(0, config_size, data); \
	return sprintf(buf, "%#x\n", uio_pruss_fan_get_config(fan_channel_number)->attrname); \
} \
static ssize_t uio_pruss_fan_##attrname##_##fan_channel_number##_store(struct device *dev, \
                               struct device_attribute *attr, \
                               const char *buf, size_t len) \
{ \
    u8 config_size = (sizeof(t_fan_config)/sizeof(u32)) * NUM_OF_FANS; \
	u32 value = 0;\
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev); \
\
	sscanf(buf, "0x%x", &value); \
	uio_pruss_fan_get_config(fan_channel_number)->attrname = value;\
	write_fan_data(0, config_size, data); \
	return len; \
} \
static DEVICE_ATTR(fan_##attrname##_##fan_channel_number, S_IRUGO | S_IWUSR, \
		uio_pruss_fan_##attrname##_##fan_channel_number##_show, \
		uio_pruss_fan_##attrname##_##fan_channel_number##_store);


#define CREATE_CHAIN_ATTRIBUTE_U8_RW(attrname, fan_channel_number) \
static ssize_t uio_pruss_fan_##attrname##_##fan_channel_number##_show(struct device *dev, \
                              struct device_attribute *attr, \
                              char *buf) \
{ \
	u8 config_size = (sizeof(t_fan_config)/sizeof(u32))*NUM_OF_FANS; \
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev); \
\
	read_fan_data(0, config_size, data); \
	return sprintf(buf, "%#x\n", uio_pruss_fan_get_config(fan_channel_number)->attrname); \
} \
static ssize_t uio_pruss_fan_##attrname##_##fan_channel_number##_store(struct device *dev, \
                               struct device_attribute *attr, \
                               const char *buf, size_t len) \
{ \
	u8 config_size = (sizeof(t_fan_config)/sizeof(u32)) * NUM_OF_FANS; \
	u32 value = 0;\
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev); \
\
	sscanf(buf, "0x%x", &value); \
	uio_pruss_fan_get_config(fan_channel_number)->attrname = value;\
	write_fan_data(0, config_size, data); \
	return len; \
} \
static DEVICE_ATTR(fan_##attrname##_##fan_channel_number, S_IRUGO | S_IWUSR, \
		uio_pruss_fan_##attrname##_##fan_channel_number##_show, \
		uio_pruss_fan_##attrname##_##fan_channel_number##_store);

// FAN 0 
CREATE_CHAIN_ATTRIBUTE_U16_RW(duty_cycle, 0);
CREATE_CHAIN_ATTRIBUTE_U16_RW(period, 0);
CREATE_CHAIN_ATTRIBUTE_TACHO_RO(tacho, 0);
CREATE_CHAIN_ATTRIBUTE_U8_RW(enable, 0);

// FAN 1
CREATE_CHAIN_ATTRIBUTE_U16_RW(duty_cycle, 1);
CREATE_CHAIN_ATTRIBUTE_U16_RW(period, 1);
CREATE_CHAIN_ATTRIBUTE_TACHO_RO(tacho, 1);
CREATE_CHAIN_ATTRIBUTE_U8_RW(enable, 1);

// FAN 2
CREATE_CHAIN_ATTRIBUTE_U16_RW(duty_cycle, 2);
CREATE_CHAIN_ATTRIBUTE_U16_RW(period, 2);
CREATE_CHAIN_ATTRIBUTE_TACHO_RO(tacho, 2);
CREATE_CHAIN_ATTRIBUTE_U8_RW(enable, 2);

// FAN 3
CREATE_CHAIN_ATTRIBUTE_U16_RW(duty_cycle, 3);
CREATE_CHAIN_ATTRIBUTE_U16_RW(period, 3);
CREATE_CHAIN_ATTRIBUTE_TACHO_RO(tacho, 3);
CREATE_CHAIN_ATTRIBUTE_U8_RW(enable, 3);

// FAN 4
CREATE_CHAIN_ATTRIBUTE_U16_RW(duty_cycle, 4);
CREATE_CHAIN_ATTRIBUTE_U16_RW(period, 4);
CREATE_CHAIN_ATTRIBUTE_TACHO_RO(tacho, 4);
CREATE_CHAIN_ATTRIBUTE_U8_RW(enable, 4);

// FAN 5
CREATE_CHAIN_ATTRIBUTE_U16_RW(duty_cycle, 5);
CREATE_CHAIN_ATTRIBUTE_U16_RW(period, 5);
CREATE_CHAIN_ATTRIBUTE_TACHO_RO(tacho, 5);
CREATE_CHAIN_ATTRIBUTE_U8_RW(enable, 5);

// FAN 6
CREATE_CHAIN_ATTRIBUTE_U16_RW(duty_cycle, 6);
CREATE_CHAIN_ATTRIBUTE_U16_RW(period, 6);
CREATE_CHAIN_ATTRIBUTE_TACHO_RO(tacho, 6);
CREATE_CHAIN_ATTRIBUTE_U8_RW(enable, 6);

// FAN 7
CREATE_CHAIN_ATTRIBUTE_U16_RW(duty_cycle, 7);
CREATE_CHAIN_ATTRIBUTE_U16_RW(period, 7);
CREATE_CHAIN_ATTRIBUTE_TACHO_RO(tacho, 7);
CREATE_CHAIN_ATTRIBUTE_U8_RW(enable, 7);


static const struct attribute *uio_pruss_fan_sysfs_attrs[] = {
	&dev_attr_fan_run.attr,
	&dev_attr_fan_run_0.attr,
	&dev_attr_fan_run_1.attr,
	&dev_attr_fan_info_regs.attr,
	&dev_attr_fan_tacho_regs.attr,
	&dev_attr_fan_cfg_regs_pru.attr,
	&dev_attr_fan_debug_pru0.attr,
	&dev_attr_fan_pru0_show_registers.attr,
	&dev_attr_fan_control_regs_pru0.attr,
	&dev_attr_fan_status_regs_pru0.attr,
	&dev_attr_fan_cycle_regs_pru0.attr,
	&dev_attr_fan_stall_regs_pru0.attr,
	&dev_attr_fan_debug_pru1.attr,
	&dev_attr_fan_pru1_show_registers.attr,
	&dev_attr_fan_control_regs_pru1.attr,
	&dev_attr_fan_status_regs_pru1.attr,
	&dev_attr_fan_cycle_regs_pru1.attr,
	// PWM channel 0
	&dev_attr_fan_duty_cycle_0.attr,
	&dev_attr_fan_period_0.attr,
	&dev_attr_fan_tacho_0.attr,
	&dev_attr_fan_enable_0.attr,

	// PWM channel 1
	&dev_attr_fan_duty_cycle_1.attr,
	&dev_attr_fan_period_1.attr,
	&dev_attr_fan_tacho_1.attr,
	&dev_attr_fan_enable_1.attr,

	// PWM channel 2
	&dev_attr_fan_duty_cycle_2.attr,
	&dev_attr_fan_period_2.attr,
	&dev_attr_fan_tacho_2.attr,
	&dev_attr_fan_enable_2.attr,

	// PWM channel 3
	&dev_attr_fan_duty_cycle_3.attr,
	&dev_attr_fan_period_3.attr,
	&dev_attr_fan_tacho_3.attr,
	&dev_attr_fan_enable_3.attr,

	// PWM channel 4
	&dev_attr_fan_duty_cycle_4.attr,
	&dev_attr_fan_period_4.attr,
	&dev_attr_fan_tacho_4.attr,
	&dev_attr_fan_enable_4.attr,

	// PWM channel 5
	&dev_attr_fan_duty_cycle_5.attr,
	&dev_attr_fan_period_5.attr,
	&dev_attr_fan_tacho_5.attr,
	&dev_attr_fan_enable_5.attr,

	// PWM channel 6
	&dev_attr_fan_duty_cycle_6.attr,
	&dev_attr_fan_period_6.attr,
	&dev_attr_fan_tacho_6.attr,
	&dev_attr_fan_enable_6.attr,

	// PWM channel 7
	&dev_attr_fan_duty_cycle_7.attr,
	&dev_attr_fan_period_7.attr,
	&dev_attr_fan_tacho_7.attr,
	&dev_attr_fan_enable_7.attr,

    NULL,
};


static const struct attribute_group pruss_device_attr_group = {
        .attrs = (struct attribute **) uio_pruss_fan_sysfs_attrs,
};

int uio_pruss_fan_sysfs_create_nodes(struct platform_device *pdev)
{
	int retval = -ENOMEM;
	
	/* Create the sysfs interface for the driver */
	retval = sysfs_create_group(&pdev->dev.kobj, &pruss_device_attr_group);
	
	return retval;
}
EXPORT_SYMBOL(uio_pruss_fan_sysfs_create_nodes);

void uio_pruss_fan_sysfs_remove_nodes(struct platform_device *pdev)
{
	sysfs_remove_group(&pdev->dev.kobj, &pruss_device_attr_group);
}
EXPORT_SYMBOL(uio_pruss_fan_sysfs_remove_nodes);
