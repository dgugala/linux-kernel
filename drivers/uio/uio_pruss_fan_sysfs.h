#ifndef __UIO_PRUSS_FAN_SYSFS_H__
#define __UIO_PRUSS_FAN_SYSFS_H__

#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/clk.h>

int uio_pruss_fan_sysfs_create_nodes(struct platform_device *pdev);
void uio_pruss_fan_sysfs_remove_nodes(struct platform_device *pdev);

#endif /* __UIO_PRUSS_FAN_SYSFS_H */

