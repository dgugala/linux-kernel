#include <linux/errno.h>
#include <linux/firmware.h>
#include <linux/slab.h>

#include "uio_pruss_fb_led_conv.h"
#include "uio_pruss_fb_led_pru.h"

/*****************************************************************************/

#define LED_CONV_CONF_NAME 	"led_conv_conf.dat"

#define DBG_LED_CONFS

/*****************************************************************************/

static struct uio_pruss_fb_led_chain *uio_pruss_fb_chains_p[NUM_CHAINS];
static struct uio_pruss_fb_led_conv_block *conv_blocks_chains_p[NUM_CHAINS];
static int uio_pruss_fb_led_count = 0;

#undef LDMX06Y04
#undef LDMX19Y04

#ifdef LDMX06Y04
/* NOTE: USE THIS ONLY FOR TESTS 95LDMX06Y04 x 6 */
static struct uio_pruss_fb_led_conv_block conv_blocks_chain1[] = {
	// Second row
	{23*640, 0, 6, 0},
	{31*640, 0, 6, 0},
	{23*640 + 48, 0, 6, 0},
	{31*640 + 48, 0, 6, 0},
	{23*640 + 48 + 48, 0, 6, 0},
	{31*640 + 48 + 48, 0, 6, 0},
	// First row
	{8*640 + 48 + 48, 0, 6, 1},
	{0 + 48 + 48, 0, 6, 1},
	{8*640 + 48, 0, 6, 1},
	{0 + 48, 0, 6, 1},
	{8*640, 0, 6, 1},
	{0, 0, 6, 1}
};
#endif

#ifdef LDMX19Y04
/* NOTE: Eindhoven demo display */
static struct uio_pruss_fb_led_conv_block conv_blocks_chain1[] = {
	// Fourth row
	{23*640, 0, 20, 0},
	// Third row
	{0, 0, 20, 1},
	// Second row
	{23*640, 0, 20, 0},
	// First row
	{0, 0, 20, 1}
};
#endif

// #define NUM_CHAINS	sizeof(uio_pruss_fb_chains)/sizeof(lpp01ns[0])

#ifdef LDMX06Y04
/* NOTE: USE THIS ONLY FOR TESTS */
// Each chain has two bitplains (kind of double buffering)
static struct uio_pruss_fb_led_chain uio_pruss_fb_chains[] = {
	{
		.bp0_mem = NULL, .bp0_mem_addr = 0,
		.bp1_mem = NULL, .bp1_mem_addr = 0,
		.bp_mem_sz = 576, .bp_width = 72,
		.width = 144, .height = 32, .mux = 8,
		.conv_blk = conv_blocks_chain1,
		.conv_blk_sz = sizeof(conv_blocks_chain1)/sizeof(conv_blocks_chain1[0]),
		.wr_bp_mem = NULL, .wr_bp_mem_addr = 0
	}, // Chain 0
	{
		.bp0_mem = NULL, .bp0_mem_addr = 0,
		.bp1_mem = NULL, .bp1_mem_addr = 0,
		.bp_mem_sz = 576, .bp_width = 72,
		.width = 144, .height = 32, .mux = 8,
		.conv_blk = conv_blocks_chain1,
		.conv_blk_sz = sizeof(conv_blocks_chain1)/sizeof(conv_blocks_chain1[0]),
		.wr_bp_mem = NULL, .wr_bp_mem_addr = 0
	}, // Chain 1
	{
		.bp0_mem = NULL, .bp0_mem_addr = 0,
		.bp1_mem = NULL, .bp1_mem_addr = 0,
		.bp_mem_sz = 576, .bp_width = 72,
		.width = 144, .height = 32, .mux = 8,
		.conv_blk = conv_blocks_chain1,
		.conv_blk_sz = sizeof(conv_blocks_chain1)/sizeof(conv_blocks_chain1[0]),
		.wr_bp_mem = NULL, .wr_bp_mem_addr = 0
	}, // Chain 2
	{
		.bp0_mem = NULL, .bp0_mem_addr = 0,
		.bp1_mem = NULL, .bp1_mem_addr = 0,
		.bp_mem_sz = 576, .bp_width = 72,
		.width = 144, .height = 32, .mux = 8,
		.conv_blk = conv_blocks_chain1,
		.conv_blk_sz = sizeof(conv_blocks_chain1)/sizeof(conv_blocks_chain1[0]),
		.wr_bp_mem = NULL, .wr_bp_mem_addr = 0
	}, // Chain 3
};
#endif

#ifdef LDMX19Y04
static struct uio_pruss_fb_led_chain uio_pruss_fb_chains[] = {
	{
		.bp0_mem = NULL, .bp0_mem_addr = 0,
		.bp1_mem = NULL, .bp1_mem_addr = 0,
		.bp_mem_sz = 960, .bp_width = 80,
		.width = 160, .height = 48, .mux = 12,
		.conv_blk = conv_blocks_chain1,
		.conv_blk_sz = sizeof(conv_blocks_chain1)/sizeof(conv_blocks_chain1[0]),
		.wr_bp_mem = NULL, .wr_bp_mem_addr = 0
	}, // Chain 0
	{
		.bp0_mem = NULL, .bp0_mem_addr = 0,
		.bp1_mem = NULL, .bp1_mem_addr = 0,
		.bp_mem_sz = 960, .bp_width = 80,
		.width = 160, .height = 48, .mux = 12,
		.conv_blk = conv_blocks_chain1,
		.conv_blk_sz = sizeof(conv_blocks_chain1)/sizeof(conv_blocks_chain1[0]),
		.wr_bp_mem = NULL, .wr_bp_mem_addr = 0
	}, // Chain 1
	{
		.bp0_mem = NULL, .bp0_mem_addr = 0,
		.bp1_mem = NULL, .bp1_mem_addr = 0,
		.bp_mem_sz = 960, .bp_width = 80,
		.width = 160, .height = 48, .mux = 12,
		.conv_blk = conv_blocks_chain1,
		.conv_blk_sz = sizeof(conv_blocks_chain1)/sizeof(conv_blocks_chain1[0]),
		.wr_bp_mem = NULL, .wr_bp_mem_addr = 0
	}, // Chain 2
	{
		.bp0_mem = NULL, .bp0_mem_addr = 0,
		.bp1_mem = NULL, .bp1_mem_addr = 0,
		.bp_mem_sz = 960, .bp_width = 80,
		.width = 160, .height = 48, .mux = 12,
		.conv_blk = conv_blocks_chain1,
		.conv_blk_sz = sizeof(conv_blocks_chain1)/sizeof(conv_blocks_chain1[0]),
		.wr_bp_mem = NULL, .wr_bp_mem_addr = 0
	}, // Chain 3
};
#endif

// Use it with care
#if 0
static void uio_pruss_fb_led_dump_chainmem(struct uio_pruss_fb_led_chain *chain, uint16_t bp_width)
{
	uint16_t i = 0;
	
	printk("CHAINMEM 0\n");
	for (i = 0; i < chain->bp_mem_sz; i++) {
		printk("%02x ", *(uint8_t*)(chain->bp0_mem + i));
		if (((i + 1) % bp_width) == 0) {
			printk("\n");
		}
	}
	
	printk("CHAINMEM 1\n");
	for (i = 0; i < chain->bp_mem_sz; i++) {
		printk("%02x ", *(uint8_t*)(chain->bp1_mem + i));
		if (((i + 1) % bp_width) == 0) {
			printk("\n");
		}
	}
}
#endif

#ifdef DBG_LED_CONFS
static void uio_pruss_fb_led_dump_chain_conf(u8 chain_index)
{
	struct uio_pruss_fb_led_chain *chain = uio_pruss_fb_chains_p[chain_index];
// 	struct uio_pruss_fb_led_conv_block *conv_blk;
// 	u16 i = 0;
	
	printk("CHAIN %d conf:\n", chain_index);
	printk("\t.bp_mem_sz = %d, .bp_width = %d,\n",
	       chain->bp_mem_sz, chain->bp_width);
	printk("\t.width = %d, .height = %d, mux = %d\n",
	       chain->width, chain->height, chain->mux);
	printk("\t.conv_blk_sz = %d.\n", chain->conv_blk_sz);
// 	printk("\t.conv_blk:\n");
	
// 	conv_blk = chain->conv_blk;
// 	for (i = 0; i < chain->conv_blk_sz; i++) {
// 		printk("\t\t[%d] .vm_offset = %d, .length = %d, .direction = %d\n", i,
// 		       conv_blk->vm_offset, conv_blk->len, conv_blk->direction);
// 		conv_blk++;
// 	}
}
#endif /* DBG_LED_CONFS */

// Optimized code: 13.7.2015
static void uio_pruss_fb_led_pack_bytes_to_byte_normal(u16 *bp_index, void *bp_mem,
					 u32 *vm_index, void *fb_mem)
{
	u8 bp_data = 0;
	u16 *fb_mem_orig = (u16 *)&(((u16 *)fb_mem)[*vm_index]);
	
	bp_data = (u8)((fb_mem_orig[0] & 0x01) << 7) |
		  (u8)((fb_mem_orig[1] & 0x01) << 6) |
		  (u8)((fb_mem_orig[2] & 0x01) << 5) |
		  (u8)((fb_mem_orig[3] & 0x01) << 4) |
		  (u8)((fb_mem_orig[4] & 0x01) << 3) |
		  (u8)((fb_mem_orig[5] & 0x01) << 2) |
		  (u8)((fb_mem_orig[6] & 0x01) << 1) |
		  (u8)((fb_mem_orig[7] & 0x01));
	
	(*vm_index) += 8;
	
	*(u8 *)(bp_mem + *bp_index) = bp_data;
	(*bp_index)++;
}

static void uio_pruss_fb_led_pack_bytes_to_byte_inverted(u16 *bp_index, void *bp_mem,
					 u32 *vm_index, void *fb_mem)
{
	u8 bp_data = 0;
	u16 *fb_mem_orig = (u16 *)&(((u16 *)fb_mem)[*vm_index]);
	
	bp_data = (u8)((fb_mem_orig[0] & 0x01)) |
		  (u8)((fb_mem_orig[1] & 0x01) << 1) |
		  (u8)((fb_mem_orig[2] & 0x01) << 2) |
		  (u8)((fb_mem_orig[3] & 0x01) << 3) |
		  (u8)((fb_mem_orig[4] & 0x01) << 4) |
		  (u8)((fb_mem_orig[5] & 0x01) << 5) |
		  (u8)((fb_mem_orig[6] & 0x01) << 6) |
		  (u8)((fb_mem_orig[7] & 0x01) << 7);
	
	(*vm_index) += 8;
	
	*(u8 *)(bp_mem + *bp_index) = bp_data;
	(*bp_index)--;
}

static void uio_pruss_fb_led_normal_shift(u8* bp_data, u8* previous_bits, u8 num, u8 direction)
{

    /** Here we take previous bits in store. 
     *  This needs to be temp variable because we
     *  still need those previous bits from previous byte. */
    u8 tmp = *bp_data;

    if (direction) {
        /** Shift current data. */
        *bp_data = ((*bp_data) >> num);
        /** Restore bits from previous */
        *bp_data = ((*bp_data) | ((*previous_bits) << (8-num)));
        /** Store current. */
        if (num == 1) {
             *previous_bits = tmp & 0x01;
        } else if (num == 2) {
             *previous_bits = tmp & 0x03;
        } else if (num == 3) {
             *previous_bits = tmp & 0x07;
        } else if (num == 4) {
             *previous_bits = tmp & 0x0F;
        } else if (num == 5) {
             *previous_bits = tmp & 0x1F;
        } else if (num == 6) {
             *previous_bits = tmp & 0x3F;
        } else {
             *previous_bits = tmp & 0x7F;
        }

    } else {
        /** Shift current data. */
        *bp_data = ((*bp_data) << num);
        /** Restore bits from previous */
        *bp_data = ((*bp_data) | (*previous_bits));
        /** Store current. */
        *previous_bits = (tmp >> (8-num));
    }
}

// Shifting byte code: 27.3.2018
static void uio_pruss_fb_led_pack_bytes_to_byte_normal_with_shift(u16 *bp_index, void *bp_mem,
					 u32 *vm_index, void *fb_mem, u8 shift_num, 
                                         u8 shift_direction, u8* previous_data_bits)
{
	u8 bp_data = 0;
	u16 *fb_mem_orig = (u16 *)&(((u16 *)fb_mem)[*vm_index]);

        bp_data = (u8)((fb_mem_orig[0] & 0x01) << 7) |
		  	(u8)((fb_mem_orig[1] & 0x01) << 6) |
		  	(u8)((fb_mem_orig[2] & 0x01) << 5) |
		  	(u8)((fb_mem_orig[3] & 0x01) << 4) |
		  	(u8)((fb_mem_orig[4] & 0x01) << 3) |
		  	(u8)((fb_mem_orig[5] & 0x01) << 2) |
		  	(u8)((fb_mem_orig[6] & 0x01) << 1) |
		  	(u8)((fb_mem_orig[7] & 0x01));
        if (shift_num) {
            uio_pruss_fb_led_normal_shift(&bp_data, previous_data_bits, shift_num, shift_direction);
        } else {
            *previous_data_bits = bp_data;
        }

	(*vm_index) += 8;

	*(u8 *)(bp_mem + *bp_index) = bp_data;
	(*bp_index)++;
}

static void uio_pruss_fb_led_pack_bytes_to_byte_inverted_with_shift(u16 *bp_index, void *bp_mem,
					 u32 *vm_index, void *fb_mem, u8 shift_num,
                                         u8 shift_direction, u8* previous_data_bits)
{
	u8 bp_data = 0;
	u16 *fb_mem_orig = (u16 *)&(((u16 *)fb_mem)[*vm_index]);

	bp_data = (u8)((fb_mem_orig[0] & 0x01)) |
		  (u8)((fb_mem_orig[1] & 0x01) << 1) |
		  (u8)((fb_mem_orig[2] & 0x01) << 2) |
		  (u8)((fb_mem_orig[3] & 0x01) << 3) |
		  (u8)((fb_mem_orig[4] & 0x01) << 4) |
		  (u8)((fb_mem_orig[5] & 0x01) << 5) |
		  (u8)((fb_mem_orig[6] & 0x01) << 6) |
		  (u8)((fb_mem_orig[7] & 0x01) << 7);
        if (shift_num) {
            uio_pruss_fb_led_normal_shift(&bp_data, previous_data_bits, shift_num, shift_direction);
        } else {
            *previous_data_bits = bp_data;
        }

	(*vm_index) += 8;

	*(u8 *)(bp_mem + *bp_index) = bp_data;
	(*bp_index)--;
}

#define Swap4Bytes(val) \
	 ( (((val) >> 24) & 0x000000FF) | (((val) >>  8) & 0x0000FF00) | \
	   (((val) <<  8) & 0x00FF0000) | (((val) << 24) & 0xFF000000) )

void uio_pruss_fb_led_to_bitplane_data(void *bp_mem, struct uio_pruss_fb_led_conv_block *conv_blk,
				u16 num_blk, u8 mux_blk, u16 bp_width,
				void *fb_mem, u_long line_len)
{
	u32 i = 0, mux = 0;
	u16 cur_blk;
	struct uio_pruss_fb_led_conv_block *blk;
	u32 vm_index = 0;
	u16 bp_index = 0, bp_index_tmp = 0;
	u8 previous_bits = 0;
	
	for (mux = 0; mux < mux_blk; mux++) {
	    while (bp_index < bp_width*(mux + 1)) {
		
		blk = conv_blk;
		for (cur_blk = 0; cur_blk < num_blk; cur_blk++) {
		    /* Direction flag:
		     * decrementing offset - bytes normal order: 0
		     * incrementing offset - bytes inverted order: 1
		     * offset define in driver file- bytes normal order: 2
		     * offset define in driver file- bytes inverted order: 3
		     *
		     * Bits: 7 - 5 are reserved for shift number.
		     * Bit: 4 is reserved for shift direction: 0 = left, 1 = right
		     *
		     */
		    if (blk->direction == 0) {
			vm_index = (blk->vm_offset - ((mux)*(line_len/2)));
			bp_index_tmp = bp_index;
			for (i = 0; i < blk->len; i++) {
			    uio_pruss_fb_led_pack_bytes_to_byte_normal(&bp_index_tmp, bp_mem, &vm_index, fb_mem);
			}
		    } else if (blk->direction == 1) {
			vm_index = (blk->vm_offset + ((mux)*(line_len/2)));
			bp_index_tmp = bp_index + (blk->len - 1);
			for (i = 0; i < blk->len; i++) {
			    uio_pruss_fb_led_pack_bytes_to_byte_inverted(&bp_index_tmp, bp_mem, &vm_index, fb_mem);
			}
		    } else if (blk->direction == 2) {
			vm_index = blk->vm_offset;
			bp_index_tmp = bp_index;
			for (i = 0; i < blk->len; i++) {
			    uio_pruss_fb_led_pack_bytes_to_byte_normal(&bp_index_tmp, bp_mem, &vm_index, fb_mem);
			}
		    } else if (blk->direction == 3) {
			vm_index = blk->vm_offset;
			bp_index_tmp = bp_index + (blk->len - 1);
			for (i = 0; i < blk->len; i++) {
			    uio_pruss_fb_led_pack_bytes_to_byte_inverted(&bp_index_tmp, bp_mem, &vm_index, fb_mem);
			}
		    } else if ((blk->direction & 0x0F) ==  2) {
			vm_index = blk->vm_offset;
			bp_index_tmp = bp_index;
			for (i = 0; i < blk->len; i++) {
			    uio_pruss_fb_led_pack_bytes_to_byte_normal_with_shift(&bp_index_tmp, 
                                          bp_mem, &vm_index, fb_mem, 
                                          (blk->direction >> 5), ((blk->direction & 0x10) >> 4), &previous_bits);
			}
		    } else if ((blk->direction & 0x0F) == 3) {
			vm_index = blk->vm_offset;
			bp_index_tmp = bp_index + (blk->len - 1);
			for (i = 0; i < blk->len; i++) {
			    uio_pruss_fb_led_pack_bytes_to_byte_inverted_with_shift(&bp_index_tmp, 
                                           bp_mem, &vm_index, fb_mem,
                                          (blk->direction >> 5), ((blk->direction & 0x10) >> 4), &previous_bits);
			}
		    } else {
			/*
			 * Make sure compatibility with older drivers.
			 * This is the same as direction = 1.
			 */
			vm_index = (blk->vm_offset + ((mux)*(line_len/2)));
			bp_index_tmp = bp_index + (blk->len - 1);
			for (i = 0; i < blk->len; i++) {
			    uio_pruss_fb_led_pack_bytes_to_byte_inverted(&bp_index_tmp, bp_mem, &vm_index, fb_mem);
			}
		    }
		    
		    bp_index += blk->len;
		    blk++;
		}
	    }
	}
	
	for (i = 0; i < (bp_width*mux)/sizeof(u32); i++) {
	    ((u32*)bp_mem)[i] = Swap4Bytes(((u32*)bp_mem)[i]);
	}
}

/*****************************************************************************/

static const struct attribute *uio_pruss_fb_led_conv_attrs[] = {
};

static const struct attribute_group uio_pruss_fb_led_conv_device_attr_group = {
        .attrs = (struct attribute **) uio_pruss_fb_led_conv_attrs,
};

/*****************************************************************************/

void uio_pruss_fb_led_fb_memcpy_tobitplane(void *fb_mem, u_long fb_mem_sz, u_long line_len)
{
	u8 i = 0;
    
	for (i = 0; i < NUM_CHAINS; i++) {
// 		printk("CHAIN: %d\n", i);
		uio_pruss_fb_led_to_bitplane_data(
					uio_pruss_fb_chains_p[i]->wr_bp_mem,
					uio_pruss_fb_chains_p[i]->conv_blk,
					uio_pruss_fb_chains_p[i]->conv_blk_sz,
					uio_pruss_fb_chains_p[i]->mux,
					uio_pruss_fb_chains_p[i]->bp_width, //lpp01_chains_p[i]->bp_mem_sz/8, // CHECK THIS!!!!
					fb_mem,
					line_len);
		/* Swap bitplane buffers by setting the new pointer */
		uio_pruss_fb_led_set_visible_chain_data_pointer(i, uio_pruss_fb_chains_p[i]->wr_bp_mem_addr);
		
		if (uio_pruss_fb_chains_p[i]->wr_bp_mem_addr == uio_pruss_fb_chains_p[i]->bp0_mem_addr) {
			uio_pruss_fb_chains_p[i]->wr_bp_mem = uio_pruss_fb_chains_p[i]->bp1_mem;
			uio_pruss_fb_chains_p[i]->wr_bp_mem_addr = uio_pruss_fb_chains_p[i]->bp1_mem_addr;
		} else {
			uio_pruss_fb_chains_p[i]->wr_bp_mem = uio_pruss_fb_chains_p[i]->bp0_mem;
			uio_pruss_fb_chains_p[i]->wr_bp_mem_addr = uio_pruss_fb_chains_p[i]->bp0_mem_addr;
		}
// 		    lpp01_led_dump_chainmem(&lpp01_chains[i], 20);
	}
}
EXPORT_SYMBOL(uio_pruss_fb_led_fb_memcpy_tobitplane);

void uio_pruss_fb_led_fb_clr_bitplanes(void)
{
	u8 chain_index = 0;
    
	for (chain_index = 0; chain_index < NUM_CHAINS; chain_index++) {
		memset(uio_pruss_fb_chains_p[chain_index]->bp0_mem, 0, uio_pruss_fb_chains_p[chain_index]->bp_mem_sz);
		memset(uio_pruss_fb_chains_p[chain_index]->bp1_mem, 0, uio_pruss_fb_chains_p[chain_index]->bp_mem_sz);
	}
}
EXPORT_SYMBOL(uio_pruss_fb_led_fb_clr_bitplanes);

int uio_pruss_fb_led_create_sysfs_conv_nodes(struct platform_device *pdev)
{
	int retval = -ENOMEM;
	
	/* Create the sysfs interface for the driver */
	retval = sysfs_create_group(&pdev->dev.kobj, &uio_pruss_fb_led_conv_device_attr_group);
	
	return retval;
}
EXPORT_SYMBOL(uio_pruss_fb_led_create_sysfs_conv_nodes);

void uio_pruss_fb_led_remove_sysfs_conv_nodes(struct platform_device *pdev)
{
    sysfs_remove_group(&pdev->dev.kobj, &uio_pruss_fb_led_conv_device_attr_group);
}
EXPORT_SYMBOL(uio_pruss_fb_led_remove_sysfs_conv_nodes);


/*
 * This function calculates how many LEDs there is in all the display.
 */
int uio_pruss_fb_led_conv_get_led_count(void)
{
	return uio_pruss_fb_led_count;
}
EXPORT_SYMBOL(uio_pruss_fb_led_conv_get_led_count);

/* Assign to each chain two memory regions one for each bitplane */
int uio_pruss_fb_led_conv_init(void *bp_mem, struct platform_device *pdev)
{
	/*
	 * The firmware is binary data containing converter blocks and
	 * chains configuration.
	 */
	const struct firmware *fw0;
	int retval = -ENODEV;
	u8 chain_index = 0;
	u8 *mem = bp_mem;
	u32 addr = PRUS_BITPLANES_START_ADDR; // This address is used by PRUS
	u32 conv_blk_sz = 0;
	void *data;
	
	/* Load the binary data */
	retval = request_firmware(&fw0, LED_CONV_CONF_NAME, &pdev->dev);
	if (retval < 0) {
	    dev_err(&pdev->dev, "error requesting firmware (%d)\n", retval);
	    return retval;
	}
	dev_info(&pdev->dev, "fw (PRU0) size %td downloading...\n", fw0->size);
	
	/* Data of the loaded firmware file */
	data = (void *)fw0->data;
	for (chain_index = 0; chain_index < NUM_CHAINS; chain_index++) {
// 		printk("%s: reserve %d bytes for chain 1 conf\n", __FUNCTION__, sizeof(struct lpp01_led_chain));
		uio_pruss_fb_chains_p[chain_index] = kmalloc(sizeof(struct uio_pruss_fb_led_chain), GFP_KERNEL);
		if (!uio_pruss_fb_chains_p[chain_index])
			goto conv_init_err1;
		
		memcpy(uio_pruss_fb_chains_p[chain_index], data, sizeof(struct uio_pruss_fb_led_chain));
		data += sizeof(struct uio_pruss_fb_led_chain);
	}
	
	for (chain_index = 0; chain_index < NUM_CHAINS; chain_index++) {
		/* conv_blk_sz is in bytes */
		conv_blk_sz = sizeof(struct uio_pruss_fb_led_conv_block) * uio_pruss_fb_chains_p[chain_index]->conv_blk_sz;
// 		printk("%s: reserve %d bytes for chain 1 conv blocks\n",
// 		       __FUNCTION__, conv_blk_sz);
		conv_blocks_chains_p[chain_index] = kmalloc(conv_blk_sz, GFP_KERNEL);
		if (!conv_blocks_chains_p[chain_index])
			goto conv_init_err2;
		
		memcpy(conv_blocks_chains_p[chain_index], data, conv_blk_sz);
		uio_pruss_fb_chains_p[chain_index]->conv_blk = conv_blocks_chains_p[chain_index];
		data += conv_blk_sz;
#ifdef DBG_LED_CONFS
		uio_pruss_fb_led_dump_chain_conf(chain_index);
#endif
	}
	
	
	for (chain_index = 0; chain_index < NUM_CHAINS; chain_index++) {
		uio_pruss_fb_chains_p[chain_index]->bp0_mem = mem;
		uio_pruss_fb_chains_p[chain_index]->bp0_mem_addr = addr;
		uio_pruss_fb_led_set_visible_chain_data_pointer(chain_index, uio_pruss_fb_chains_p[chain_index]->bp0_mem_addr);
		memset(uio_pruss_fb_chains_p[chain_index]->bp0_mem, 0, uio_pruss_fb_chains_p[chain_index]->bp_mem_sz);
		mem += uio_pruss_fb_chains_p[chain_index]->bp_mem_sz;
		addr += uio_pruss_fb_chains_p[chain_index]->bp_mem_sz;
		
		uio_pruss_fb_chains_p[chain_index]->bp1_mem = mem;
		uio_pruss_fb_chains_p[chain_index]->bp1_mem_addr = addr;
		uio_pruss_fb_led_set_visible_chain_data_pointer(chain_index, uio_pruss_fb_chains_p[chain_index]->bp1_mem_addr);
		memset(uio_pruss_fb_chains_p[chain_index]->bp1_mem, 0, uio_pruss_fb_chains_p[chain_index]->bp_mem_sz);
		mem += uio_pruss_fb_chains_p[chain_index]->bp_mem_sz;
		addr += uio_pruss_fb_chains_p[chain_index]->bp_mem_sz;
		
		/* 
		 * After init, the visible bitplane is number 1.
		 * Set writable bitplane number 0.
		 */
		uio_pruss_fb_chains_p[chain_index]->wr_bp_mem = uio_pruss_fb_chains_p[chain_index]->bp0_mem;
		uio_pruss_fb_chains_p[chain_index]->wr_bp_mem_addr = uio_pruss_fb_chains_p[chain_index]->bp0_mem_addr;
		
		uio_pruss_fb_led_count += uio_pruss_fb_chains_p[chain_index]->bp_mem_sz * 8;
	}
	
	release_firmware(fw0);
	
	return 0;
	
conv_init_err2:
	// Release the memory allocated if any.
	for (chain_index = 0; chain_index < NUM_CHAINS; chain_index++) {
		if (conv_blocks_chains_p[chain_index])
			kfree(conv_blocks_chains_p[chain_index]);
	}
	
conv_init_err1:
	// Release the memory allocated if any.
	for (chain_index = 0; chain_index < NUM_CHAINS; chain_index++) {
		if (uio_pruss_fb_chains_p[chain_index])
			kfree(uio_pruss_fb_chains_p[chain_index]);
	}
	release_firmware(fw0);

	return retval;
}
EXPORT_SYMBOL(uio_pruss_fb_led_conv_init);

void uio_pruss_fb_led_conv_release(void)
{
	u8 chain_index = 0;

	for (chain_index = 0; chain_index < NUM_CHAINS; chain_index++) {
		if (conv_blocks_chains_p[chain_index]) {
// 			printk("%s: free mem conv blk chain %d\n", __FUNCTION__, chain_index);
			kfree(conv_blocks_chains_p[chain_index]);
		}
	}
	
	for (chain_index = 0; chain_index < NUM_CHAINS; chain_index++) {
		if (uio_pruss_fb_chains_p[chain_index]) {
// 			printk("%s: free mem conf chain %d\n", __FUNCTION__, chain_index);
			kfree(uio_pruss_fb_chains_p[chain_index]);
		}
	}
}
EXPORT_SYMBOL(uio_pruss_fb_led_conv_release);

void uio_pruss_fb_led_conv_get_bitplane_pointer(void **bp_mem, int chain_index)
{
	if (uio_pruss_fb_chains_p[chain_index]->bp0_mem == uio_pruss_fb_chains_p[chain_index]->wr_bp_mem)
	{
		*bp_mem = uio_pruss_fb_chains_p[chain_index]->bp1_mem;
	} else {
		*bp_mem = uio_pruss_fb_chains_p[chain_index]->bp0_mem;
	}
}
EXPORT_SYMBOL(uio_pruss_fb_led_conv_get_bitplane_pointer);
