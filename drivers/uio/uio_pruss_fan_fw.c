#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/firmware.h>
#include <linux/platform_device.h>

#include "uio_pruss_fan_mem.h"

#define FW0_NAME		"pru_fan_fw0.bin"
#define FW1_NAME		"pru_fan_fw1.bin"


int uio_pruss_fan_fw_load(struct platform_device *pdev, tprussdrv *data)
{
	const struct firmware *fw0, *fw1;
	int retval = -ENODEV;
	
	retval = request_firmware(&fw0, FW0_NAME, &pdev->dev);
	if (retval < 0) {
	    dev_err(&pdev->dev, "error requesting firmware (%d)\n", retval);
	    return retval;
	}
	dev_info(&pdev->dev, "%s (PRU0) size %td downloading...\n", FW0_NAME, fw0->size);
    
	retval = request_firmware(&fw1, FW1_NAME, &pdev->dev);
	if (retval < 0) {
	    dev_err(&pdev->dev, "error requesting firmware (%d)\n", retval);
	    goto err1;
	}
	dev_info(&pdev->dev, "%s (PRU1) size %td downloading...\n", FW1_NAME, fw1->size);

	/*
	* pru_load returns
	*   0: OK
	*   1: failure
	* It looks like the pru_lib is not developed using kernel types.
	*/
	retval = pru_load(PRU_NUM0, 
	                  (unsigned int *)fw0->data,
                      fw0->size, data);

	if (retval) {
	    retval = -ENXIO;
	    dev_err(&pdev->dev, "cannot load firmware (%d)\n", retval);
	    goto err2;
	}

	retval = pru_load(PRU_NUM1, 
	                  (unsigned int *)fw1->data,
                      fw1->size, data);
	if (retval) {
	    retval = -ENXIO;
	    dev_err(&pdev->dev, "cannot load firmware (%d)\n", retval);
	    goto err2;
	}

	/* User must start the PRUSS */
	data->pruss_running = 0;
	
	release_firmware(fw0);

	release_firmware(fw1);

	return 0;
	
err2:
	release_firmware(fw1);
err1:
	release_firmware(fw0);
	
	return retval;

}
EXPORT_SYMBOL(uio_pruss_fan_fw_load);

static void uio_pruss_fan_fw_check_running_state(tprussdrv *data)
{
	if (data->pruss_0_running || data->pruss_1_running) {
		data->pruss_running = 1;
	} else {
		data->pruss_running = 0;
	}
}

int uio_pruss_fan_fw_start(struct device *dev, int pru_id)
{
	tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
	int ret = 0;

	printk(KERN_INFO "Starting PRU %d\n", pru_id);
	ret = pru_run(pru_id, data);
	if (ret) {
		dev_err(dev, "can't start PRU%d (%d)\n", pru_id, ret);
		return -1;
	}

	if (pru_id == PRU_NUM0)
                data->pruss_0_running = 1;
	if (pru_id == PRU_NUM1)
                data->pruss_1_running = 1;

	uio_pruss_fan_fw_check_running_state(data);

	return 0;
}
EXPORT_SYMBOL(uio_pruss_fan_fw_start);

int uio_pruss_fan_fw_stop(struct device *dev, int pru_id)
{
    tprussdrv *data = DEV_GET_PRUSS_DATA(dev);
	int ret = 0;

	ret = pru_stop(pru_id, data);
	if (ret) {
		dev_err(dev, "can't stop PRU%d (%d)\n", pru_id, ret);
		return ret;
	}
	if (pru_id == PRU_NUM0)
                data->pruss_0_running = 0;
	if (pru_id == PRU_NUM1)
                data->pruss_1_running = 0;
	
	uio_pruss_fan_fw_check_running_state(data);

	return 0;
}
EXPORT_SYMBOL(uio_pruss_fan_fw_stop);