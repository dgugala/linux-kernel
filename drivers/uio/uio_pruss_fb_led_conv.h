#ifndef __UIO_PRUSS_FB_LED_CONV_H__
#define __UIO_PRUSS_FB_LED_CONV_H__

#include <linux/types.h>

#ifdef __KERNEL__
#include <linux/module.h>
#include <linux/platform_device.h>
#endif /* __KERNEL__ */

/*****************************************************************************/

#define NUM_CHAINS		4

/*****************************************************************************/

struct uio_pruss_fb_led_conv_block {
	__u32 vm_offset;	// Video Memory offset (Framebuffer)
	__u32 bp_offset;	// Bitplane Memory offset
	__u16 len;		// In bytes
	__u8 direction;	// 0: normal; 1: inverted.
};

struct uio_pruss_fb_led_chain {
	void *bp0_mem;		// Bitplane 0 memory.
	__u32 bp0_mem_addr; 	// RAM address of bitplane 0. This address is used by PRUS.
	void *bp1_mem;		// Bitplane 1 memory.
	__u32 bp1_mem_addr; 	// RAM address of bitplane 1. This address is used by PRUS.
	__u16 bp_mem_sz;
	__u16 bp_width;		// Number of leds in one mux address, in bytes.
	__u16 width;		// In pixels.
	__u16 height;		// In pixels.
	__u16 mux;
	struct uio_pruss_fb_led_conv_block *conv_blk;
	__u16 conv_blk_sz;
	void *wr_bp_mem;	// Pointer to the memory buffer where new data will be written.
				// This is the back bitplane buffer.
	__u32 wr_bp_mem_addr;	// RAM address of the back bitplane. Used by PRUS.
};

/*****************************************************************************/

#ifdef __KERNEL__
int uio_pruss_fb_led_conv_init(void *bp_mem, struct platform_device *pdev);
void uio_pruss_fb_led_conv_release(void);
void uio_pruss_fb_led_fb_memcpy_tobitplane(void *fb_mem, u_long fb_mem_sz, u_long line_len);
void uio_pruss_fb_led_fb_clr_bitplanes(void);
int uio_pruss_fb_led_conv_get_led_count(void);
void uio_pruss_fb_led_conv_get_bitplane_pointer(void **bp_mem, int chain);
#endif /* __KERNEL__ */

#endif /* __UIO_PRUSS_FB_LED_CONV_H */
