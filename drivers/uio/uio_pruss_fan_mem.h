#ifndef __UIO_PRUSS_FAN_MEM_H__
#define __UIO_PRUSS_FAN_MEM_H__

#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/clk.h>

#include "prussdrv.h"


typedef struct uio_pruss_dev {
        struct uio_info *info;
        tprussdrv *pru_data;
        struct clk *pruss_clk;
        dma_addr_t sram_paddr;
        dma_addr_t ddr_paddr;
        void __iomem *prussio_vaddr;
        unsigned long sram_vaddr;
        void *ddr_vaddr;
        unsigned int hostirq_start;
        unsigned int pintc_base;
        struct gen_pool *sram_pool;
} t_uio_pruss_dev;

#define DEV_GET_PRUSS_DATA(x) ((((t_uio_pruss_dev*)dev_get_drvdata(x))->pru_data))

void uio_pruss_fan_mem_write_data_u32(u32 offset, u32 *datatowrite, u16 wordstowrite, tprussdrv *data, int target_mem);
void uio_pruss_fan_mem_read_data_u32(u32 offset, u32 *datatoread, u16 wordstoread, tprussdrv *data, int target_mem);

tprussdrv* uio_pruss_fan_mem_create_data(void);
void uio_pruss_fan_mem_remove_data(tprussdrv* data);
int uio_pruss_fan_mem_init_pruss(tprussdrv *data);

#endif /* __UIO_PRUSS_FAN_MEM_H */

