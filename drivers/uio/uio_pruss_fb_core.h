#ifndef __UIO_PRUSS_FB_CORE_H__
#define __UIO_PRUSS_FB_CORE_H__

void uio_pruss_fb_core_set_prussdrv(tprussdrv* drv);
struct uio_pruss_fb_info* uio_pruss_fb_core_create(struct platform_device *dev);
void uio_pruss_fb_core_remove(struct uio_pruss_fb_info* info);
void* uio_pruss_fb_core_get_bitplanememory_pointer(void);

#endif /* __UIO_PRUSS_FB_CORE_H__ */
