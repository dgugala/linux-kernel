#ifndef __UIO_PRUSS_FAN_CONFIG_H__
#define __UIO_PRUSS_FAN_CONFIG_H__

#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/clk.h>

/* 8 PWM and 8 Tacho signals */
#define NUM_OF_FANS             8
#define FAN_CONFIG_OFFSET       0x0010
#define FAN_TACHO_OFFSET        0x0040

#define PERIOD_DEF              1000
#define DUTY_CYCLE_DEF          0
#define ENABLE_DEF              0
#define TACHO_DEF		0xBEEF

typedef struct fan_config {
        u16 duty_cycle;
        u16 period;
        u8  enable;
        u8  reserved_1;
        u8  reserved_2;
        u8  reserved_3;
} t_fan_config;

typedef struct fan_feedback {
	u32 tacho;
} t_fan_feedback;

void uio_pruss_fan_init_configs(void);
t_fan_config *uio_pruss_fan_get_config(unsigned int fan_number);
t_fan_feedback *uio_pruss_fan_get_feedback(unsigned int fan_number);

#endif /* __UIO_PRUSS_FAN_CONFIG_H */

