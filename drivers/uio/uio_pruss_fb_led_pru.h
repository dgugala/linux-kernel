#ifndef __UIO_PRUSS_FB_LED_PRU_H__
#define __UIO_PRUSS_FB_LED_PRU_H__

#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/clk.h>

#include "prussdrv.h"
#include "__prussdrv.h"


// Define the default values of the chain configuration registers
#define LATCH_ON_DEF		0x00015F90
#define NEXT_SYNC_DEF		0x00015F90
#define SHIFT_REG_OUT_DEF	0x12345678
#define	SHIFT_REG_IN_DEF	0x00000000
#define NUM_OF_FRAMES_DEF	0x00FF
#define NUM_OF_CHAIN_LEDS_DEF	0x0060
#define LED_BIT_COUNTER_DEF	0x0060
#define LATCH_LEN_DEF		0x40
#define LAST_ADDRESS_DEF	0x07
#define BIT_CONFIGS_DEF		0x01
#define WORD_WIDTH_BITS_DEF	0x1F
#define II_BIT_COUNTER_DEF	0x1F
#define ADDRESS_COUNTER_DEF	0x07
#if 0
#define DATA_RAM_OFFSET_0	0x0000
#define DATA_RAM_SIZE_0		0x0200 // 512 bytes
#define DATA_RAM_OFFSET_1	0x2000
#define DATA_RAM_SIZE_1		0x0200 // 512 bytes
#endif

#define PRUSS_ERR_CNT_OFFSET		0x0000
#define PRUSS_MAX_ERR_CNT_OFFSET	0x0004
#define PRUSS_CHAIN_FEEDBACK_OFFSET	0x0008

#if 1
#define CONF_REGS_OFFSET	0x0144
#else
#define CONF_REGS_OFFSET_0	0x0144
#define CONF_REGS_OFFSET_1	0x2144
#endif
#if 0
// PRU0 pointers to the begining of the bitplane configuration blocks
#define BITPLANE0_DATA_BLOCK_ADDR	0x00000100 // Chain 1 bitplane 0
#define BITPLANE1_DATA_BLOCK_ADDR	0x00000104 // Chain 1 bitplane 1
#define BITPLANE2_DATA_BLOCK_ADDR	0x00000108 // Chain 2 bitplane 0
#define BITPLANE3_DATA_BLOCK_ADDR	0x0000010c // Chain 2 bitplane 1

// PRU1 pointers to the begining of the bitplane configuration blocks
#define BITPLANE4_DATA_BLOCK_ADDR	0x00002100 // Chain 3 bitplane 0
#define BITPLANE5_DATA_BLOCK_ADDR	0x00002104 // Chain 3 bitplane 1
#define BITPLANE6_DATA_BLOCK_ADDR	0x00002108 // Chain 4 bitplane 0
#define BITPLANE7_DATA_BLOCK_ADDR	0x0000210c // Chain 4 bitplane 1
#else
/* PRU0 and PRU1 bitplain pointers to the gegining of the 
 * bitplane configuration blocks. 
 * There is two chains in one PRU. Chain 1 and Chain 2.
 */
#define BITPLANE0_CONFIG_OFFSET		0x0010
#define BITPLANE1_CONFIG_OFFSET		0x0030
#define BITPLANE2_CONFIG_OFFSET		0x0050
#define BITPLANE3_CONFIG_OFFSET		0x0070
#define BITPLANE0_DATA_BLOCK_OFFSET	0x00000100 // Chain 1 bitplane 0
#define BITPLANE1_DATA_BLOCK_OFFSET	0x00000104 // Chain 1 bitplane 1
#define BITPLANE2_DATA_BLOCK_OFFSET	0x00000108 // Chain 2 bitplane 0
#define BITPLANE3_DATA_BLOCK_OFFSET	0x0000010c // Chain 2 bitplane 1
#endif

#define FB_START_ADDR		0xc7e00000 // Absolute RAM address not used by kernel or applications
#define PRUS_BITPLANES_MEM_SZ	0x00100000 // 1MB reserved for bitplanes buffers
#define PRUS_BITPLANES_OFFSET	0x00100000 // 1MB reserved for frame buffer memory
					   // Another 1MB is reserved for bitplanes memory used by PRUS
#define PRUS_BITPLANES_START_ADDR FB_START_ADDR + PRUS_BITPLANES_OFFSET

struct uio_pruss_fb_info {
        /** Framebuffer data. */
        struct fb_info *fb_info;
        /* DMA */
        dma_addr_t      vram_phys;
        unsigned long   vram_size;
        void            *vram_virt;
        unsigned int    dma_start;
        unsigned int    dma_end;
};

struct uio_pruss_dev {
        struct uio_info *info;
        struct uio_pruss_fb_info *fb_info;
        tprussdrv *pru_data;
        struct clk *pruss_clk;
        dma_addr_t sram_paddr;
        dma_addr_t ddr_paddr;
        void __iomem *prussio_vaddr;
        unsigned long sram_vaddr;
        void *ddr_vaddr;
        unsigned int hostirq_start;
        unsigned int pintc_base;
        struct gen_pool *sram_pool;
};

typedef struct uio_pruss_fb_led_bitplane_config {
	void	*data_pointer;		// where are led data words in the main memory
	u32	latch_on;		// active edge position compared to 0 point
	u32	next_sync;		// cycle counter value of next Latch state toggling
	u32	shift_reg_out;		// data word of led data out
	u32	shift_reg_in;		// data word of led data in
	u16	number_of_frames;	// how many frames before frame buffer is changed
	u16	number_of_chain_leds;	// how many leds one led controller chain 0..leds/chain-1
	u16	led_bit_counter;	// led number in one led controller chain leds/chain-1..0
	u8	latch_length;		// active pulse length as number of 1/150MHz pulses (6.67nS)
	u8	last_address;		// last multiplexing address in this channel  0..0x0f
	u8	bit_configs;		// B0=+1 or 0 depending the chain 1 mux direction, +1 is increasing address
					// B1= idle state of SCK  (after passive edge)
					// B2= non active state of Latch
					// B3=
					// B4= frame done handshaking
					// B5= dir_byte
					// B6= 
					// B7= next_latch_on/off
	u8	word_width_bits;	// how many bits (1..0x1f) are used of 4 bytes fetched from
					// frame buffer at one time are valid
	u8	ii;			// bit counter for shift_reg 0..word_width-1
	u8	address_counter;	// existing mux address, last address..0
} tled_bitplane_config;

tprussdrv* uio_pruss_fb_led_pru_create_data(void);
void uio_pruss_fb_led_pru_remove_data(tprussdrv* data);

int uio_pruss_fb_led_create_sysfs_pruss_nodes(struct platform_device *pdev);
void uio_pruss_fb_led_remove_sysfs_pruss_nodes(struct platform_device *pdev);

int uio_pruss_fb_led_load_fw(struct platform_device *pdev, tprussdrv *data);
int uio_pruss_fb_led_load_diagnostics_fw(struct platform_device *pdev, tprussdrv *data);

void uio_pruss_fb_led_set_visible_chain_data_pointer(u8 chain_index, u32 data_addr);
int uio_pruss_fb_led_init_chain_configs(tprussdrv *data);

void uio_pruss_fb_led_pru0_ram_write_data_u32(u32 offset, u32 *datatowrite,
                u16 wordstowrite, tprussdrv *data);
void uio_pruss_fb_led_pru1_ram_write_data_u32(u32 offset, u32 *datatowrite,
                u16 wordstowrite, tprussdrv *data);
/*
void uio_pruss_fb_led_pru_ram_write_data_u32(u32 offset, u32 *datatowrite,
	    u16 wordstowrite, tprussdrv *data);*/
int uio_pruss_fb_led_wait_frame_sync(void);

#endif /* __UIO_PRUSS_FB_LED_PRU_H */
