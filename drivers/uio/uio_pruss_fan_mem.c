#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/firmware.h>

#include "prussdrv.h"
#include "uio_pruss_fan_config.h"

/** Internal Pruss driver data pointer */
static tprussdrv* uio_pruss_fan_data = NULL;

tprussdrv* uio_pruss_fan_mem_create_data(void)
{
	tprussdrv* data;
	data = kzalloc(sizeof(tprussdrv), GFP_KERNEL);
	if (data)
		uio_pruss_fan_data = data;
	return data;
}
EXPORT_SYMBOL(uio_pruss_fan_mem_create_data);

void uio_pruss_fan_mem_remove_data(tprussdrv* data)
{
	if (data) {
		kfree(data);
	}
	uio_pruss_fan_data = NULL;
}
EXPORT_SYMBOL(uio_pruss_fan_mem_remove_data);

void uio_pruss_fan_mem_write_data_u32(u32 offset, u32 *datatowrite,
		u16 wordstowrite, tprussdrv *data, int target_mem)
{
	offset +=(u32)prussdrv_pru_get_memory_offset(target_mem, data); 

	prussdrv_pru_write_memory_u32(offset,datatowrite, wordstowrite, data);
}
EXPORT_SYMBOL(uio_pruss_fan_mem_write_data_u32);

void uio_pruss_fan_mem_read_data_u32(u32 offset, u32 *datatoread,
	    u16 wordstoread, tprussdrv *data, int target_mem)
{
	offset +=(u32)prussdrv_pru_get_memory_offset(target_mem, data); 

	prussdrv_pru_read_memory_u32(offset, datatoread, wordstoread, data);
}
EXPORT_SYMBOL(uio_pruss_fan_mem_read_data_u32);


int uio_pruss_fan_mem_init_pruss(tprussdrv *data)
{
	u8 i = 0;
	t_fan_config *conf;
	u8 config_size = (sizeof(t_fan_config)/sizeof(u32)*NUM_OF_FANS);
	u32 addr = 0x10;
	u32 fan_data_offset = 0x0;

	uio_pruss_fan_init_configs();

	/* PRU0 init values */
	fan_data_offset = FAN_CONFIG_OFFSET;
	uio_pruss_fan_mem_write_data_u32(fan_data_offset,
                                (u32 *)uio_pruss_fan_get_config(0),
					 			config_size, data, PRUSS0_PRU0_DATARAM);

	// Without the following initializations, PRU software won't be able
	// find the initialization configurations of the bitplanes.
	// PRU0 (PWM 0 - 7)
    uio_pruss_fan_mem_write_data_u32(0x100, &addr, 1, data, PRUSS0_PRU0_DATARAM);

	// PRU1 (Tacho 0 - 7)
	addr = FAN_TACHO_OFFSET;
	uio_pruss_fan_mem_write_data_u32(0x200, &addr, 1, data, PRUSS0_PRU1_DATARAM);

	return 0;
}
EXPORT_SYMBOL(uio_pruss_fan_mem_init_pruss);
