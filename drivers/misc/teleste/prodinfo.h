#ifndef __TELESTE_PRODINFO_H
#define __TELESTE_PRODINFO_H

#include <linux/types.h>

#define PRODINFO_LPP01_ITEMCODE		"95LPP01                         "
#define PRODINFO_LPP02_ITEMCODE		"95LPP02                         "
#define PRODINFO_LPP03_ITEMCODE		"95LPP03                         "
#define PRODINFO_LPP04_ITEMCODE		"95LPP04                         "
#define PRODINFO_LPP05_ITEMCODE		"95LPP05                         "

#define PRODINFO_UDP01_ITEMCODE		"98UDP01                         "



#define PRODINFO_EEPROM_SZ		1024 // 1KB in LPP01

#define PRODINFO_MAX_LEN		255

#define PRODINFO_HEADER_SIZE 		60 // The size of the header is always the same. In Bytes.
#define PRODINFO_PCB_ITEMCODE_LEN	32
#define PRODINFO_PCB_SERNUM_LEN		16
#define PRODINFO_PCB_TESTERNAME_LEN	4

#define PRODINFO_PRODINFOII_ID		11
#define PRODINFO_PRODINFOII_SZ		76 // Bytes
#define PRODINFO_PRODINFOII_ITEMCODE_LEN	32
#define PRODINFO_PRODINFOII_SERNUM_LEN		16
#define PRODINFO_PRODINFOII_VERSION_LEN		16
#define PRODINFO_PRODINFOII_TESTERNAME_LEN	4

#define PRODINFO_MACADDR_ID		2
#define PRODINFO_MACADDR_SZ		11 // Bytes
#define PRODINFO_MACADDR_ADDR_LEN	6

#define PRODINFO_END_SZ		2 // Bytes
#define PRODINFO_END		0xFFFF

#define PRODINFO_TOTAL_SZ	PRODINFO_HEADER_SIZE + PRODINFO_MACADDR_SZ + PRODINFO_PRODINFOII_SZ + PRODINFO_END_SZ

typedef struct prodInfo_header {
	char pcb_itemcode[PRODINFO_PCB_ITEMCODE_LEN];
	char pcb_sernum[PRODINFO_PCB_SERNUM_LEN];
	u8 pcb_hwversion;
	time_t pcb_testdate;
	char pcb_testername[PRODINFO_PCB_TESTERNAME_LEN];
	u16 eeprom_sz;
	u8 checksum; // sum of all bytes (59) of information
} prodInfo_header_t;

typedef struct prodInfo_prodInfoII {
	u16 id;
	u8 size;
	char product_itemcode[PRODINFO_PRODINFOII_ITEMCODE_LEN];
	char product_sernum[PRODINFO_PRODINFOII_SERNUM_LEN];
	char product_version[PRODINFO_PRODINFOII_VERSION_LEN];
	time_t product_testdate;
	char product_testername[PRODINFO_PRODINFOII_TESTERNAME_LEN];
	u8 checksum; // sum of all bytes (75) of information
} prodInfo_prodInfoII_t;

typedef struct prodInfo_macAddr {
	u16 id;
	u8 size;
	u8 interface;
	u8 address[PRODINFO_MACADDR_ADDR_LEN];
	u8 checksum; // sum of all bytes (10) of information
} prodInfo_macAddr_t;

typedef struct lpp_prodInfo_block {
	prodInfo_header_t header;
	prodInfo_prodInfoII_t prodInfoII;
	prodInfo_macAddr_t macAddr;
	u16 endOfInfo;
} lpp_prodInfo_block_t;

ssize_t prodinfo_dump_header_to_buffer(char *buf, int blen);
ssize_t prodinfo_dump_prodInfoII_to_buffer(char *buf, int blen);
ssize_t prodinfo_dump_macAddr_to_buffer(char *buf, int blen);

prodInfo_header_t *prodinfo_get_header_block(void);
prodInfo_macAddr_t *prodinfo_get_macAddr_block(void);
prodInfo_prodInfoII_t *prodinfo_get_prodInfoII_block(void);

int prodinfo_load_data(char *buf);

#endif /* __TELESTE_PRODINFO_H */
