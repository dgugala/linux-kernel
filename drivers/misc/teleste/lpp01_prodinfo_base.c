#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/sched.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/netdevice.h>

#include <prodinfo.h>

#define MODULE_VERS "2.0"
#define MODULE_NAME "lpp01_prodinfo"


static struct proc_dir_entry *lpp01_prodinfo_dir, *header_file,
        *prodInfoII_file, *macAddr_file;


static char read_buffer[1024];

static int header_proc_show(struct seq_file *m, void*v)
{
	seq_printf(m, "LPP01 product info\n");

        prodinfo_dump_header_to_buffer(&read_buffer[0], sizeof(read_buffer)-1);
	seq_printf(m, read_buffer);

	return 0;
}

static int header_proc_open(struct inode *inode, struct file *file)
{
	return single_open(file, header_proc_show, NULL);
}

static const struct file_operations header_proc_fops = {
 .owner = THIS_MODULE,
 .open = header_proc_open,
 .read = seq_read,
 .llseek = seq_lseek,
 .release = single_release,
};

static int prodinfoII_proc_show(struct seq_file *m, void*v)
{
        prodinfo_dump_prodInfoII_to_buffer(&read_buffer[0], sizeof(read_buffer)-1);
	seq_printf(m, read_buffer);

	return 0;
}

static int prodinfoII_proc_open(struct inode *inode, struct file *file)
{
	return single_open(file, prodinfoII_proc_show, NULL);
}

static const struct file_operations prodinfoII_proc_fops = {
 .owner = THIS_MODULE,
 .open = prodinfoII_proc_open,
 .read = seq_read,
 .llseek = seq_lseek,
 .release = single_release,
};

static int mac_proc_show(struct seq_file *m, void*v)
{
        prodinfo_dump_macAddr_to_buffer(&read_buffer[0], sizeof(read_buffer)-1);
	seq_printf(m, read_buffer);

	return 0;
}

static int mac_proc_open(struct inode *inode, struct file *file)
{
	return single_open(file, mac_proc_show, NULL);
}

static const struct file_operations mac_proc_fops = {
 .owner = THIS_MODULE,
 .open = mac_proc_open,
 .read = seq_read,
 .llseek = seq_lseek,
 .release = single_release,
};

static int lpp01_collect_used_mac(void)
{
	struct net_device* dev;
	prodInfo_macAddr_t* mac_storage;
	int i = 0;

	mac_storage = prodinfo_get_macAddr_block();

	read_lock(&dev_base_lock);
	for_each_netdev(&init_net, dev) {
		if ( !strcmp(dev->name, "lo") ) {
			pr_info("Skip loopback device\n");
		} else {
			pr_info( "Used interface %s has MAC: %02x:%02x:%02x:%02x:%02x:%02x\n",
			dev->name,
			dev->dev_addr[0],
			dev->dev_addr[1],
			dev->dev_addr[2],
			dev->dev_addr[3],
			dev->dev_addr[4],
			dev->dev_addr[5] );
			for (i = 0; i < 6; i++) {
				mac_storage->address[i] = dev->dev_addr[i];
			}
			break;
		}

	}
 
	read_unlock(&dev_base_lock);

	return 0;
}

#define UDP_PRODUCT_INFO_EEPROM_FILE "/sys/devices/platform/ocp/44e0b000.i2c/i2c-0/0-0054/eeprom"

static char *eeprom_data = NULL;

static int lpp01_load_eeprom_to_buffer(void)
{
	struct file *f;
	mm_segment_t fs;
	eeprom_data = kmalloc( PRODINFO_EEPROM_SZ, GFP_KERNEL );
	if ( eeprom_data == NULL )
	{
		printk(KERN_ALERT "Failed to malloc needed internal buffer");
		return -ENOMEM;
	}

	f = filp_open( UDP_PRODUCT_INFO_EEPROM_FILE, O_RDONLY, 0 );
	if ( f == NULL ) {
		printk(KERN_ALERT "Failed to open eeprom file: %s", UDP_PRODUCT_INFO_EEPROM_FILE );
		return -ENOENT;
	}

	fs = get_fs();
	set_fs(get_ds());
	f->f_op->read(f, eeprom_data, PRODINFO_EEPROM_SZ, &f->f_pos);
	set_fs(fs);
	printk( KERN_INFO "EEPROM readed to buffer" );
	filp_close(f, NULL);

	prodinfo_load_data( eeprom_data );
	lpp01_collect_used_mac();

	return 0;

}


static int lpp01_free_eeprom_buffers(void)
{
	if ( eeprom_data )
	{
		kfree( eeprom_data );
		eeprom_data = NULL;
	}
	return 0;
}

static int lpp01_prodinfo_init(void)
{
        int rv = 0;

	rv = lpp01_load_eeprom_to_buffer();
	if (rv)
		goto no_mem;

        /* create directory */
        lpp01_prodinfo_dir = proc_mkdir(MODULE_NAME, NULL);
        if ( lpp01_prodinfo_dir == NULL ) {
                rv = -ENOMEM;
                goto no_mem;
        }
	header_file = proc_create("header", 0, lpp01_prodinfo_dir, &header_proc_fops);

        if ( header_file == NULL ) {
                rv = -ENOMEM;
                goto no_header;
        }

	prodInfoII_file = proc_create("prodInfoII", 0, lpp01_prodinfo_dir, &prodinfoII_proc_fops);
        if ( prodInfoII_file == NULL ) {
                rv = -ENOMEM;
                goto no_prodInfoII;
        }

	macAddr_file = proc_create("macAddr", 0, lpp01_prodinfo_dir, &mac_proc_fops);
        if ( macAddr_file == NULL ) {
                rv = -ENOMEM;
                goto no_macAddr;
        }

        /* everything OK */
        printk(KERN_INFO "%s %s initialised\n",
               MODULE_NAME, MODULE_VERS);
	
        return 0;

no_macAddr:
        remove_proc_entry("prodInfoII", lpp01_prodinfo_dir);
no_prodInfoII:
        remove_proc_entry("header", lpp01_prodinfo_dir);
no_header:                           
        remove_proc_entry(MODULE_NAME, NULL);
no_mem:
	lpp01_free_eeprom_buffers();
        return rv;
}


static void lpp01_prodinfo_cleanup(void)
{

        remove_proc_entry("macAddr", lpp01_prodinfo_dir);
        remove_proc_entry("prodInfoII", lpp01_prodinfo_dir);
        remove_proc_entry("header", lpp01_prodinfo_dir);

        remove_proc_entry(MODULE_NAME, NULL);

	lpp01_free_eeprom_buffers();

        printk(KERN_INFO "%s %s removed\n",
               MODULE_NAME, MODULE_VERS);
}


module_init(lpp01_prodinfo_init);
module_exit(lpp01_prodinfo_cleanup);

MODULE_AUTHOR("Teleste");
MODULE_DESCRIPTION("LPPXX / UDPXX Product Information");
MODULE_LICENSE("GPL");
