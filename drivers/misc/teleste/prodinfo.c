#include <linux/mm.h>
#include <linux/ctype.h>

#include <prodinfo.h>

/*
 * Here it is stored the product information for LPP / UDP
 * Product information platform driver will read information from here.
 */
lpp_prodInfo_block_t prodInfo;

/*
 * Dumps the product information header to a given buffer.
 */
ssize_t prodinfo_dump_header_to_buffer(char *buf, int blen)
{
	u16 i = 0;
	ssize_t len = 0;
	prodInfo_header_t *header = &prodInfo.header;
	
	len += snprintf(&buf[len], blen-len, "HEADER:\n");

	len += snprintf(&buf[len], blen-len, "\tPCB item code: ");
	for (i = 0; i < PRODINFO_PCB_ITEMCODE_LEN; i++) {
		if ( isprint(header->pcb_itemcode[i] ) ) 
		{
			len += snprintf(&buf[len], blen-len, "%c", header->pcb_itemcode[i]);
		} 
		else 
		{
			break;
		}
	}
	len += snprintf(&buf[len], blen-len,"\n");
	len += snprintf(&buf[len], blen-len,"\tPCB serial number: ");
	for (i = 0; i < PRODINFO_PCB_SERNUM_LEN; i++) {
		if ( isprint(header->pcb_sernum[i] ) ) 
		{
			len += snprintf(&buf[len], blen-len,"%c", header->pcb_sernum[i]);
		}
		else 
		{
			break;
		}
	}
	len += snprintf(&buf[len], blen-len, "\n");
	len += snprintf(&buf[len], blen-len, "\tPCB hw version: %d\n", header->pcb_hwversion);
	len += snprintf(&buf[len], blen-len, "\tPCB test date: %d (seconds)\n", (u32)header->pcb_testdate);
	len += snprintf(&buf[len], blen-len, "\tPCB tester name: ");
	for (i = 0; i < PRODINFO_PCB_TESTERNAME_LEN; i++) {
		if ( isprint(header->pcb_testername[i] ) ) 
		{
			len += snprintf(&buf[len], blen-len, "%c", header->pcb_testername[i]);
		}
		else 
		{
			break;
		}

	}
	len += snprintf(&buf[len], blen-len, "\n");
	len += snprintf(&buf[len], blen-len, "\tEEPROM size: %d\n", header->eeprom_sz);
	len += snprintf(&buf[len], blen-len, "\tChecksum: %d\n", header->checksum);
	
	return len;
}

/*
 * Dumps the product information II to a given buffer.
 */
ssize_t prodinfo_dump_prodInfoII_to_buffer(char *buf, int blen)
{
	u16 i = 0;
	ssize_t len = 0;
	prodInfo_prodInfoII_t *prodInfoII = &prodInfo.prodInfoII;
	
	len += snprintf(&buf[len], (blen - len), "PRODUCT INFORMATION II:\n");
	len += snprintf(&buf[len], (blen - len), "\tID: %d\n", prodInfoII->id);
	len += snprintf(&buf[len], (blen - len), "\tSize: %d\n", prodInfoII->size);
	len += snprintf(&buf[len], (blen - len), "\tProduct item code: ");
	for (i = 0; i < PRODINFO_PRODINFOII_ITEMCODE_LEN; i++) {
		if ( isprint(prodInfoII->product_itemcode[i] ) ) 
		{
			len += snprintf(&buf[len], (blen - len),  "%c", prodInfoII->product_itemcode[i]);
		}
		else 
		{
			break;
		}

	}
	len += snprintf(&buf[len], (blen - len), "\n");
	len += snprintf(&buf[len], (blen - len), "\tProduct serial number: ");
	for (i = 0; i < PRODINFO_PRODINFOII_SERNUM_LEN; i++) {
		if ( isprint(prodInfoII->product_sernum[i] ) ) 
		{
			len += snprintf(&buf[len], (blen - len), "%c", prodInfoII->product_sernum[i]);
		}
		else 
		{
			break;
		}

	}
	len += snprintf(&buf[len], (blen - len), "\n");
	len += snprintf(&buf[len], (blen - len), "\tProduct version: ");
	for (i = 0; i < PRODINFO_PRODINFOII_VERSION_LEN; i++) {
		if ( isprint(prodInfoII->product_version[i] ) ) 
		{
			len += snprintf(&buf[len], (blen - len), "%c", prodInfoII->product_version[i]);
		}
		else 
		{
			break;
		}

	}
	len += snprintf(&buf[len], (blen - len), "\n");
	len += snprintf(&buf[len], (blen - len), "\tProduct test date: %d (seconds)\n", (u32)prodInfoII->product_testdate);
	len += snprintf(&buf[len], (blen - len), "\tProduct tester name: ");
	for (i = 0; i < PRODINFO_PRODINFOII_TESTERNAME_LEN; i++) {
		if ( isprint(prodInfoII->product_testername[i] ) ) 
		{
			len += snprintf(&buf[len], (blen - len), "%c", prodInfoII->product_testername[i]);
		}
		else 
		{
			break;
		}

	}
	len += snprintf(&buf[len], (blen - len), "\n");
	len += snprintf(&buf[len], (blen - len), "\tChecksum: %d\n", prodInfoII->checksum);
	
	return len;
}

/*
 * Dumps the product information mac address to a given buffer.
 */
ssize_t prodinfo_dump_macAddr_to_buffer(char *buf, int blen)
{
	u16 i = 0;
	ssize_t len = 0;
	prodInfo_macAddr_t *macAddr = &prodInfo.macAddr;
	
	len += snprintf(&buf[len], (blen - len), "MAC ADDRESS:\n");
	len += snprintf(&buf[len], (blen - len), "\tID: %d\n", macAddr->id);
	len += snprintf(&buf[len], (blen - len), "\tSize: %d\n", macAddr->size);
	len += snprintf(&buf[len], (blen - len), "\tInterface: %d\n", macAddr->interface);
	len += snprintf(&buf[len], (blen - len), "\tAddress: ");
	for (i = 0; i < PRODINFO_MACADDR_ADDR_LEN; i++) {
		if (i == PRODINFO_MACADDR_ADDR_LEN - 1) {
			len += snprintf(&buf[len], (blen - len), "%02x", macAddr->address[i]);
		} else {
			len += snprintf(&buf[len], (blen - len), "%02x:", macAddr->address[i]);
		}
	}
	len += snprintf(&buf[len], (blen - len), "\n");
	len += snprintf(&buf[len], (blen - len), "\tChecksum: %d\n", macAddr->checksum);
	
	return len;
}

/*---------------------------------------------------------------------------*/

prodInfo_header_t *prodinfo_get_header_block(void)
{
	return &prodInfo.header;
}

prodInfo_macAddr_t *prodinfo_get_macAddr_block(void)
{
	return &prodInfo.macAddr;
}

prodInfo_prodInfoII_t *prodinfo_get_prodInfoII_block(void)
{
	return &prodInfo.prodInfoII;
}

/*---------------------------------------------------------------------------*/

/*
 * Calculates the checksum (sum of all bytes).
 */
static u8 calculate_checksum(void *data, u16 count)
{
	u8 checksum = 0;
	u16 i = 0;
	unsigned char *buf = (unsigned char *)data;
	
	for (i = 0; i < count; i++) {
		checksum += buf[i];
	}
	
	return checksum;
}

/*---------------------------------------------------------------------------*/

/*
 * Parses the data from a buffer to a product information header object.
 */
static int prodinfo_parse_header(prodInfo_header_t *header, unsigned char *buf)
{
	/* PCB item code */
	memcpy(header->pcb_itemcode, buf, PRODINFO_PCB_ITEMCODE_LEN);
	buf += PRODINFO_PCB_ITEMCODE_LEN;
	
	/* PCB serial number */
	memcpy(header->pcb_sernum, buf, PRODINFO_PCB_SERNUM_LEN);
	buf += PRODINFO_PCB_SERNUM_LEN;
	
	/* PCB hw version */
	header->pcb_hwversion = *((u8 *)buf);
	buf += sizeof(u8);
	
	/* PCB test date */
	header->pcb_testdate = (time_t)((((u32)buf[0]<<24) & 0xff000000) |
					(((u32)buf[1]<<16) & 0x00ff0000) |
					(((u32)buf[2]<<8) & 0x0000ff00) |
					((u32)buf[3] & 0x000000ff));
	buf += sizeof(time_t);
	
	/* PCB tester name */
	memcpy(header->pcb_testername, buf, PRODINFO_PCB_TESTERNAME_LEN);
	buf += PRODINFO_PCB_TESTERNAME_LEN;
	
	/* EEPROM size */
	header->eeprom_sz = (u16)((((u16)buf[0] << 8) & 0xFF00) | ((u16)buf[1] & 0x00FF));
	if (header->eeprom_sz != PRODINFO_EEPROM_SZ) {
		printk("WARNING: EEPROM size in header is not valid!\n");
	}
	buf += sizeof(u16);
	
	/* Header block checksum */
	header->checksum = *((u8 *)buf);
	if (header->checksum != calculate_checksum((void *)header, PRODINFO_HEADER_SIZE - 1)) {
		printk("WARNING: checksum error in header!\n");
		return 1;
	}
	
	return 0;
}

/*
 * Parses the data from a buffer to a product information II block object.
 */
static int prodinfo_parse_prodInfoII(prodInfo_prodInfoII_t *prodInfoII, unsigned char *buf)
{
	/* Product item code */
	memcpy(prodInfoII->product_itemcode, buf, PRODINFO_PRODINFOII_ITEMCODE_LEN);
	buf += PRODINFO_PRODINFOII_ITEMCODE_LEN;
	
	/* Product serial number */
	memcpy(prodInfoII->product_sernum, buf, PRODINFO_PRODINFOII_SERNUM_LEN);
	buf += PRODINFO_PRODINFOII_SERNUM_LEN;
	
	/* Product version */
	memcpy(prodInfoII->product_version, buf, PRODINFO_PRODINFOII_VERSION_LEN);
	buf += PRODINFO_PRODINFOII_VERSION_LEN;
	
	/* Product test date */
	prodInfoII->product_testdate = (time_t)(((((u32)buf[0]<<24) & 0xff000000) |
						(((u32)buf[1]<<16) & 0x00ff0000) |
						(((u32)buf[2]<<8) & 0x0000ff00) |
						((u32)buf[3] & 0x000000ff)));
	buf += sizeof(time_t);
	
	/* Product tester name */
	memcpy(prodInfoII->product_testername, buf, PRODINFO_PRODINFOII_TESTERNAME_LEN);
	buf += PRODINFO_PRODINFOII_TESTERNAME_LEN;
	
	/* Product information II block checksum */
	prodInfoII->checksum = *((u8 *)buf);
	if (prodInfoII->checksum != calculate_checksum((void *)prodInfoII, prodInfoII->size - 1)) {
		printk("WARNING: checksum error in product information II!\n");
		return 1;
	}
	
	return 0;
}

/*
 * Parses the data from a buffer to a MAC address block object.
 */
static int prodinfo_parse_macAddr(prodInfo_macAddr_t *macAddr, unsigned char *buf)
{
	/* MAC address interface */
	macAddr->interface = *((u8 *)buf);
	buf += sizeof(u8);
	
	/* MAC adress */
	memcpy(macAddr->address, buf, PRODINFO_MACADDR_ADDR_LEN);
	buf += PRODINFO_MACADDR_ADDR_LEN;
	
	/* MAC address block checksum */
	macAddr->checksum = *((u8 *)buf);
	if (macAddr->checksum != calculate_checksum((void *)macAddr, macAddr->size - 1)) {
		printk("WARNING: checksum error in MAC address!\n");
		return 1;
	}
	
	return 0;
}

/*---------------------------------------------------------------------------*/

int prodinfo_load_data(char *buf)
{
	u16 rdCount = 0, blockID = 0, blockSize = 0;
	
	/* Fill in the production information fields which are static like ids. */
	prodInfo.header.eeprom_sz = PRODINFO_EEPROM_SZ;
	prodInfo.prodInfoII.id = PRODINFO_PRODINFOII_ID;
	prodInfo.prodInfoII.size = PRODINFO_PRODINFOII_SZ;
	prodInfo.macAddr.id = PRODINFO_MACADDR_ID;
	prodInfo.macAddr.size = PRODINFO_MACADDR_SZ;
	prodInfo.endOfInfo = PRODINFO_END;
	
	if (prodinfo_parse_header(&prodInfo.header, buf)) {
		printk("WARNING: header not valid! Stop reading other fields!\n\n");
	} else {
		/* We read already 60 bytes (header). */
		rdCount = 60;
		while(rdCount < PRODINFO_EEPROM_SZ) {
			/* Read the block ID (u16) */
			blockID = (u16)((((u16)buf[rdCount] << 8) & 0xFF00) | ((u16)buf[rdCount+1] & 0x00FF));
			rdCount += sizeof(u16);
			
			if (blockID == PRODINFO_MACADDR_ID || blockID == PRODINFO_PRODINFOII_ID) {
				/* Read the size (u8). */
				blockSize = (u8)buf[rdCount];
				rdCount += sizeof(u8);
			}
			
			if (blockID == PRODINFO_MACADDR_ID) {
				if (blockSize != PRODINFO_MACADDR_SZ) {
					printk("WARNING: block size for MAC address is not valid!\n");
				} else {
					prodinfo_parse_macAddr(&prodInfo.macAddr, &buf[rdCount]);
					rdCount += blockSize - (sizeof(u16) + sizeof(u8));
				}
				
			} else if (blockID == PRODINFO_PRODINFOII_ID) {
				if (blockSize != PRODINFO_PRODINFOII_SZ) {
					printk("WARNING: block size for production information II is not valid!\n");
				} else {
					prodinfo_parse_prodInfoII(&prodInfo.prodInfoII, &buf[rdCount]);
					rdCount += blockSize - (sizeof(u16) + sizeof(u8));
				}
			} else if (blockID == PRODINFO_END) {
				printk("INFO: found end of information block!\n");
				break;
			} else {
				printk("WARNING: could not find valid product information blocks!\n");
				break;
			}
		}
	}
	
	return 0;
}
